import React from 'react';
import { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  textField: {
    width: '35em',
    marginTop: '5px',
  },
});

class TestDialog extends Component {
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <DialogTitle id="form-dialog-title">Create Item Card</DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="flex-start"
          >
            <Grid item>
              <TextField
                id="standard-required"
                label="Name"
                className={classes.textField}
              />
            </Grid>
            <Grid item></Grid>
            <Grid item></Grid>
            <Grid item></Grid>
            <Grid item></Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" color="primary">
            Create
          </Button>
        </DialogActions>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(TestDialog);
