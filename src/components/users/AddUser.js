import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { constantsConfig } from '../../config/ConstanstsConfig';
import { addNewUser } from '../../store/actions/auth/AuthActions';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import Grid from '@material-ui/core/Grid';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import ReactPhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/material.css';
import 'react-phone-number-input/style.css';
const employeesType = [
  {
    value: 'staff',
  },
];

const styles = theme => ({
  textField: {
    width: '35em',
    marginTop: '5px',
  },
  inputDate: {
    padding: 15,
    fontSize: '1rem',
    width: '100%',
    border: '1px solid',
    borderRadius: '0.5em',
    // borderColor:
  },
});

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      warehouseName: '',
      warehouseId: '',
      role: '',
      mobile: '',
      birthDate: null,
    };
  }
  handleChange = (e, type) => {
    if (type === 'warehouse')
      this.setState({
        warehouseId: e.target.value.id,
        warehouseName: e.target.value.warehouseName,
      });
    if (type === 'mobile')
      this.setState({
        mobile: e,
      });
    if (type === 'role')
      this.setState({
        role: e.target.value,
      });
    if (type === 'phoneNumber')
      this.setState({
        mobile: e,
      });
    else
      this.setState({
        [e.target.id]: e.target.value,
      });
  };

  handleDateChange = date => {
    this.setState({
      selectDate: date,
    });
    // setSelectedDate(date);
  };

  handleSubmit = () => {
    const updateUser = {
      name: this.state.name,
      warehouseName: this.state.warehouseName,
      warehouseId: this.state.warehouseId,
      role: this.state.role,
      mobile: parseInt(this.state.mobile),
      birthDate: this.state.birthDate,
    };
    this.props.addNewUser(updateUser);
  };
  render() {
    const { classes, warehouses } = this.props;
    return (
      <React.Fragment>
        <DialogTitle id="form-dialog-title">Create Item Card</DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="flex-start"
          >
            <Grid item>
              <TextField
                id="name"
                label="Name"
                onChange={this.handleChange}
                className={classes.textField}
              />
            </Grid>
            <Grid item>
              <TextField
                select
                label="Select"
                // value={this.state.years[0]}
                className={classes.textField}
                onChange={e => this.handleChange(e, 'warehouse')}
              >
                {warehouses &&
                  warehouses.map(warehouse => (
                    <MenuItem value={warehouse}>
                      {warehouse.warehouseName}
                    </MenuItem>
                  ))}
              </TextField>
            </Grid>
            <Grid item>
              <TextField
                select
                label="Select"
                // value={this.state.years[0]}
                className={classes.textField}
                onChange={e => this.handleChange(e, 'role')}
              >
                {employeesType &&
                  employeesType.map(type => (
                    <MenuItem value={type.value}>{type.value}</MenuItem>
                  ))}
              </TextField>
            </Grid>
            <Grid
              item
              container
              direction="row"
              justify="space-evenly"
              alignItems="center"
              style={{ marginTop: '0.5em' }}
            >
              <Grid item>
                <ReactPhoneInput
                  defaultCountry={'us'}
                  onChange={e => this.handleChange(e, 'phoneNumber')}
                />
              </Grid>
              <Grid item>
                <input
                  id="birthDate"
                  type="date"
                  label="Birth Date"
                  className={classes.inputDate}
                  onChange={e => this.handleChange(e, 'birthDate')}
                />
              </Grid>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleSubmit}
          >
            Create
          </Button>
        </DialogActions>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    warehouses: state.firestore.ordered.warehouses,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    addNewUser: credetials => dispatch(addNewUser(credetials)),
  };
};

export default compose(
  withStyles(styles),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionWarehouses,
        storeAs: 'warehouses',
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(AddUser);
