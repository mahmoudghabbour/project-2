import React from 'react';
import { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { signUp } from 'store/actions/auth/AuthActions';
import PropTypes from 'prop-types';
import { constantsConfig } from '../../config/ConstanstsConfig';
import Card from 'components/Card/Card.js';
import CardHeader from 'components/Card/CardHeader.js';
import CardBody from 'components/Card/CardBody.js';
import GridContainer from 'components/Grid/GridContainer.js';
import Fab from '@material-ui/core/Fab';
import Dialog from '@material-ui/core/Dialog';
import AddIcon from '@material-ui/icons/Add';
import UsersTable from 'components/Table/UsersTable';
import AddUser from './AddUser';
import TestDialog from 'TestDialog';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import UsersRequestsTable from 'components/Table/UsersRequestsTable';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const styles = theme => ({
  fab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
    zIndex: 2,
  },
});

var CryptoJS = require('crypto-js');

class UsersList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenAddUser: false,
      value: 0,
    };
  }
  handleCloseAddUser = () => {
    this.setState({
      isOpenAddUser: false,
    });
  };

  handleOpenAddUser = () => {
    this.setState({
      isOpenAddUser: true,
    });
  };

  handleChangeTabs = (event, newValue) => {
    this.setState({
      value: newValue,
    });
  };

  handleAcceptUser = (e, user) => {
    e.preventDefault();
    var bytes = CryptoJS.AES.decrypt(user.password, 'my-secret-key@123');
    var password = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    var updateSignUp = {
      name: user.name,
      email: user.email,
      mobile: parseInt(user.mobile),
      cities: user.cities,
      onlineStoreLink: user.onlineStoreLink,
      companyName: user.companyName,
      bank: {
        bankName: user.bank.bankName,
        accountNum: parseInt(user.bank.accountNum),
        bankFee: user.bank.bankFee,
        ibanNumber: parseInt(user.bank.ibanNumber),
        managerMobile: parseInt(user.bank.managerMobile),
        managerEmail: user.bank.managerEmail,
      },
    };
    const documentId = user.id;
    this.props.signUp({ updateSignUp, password, documentId });
  };

  render() {
    const { classes, users, usersRequests } = this.props;
    return (
      <React.Fragment>
        <Dialog
          fullWidth={true}
          maxWidth={'sm'}
          open={this.state.isOpenAddUser}
          onClose={this.handleCloseAddUser}
        >
          <AddUser />
        </Dialog>
        <AppBar position="static" color="default">
          <Tabs
            value={this.state.value}
            onChange={this.handleChangeTabs}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            aria-label="full width tabs example"
          >
            <Tab label="Users" {...a11yProps(0)} />
            <Tab label="Users Requests" {...a11yProps(1)} />
          </Tabs>
        </AppBar>
        <TabPanel value={this.state.value} index={0}>
          <GridContainer>
            <Card plain>
              <CardHeader plain color="primary">
                <h4 className={classes.cardTitleWhite}>Users Table</h4>
                <p className={classes.cardCategoryWhite}>Propratise is Table</p>
              </CardHeader>
              <CardBody>
                <UsersTable
                  tableHeaderColor="primary"
                  tableHead={[
                    'User Id',
                    'Name',
                    'Mobile',
                    'role',
                    'Warehouse Name',
                    'Birth Date',
                  ]}
                  tableData={users}
                />
              </CardBody>
            </Card>
          </GridContainer>
          <Fab
            aria-label="add"
            className={classes.fab}
            color="primary"
            onClick={this.handleOpenAddUser}
          >
            <AddIcon />
          </Fab>
        </TabPanel>
        <TabPanel value={this.state.value} index={1}>
          <GridContainer>
            <Card plain>
              <CardHeader plain color="primary">
                <h4 className={classes.cardTitleWhite}>Users Requests</h4>
                <p className={classes.cardCategoryWhite}>Propratise is Table</p>
              </CardHeader>
              <CardBody>
                <UsersRequestsTable
                  tableHeaderColor="primary"
                  tableHead={[
                    'Name',
                    'Email',
                    'Mobile',
                    'Bank Name',
                    'Account Num',
                    'Manager Email',
                    'Online store link',
                    '',
                  ]}
                  tableData={usersRequests}
                  handleAcceptUser={this.handleAcceptUser}
                />
              </CardBody>
            </Card>
          </GridContainer>
        </TabPanel>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
    users: state.firestore.ordered.users,
    usersRequests: state.firestore.ordered.usersRequests,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    signUp: creds => dispatch(signUp(creds)),
  };
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionEmployees,
        where: ['role', '==', 'staff'],
        storeAs: 'users',
      },
    ];
  }),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionUsersRequests,
        orderBy: ['createdTimestamp', 'desc'],
        storeAs: 'usersRequests',
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(UsersList);
