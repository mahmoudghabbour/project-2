import React from 'react';
import { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createWarehouse } from '../../store/actions/warehouse/WarehouseAction';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  textField: {
    width: '100%',
  },
  gridItem: {
    width: '100%',
    marginTop: '1em',
  },
});

class AddWarehouse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      warehouseName: null,
      address: null,
      warehouseNum: props.warehouses.length + 1,
    };
  }

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleSubmit = () => {
    const warehouseData = {
      warehouseName: this.state.warehouseName,
      warehouseNum: parseInt(this.state.warehouseNum),
      address: this.state.address,
    };
    this.props.createWarehouse(warehouseData);
  };
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <DialogTitle id="form-dialog-title">Add Warehouse</DialogTitle>
        <DialogContent>
          {' '}
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item className={classes.gridItem}>
              <TextField
                label="warehouse name"
                id="warehouseName"
                className={classes.textField}
                onChange={this.handleChange}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                label="Address"
                id="address"
                className={classes.textField}
                onChange={this.handleChange}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleSubmit}
          >
            Create
          </Button>
        </DialogActions>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    createWarehouse: warehouseData => dispatch(createWarehouse(warehouseData)),
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(AddWarehouse);
