import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { constantsConfig } from '../../config/ConstanstsConfig';
import AddStand from './AddStand';
import standIcon from '../../SVGIcons/Group 191.svg';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import AddIcon from '@material-ui/icons/Add';

const styles = theme => ({
  plusIcon: {
    color: '#29017c',
    fontSize: '4rem',
  },
});

const defaultProps = {
  //   bgcolor: "#29017c",
  borderColor: '#29017c',
  m: 1,
  border: 1,
  style: { width: '5rem', height: '15rem' },
};

class StandsDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenAddShelve: false,
    };
  }

  handleOpenAddShelve = () => {
    this.setState({
      isOpenAddShelve: true,
    });
  };

  handleCloseAddShelve = () => {
    this.setState({
      isOpenAddShelve: false,
    });
  };

  render() {
    const {
      classes,
      stands,
      warehouseId,
      warehouseName,
      warehouseNum,
      pathName,
      pathId,
      pathNum,
    } = this.props;
    return (
      <React.Fragment>
        <Dialog
          fullWidth
          maxWidth={'sm'}
          open={this.state.isOpenAddShelve}
          onClose={this.handleCloseAddShelve}
        >
          <AddStand
            pathId={pathId}
            pathName={pathName}
            pathNum={pathNum}
            warehouseId={warehouseId}
            warehouseName={warehouseName}
            warehouseNum={warehouseNum}
          />
        </Dialog>
        <DialogTitle id="form-dialog-title">Stands</DialogTitle>
        <DialogContent style={{ padding: 20 }}>
          <Grid container direction="row" justify="center" alignItems="center">
            {stands &&
              stands.map(stand => {
                return (
                  <Grid item key={stand.id}>
                    <Box borderRadius={16} {...defaultProps}>
                      <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                        style={{ marginTop: '80%' }}
                      >
                        <Grid item>
                          <img
                            src={standIcon}
                            // className={classes.pathIcon}
                            alt=""
                          />
                        </Grid>
                        <Grid item>
                          <Typography
                            variant="caption"
                            display="block"
                            align="center"
                            gutterBottom
                            style={{ color: '#29017c' }}
                          >
                            Stand
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography
                            variant="h6"
                            align="center"
                            gutterBottom
                            style={{ color: '#29017c' }}
                          >
                            {stand.standNum}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Box>
                  </Grid>
                );
              })}
            <Grid item>
              <Box borderRadius={16} {...defaultProps}>
                <Grid
                  container
                  direction="column"
                  justify="center"
                  alignItems="center"
                  style={{ marginTop: '80%' }}
                  onClick={this.handleOpenAddShelve}
                >
                  <Grid item>
                    <AddIcon fontSize="large" className={classes.plusIcon} />
                  </Grid>
                  <Grid item>
                    <Typography
                      variant="caption"
                      display="block"
                      align="center"
                      gutterBottom
                      style={{ color: '#29017c' }}
                    >
                      Stand
                    </Typography>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
          </Grid>
        </DialogContent>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
    stands: state.firestore.ordered.stands,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionStands,
        where: [['pathId', '==', props.pathId]],
        orderBy: ['standNum', 'asc'],
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(StandsDialog);
