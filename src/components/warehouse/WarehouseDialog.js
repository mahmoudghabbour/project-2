import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { constantsConfig } from '../../config/ConstanstsConfig';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddIcon from '@material-ui/icons/Add';
import AddWarehouse from './AddWarehouse';
import warehouseIcon from '../../SVGIcons/3f0289925034dd43370d963c546468c2.svg';
const styles = theme => ({
  warehouseIcon: {
    height: '12vmin',
    marginTop: '25%',
  },
  plusIcon: {
    color: '#29017c',
    fontSize: '4rem',
  },
  typography: {
    fontSize: '1rem',
    color: '#29017c',
  },
});

const defaultProps = {
  borderColor: '#29017c',
  m: 1,
  border: 1,
  style: { width: '10rem', height: '15rem' },
};

class WarehouseDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenAddWarehouse: false,
    };
  }
  handleOpenAddWarehouse = () => {
    this.setState({
      isOpenAddWarehouse: true,
    });
  };
  handleCloseAddWarehouse = () => {
    this.setState({
      isOpenAddWarehouse: false,
    });
  };
  render() {
    const {
      classes,
      warehouses,
      handleChangeWarehouseId,
      nextStep,
    } = this.props;
    return (
      <React.Fragment>
        <Dialog
          fullWidth
          maxWidth={'sm'}
          open={this.state.isOpenAddWarehouse}
          onClose={this.handleCloseAddWarehouse}
        >
          <AddWarehouse warehouses={warehouses} />
        </Dialog>
        <DialogTitle id="form-dialog-title">Warehouses</DialogTitle>
        <DialogContent style={{ padding: 20 }}>
          <Grid container direction="row" justify="center" alignItems="center">
            {warehouses &&
              warehouses.map(warehouse => {
                return (
                  <Grid item>
                    <Box borderRadius={16} {...defaultProps}>
                      <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                        style={{ marginTop: '30%' }}
                        onClick={e => {
                          handleChangeWarehouseId(
                            e,
                            warehouse.id,
                            warehouse.warehouseName,
                            warehouse.warehouseNum
                          );
                          nextStep();
                        }}
                      >
                        <Grid item>
                          <img
                            src={warehouseIcon}
                            alt=""
                            className={classes.warehouseIcon}
                          />
                        </Grid>
                        <Grid item>
                          <Typography className={classes.typography}>
                            {warehouse.warehouseName}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Box>
                  </Grid>
                );
              })}
            <Grid item>
              <Box
                onClick={this.handleOpenAddWarehouse}
                borderRadius={16}
                {...defaultProps}
              >
                <Grid
                  container
                  direction="column"
                  justify="center"
                  alignItems="center"
                  style={{ marginTop: '50%' }}
                >
                  <Grid item>
                    <AddIcon fontSize="large" className={classes.plusIcon} />
                  </Grid>
                  <Grid item>
                    <Typography className={classes.typography}>
                      {' '}
                      Add Warehouse
                    </Typography>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
          </Grid>
        </DialogContent>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
    warehouses: state.firestore.ordered.warehouses,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionWarehouses,
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(WarehouseDialog);
