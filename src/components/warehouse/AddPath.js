import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createPath } from '../../store/actions/warehouse/WarehouseAction';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Grid from '@material-ui/core/Grid';
const styles = theme => ({
  textField: {
    width: '100%',
  },
  gridItem: {
    width: '100%',
    marginTop: '1em'
  },
});

class AddPath extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      pathNum: null,
      pathName: null,
      warehouseId: props.warehouseId,
      warehouseName: props.warehouseName,
      warehouseNum: props.warehouseNum,
    };
  }
  handleClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  handleChange = e => {
    this.setState({
      pathNum: e.target.value,
      pathName: 'path ' + e.target.value,
    });
  };

  handleSubmit = () => {
    const pathData = {
      pathNum: parseInt(this.state.pathNum),
      pathName: this.state.pathName,
      warehouseId: this.state.warehouseId,
      warehouseName: this.state.warehouseName,
      warehouseNum: this.state.warehouseNum,
    };
    // debugger;
    this.props.createPath(pathData);
  };
  render() {
    const { classes, paths, nextStep, handleChangePathId } = this.props;
    return (
      <Dialog open={this.state.isOpen} onClose={this.handleClose}>
        <DialogTitle id="form-dialog-title">Add Path</DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item className={classes.gridItem}>
              <TextField
                id="pathNum"
                label="Path num"
                className={classes.textField}
                onChange={this.handleChange}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleSubmit}
          >
            Create
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
    // paths: state.firestore.ordered.paths,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    createPath: pathData => dispatch(createPath(pathData)),
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(AddPath);
