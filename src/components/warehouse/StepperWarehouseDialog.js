import React from 'react';
import { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import WarehouseDialog from './WarehouseDialog';
import AddPath from './AddPath';
import AddStand from './AddStand';
import PathDialog from './PathDialog';
import StandsDialog from './StandsDialog';

const styles = theme => ({});

class StepperWarehouseDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      warehouseId: null,
      warehouseName: null,
      warehouseNum: null,
      pathId: null,
      pathNum: null,
      pathName: null,
    };
  }
  //Proceed to next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1,
    });
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1,
    });
  };
  handleOpenWarehouseDialog = () => {
    this.setState({
      isOpenWarehouseDialog: true,
    });
  };
  handleCloseWarehouseDialog = () => {
    this.setState({
      isOpenWarehouseDialog: false,
    });
  };
  handleChangeWarehouseId = (e, warehouseId, warehouseName, warehouseNum) => {
    this.setState({
      warehouseId: warehouseId,
      warehouseName: warehouseName,
      warehouseNum: warehouseNum,
    });
  };

  handleChangePathId = (e, pathId, pathName, pathNum) => {
    this.setState({
      pathId: pathId,
      pathName: pathName,
      pathNum: pathNum,
    });
  };
  render() {
    const { step } = this.state;
    const { handleCloseWarehouseDialog } = this.props;
    switch (step) {
      case 1:
        return (
          <Dialog
            open={step === 1 ? true : false}
            fullWidth={true}
            onClose={handleCloseWarehouseDialog}
          >
            <WarehouseDialog
              nextStep={this.nextStep}
              handleChangeWarehouseId={this.handleChangeWarehouseId}
            />
          </Dialog>
        );
      case 2:
        return (
          <Dialog
            open={step === 2 ? true : false}
            fullWidth={true}
            onClose={handleCloseWarehouseDialog}
          >
            <PathDialog
              nextStep={this.nextStep}
              warehouseId={this.state.warehouseId}
              warehouseName={this.state.warehouseName}
              warehouseNum={this.state.warehouseNum}
              handleChangePathId={this.handleChangePathId}
            />
          </Dialog>
        );
      case 3:
        return (
          <Dialog
            open={step === 3 ? true : false}
            fullWidth={true}
            onClose={handleCloseWarehouseDialog}
          >
            <StandsDialog
              warehouseId={this.state.warehouseId}
              warehouseName={this.state.warehouseName}
              warehouseNum={this.state.warehouseNum}
              pathId={this.state.pathId}
              pathName={this.state.pathName}
              pathNum={this.state.pathNum}
            />
          </Dialog>
        );
    }
  }
}

export default withStyles(styles)(StepperWarehouseDialog);
