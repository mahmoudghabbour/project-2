import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { constantsConfig } from '../../config/ConstanstsConfig';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Box from '@material-ui/core/Box';
import AddIcon from '@material-ui/icons/Add';
import AddPath from './AddPath';
import pathIcon from '../../SVGIcons/Group 173.svg';

const styles = theme => ({
  plusIcon: {
    color: '#29017c',
    fontSize: '4rem',
  },
});

const defaultProps = {
  //   bgcolor: "#29017c",
  borderColor: '#29017c',
  m: 1,
  border: 1,
  style: { width: '5rem', height: '15rem' },
};

class PathDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      isOpenAddPath: false,
    };
  }
  handleClose = () => {
    this.setState({
      isOpen: false,
    });
  };
  handleOpenAddPath = () => {
    this.setState({
      isOpenAddPath: true,
    });
  };

  handleCloseAddPath = () => {
    this.setState({
      isOpenAddPath: false,
    });
  };
  render() {
    const {
      classes,
      paths,
      nextStep,
      handleChangePathId,
      warehouseName,
      warehouseId,
      warehouseNum,
    } = this.props;
    return (
      <Dialog open={this.state.isOpen} onClose={this.handleClose}>
        <Dialog
          fullWidth
          maxWidth={'sm'}
          open={this.state.isOpenAddPath}
          onClose={this.handleCloseAddPath}
        >
          <AddPath
            warehouseId={warehouseId}
            warehouseName={warehouseName}
            warehouseNum={warehouseNum}
          />
        </Dialog>
        <DialogTitle id="form-dialog-title">Paths</DialogTitle>
        <DialogContent style={{ padding: 20 }}>
          <Grid container direction="row" justify="center" alignItems="center">
            {paths &&
              paths.map(path => {
                return (
                  <Grid item>
                    <Box borderRadius={16} {...defaultProps}>
                      <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                        style={{ marginTop: '80%' }}
                        onClick={e => {
                          handleChangePathId(
                            e,
                            path.id,
                            path.pathName,
                            path.pathNum
                          );
                          nextStep();
                        }}
                      >
                        <Grid item>
                          <img
                            src={pathIcon}
                            className={classes.pathIcon}
                            alt=""
                          />
                        </Grid>
                        <Grid item>
                          <Typography
                            variant="caption"
                            display="block"
                            align="center"
                            gutterBottom
                            style={{ color: '#29017c' }}
                          >
                            path
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography
                            variant="h6"
                            align="center"
                            gutterBottom
                            style={{ color: '#29017c' }}
                          >
                            {path.pathNum}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Box>
                  </Grid>
                );
              })}
            <Grid item>
              <Box borderRadius={16} {...defaultProps}>
                <Grid
                  container
                  direction="column"
                  justify="center"
                  alignItems="center"
                  style={{ marginTop: '80%' }}
                  onClick={this.handleOpenAddPath}
                >
                  <Grid item>
                    <AddIcon fontSize="large" className={classes.plusIcon} />
                  </Grid>
                  <Grid item>
                    <Typography
                      variant="caption"
                      display="block"
                      align="center"
                      gutterBottom
                      style={{ color: '#29017c' }}
                    >
                      path
                    </Typography>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
          </Grid>
        </DialogContent>
        {/* <React.Fragment>
          {paths &&
            paths.map(path => {
              return (
                <React.Fragment>
                  <Typography
                    onClick={e => {
                      handleChangePathId(
                        e,
                        path.id,
                        path.pathName,
                        path.pathNum
                      );
                      nextStep();
                    }}
                  >
                    Path num {path.pathNum}
                  </Typography>
                </React.Fragment>
              );
            })}
          <Button onClick={this.handleOpenAddPath}>Add path</Button>
        </React.Fragment> */}
      </Dialog>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
    paths: state.firestore.ordered.paths,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionPaths,
        where: ['warehouseId', '==', props.warehouseId],
        orderBy: ['pathNum', 'asc'],
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(PathDialog);
