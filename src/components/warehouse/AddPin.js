import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { constantsConfig } from '../../config/ConstanstsConfig';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({});

class AddPin extends Component {
  render() {
    return <React.Fragment></React.Fragment>;
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionPins,
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(AddPin);
