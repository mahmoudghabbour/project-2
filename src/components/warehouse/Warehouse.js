import React from 'react';
import { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { constantsConfig } from '../../config/ConstanstsConfig';
import Dialog from '@material-ui/core/Dialog';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import StepperWarehouseDialog from './StepperWarehouseDialog';
import warehouseIcon from '../../SVGIcons/3f0289925034dd43370d963c546468c2.svg';

const styles = theme => ({
  fab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
    zIndex: 2,
  },
  warehouseIcon: {
    height: '12vmin',
    marginTop: '25%',
  },
  plusIcon: {
    color: '#29017c',
    fontSize: '4rem',
  },
  typography: {
    fontSize: '1rem',
    color: '#29017c',
  },
});

const defaultProps = {
  borderColor: '#29017c',
  m: 1,
  border: 1,
  style: { width: '10rem', height: '15rem' },
};

class Warehouse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenWarehouseDialog: false,
      step: 1,
    };
  }
  //Proceed to next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1,
    });
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1,
    });
  };
  handleOpenWarehouseDialog = () => {
    this.setState({
      isOpenWarehouseDialog: true,
    });
  };
  handleCloseWarehouseDialog = () => {
    this.setState({
      isOpenWarehouseDialog: false,
    });
  };
  render() {
    const { classes, warehouses } = this.props;
    return (
      <React.Fragment>
        <Dialog
          open={this.state.isOpenWarehouseDialog}
          onClose={this.handleCloseWarehouseDialog}
        >
          <StepperWarehouseDialog
            handleCloseWarehouseDialog={this.handleCloseWarehouseDialog}
          />
        </Dialog>
        <Grid
          container
          direction="row"
          justify="space-evenly"
          alignItems="center"
          style={{ marginTop: '5em' }}
        >
          {warehouses &&
            warehouses.map(warehouse => {
              return (
                <Grid item>
                  <Box borderRadius={16} {...defaultProps}>
                    <Grid
                      container
                      direction="column"
                      justify="center"
                      alignItems="center"
                      style={{ marginTop: '30%' }}
                      // onClick={e => {
                      //   handleChangeWarehouseId(
                      //     e,
                      //     warehouse.id,
                      //     warehouse.warehouseName,
                      //     warehouse.warehouseNum
                      //   );
                      //   nextStep();
                      // }}
                    >
                      <Grid item>
                        <img
                          src={warehouseIcon}
                          alt=""
                          className={classes.warehouseIcon}
                        />
                      </Grid>
                      <Grid item>
                        <Typography className={classes.typography}>
                          {warehouse.warehouseName}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Box>
                </Grid>
              );
            })}
        </Grid>
        <Fab
          aria-label="add"
          className={classes.fab}
          color="primary"
          onClick={this.handleOpenWarehouseDialog}
        >
          <AddIcon />
        </Fab>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
    warehouses: state.firestore.ordered.warehouses,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionWarehouses,
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(Warehouse);
