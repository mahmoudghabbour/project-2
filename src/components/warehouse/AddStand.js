import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { constantsConfig } from '../../config/ConstanstsConfig';
import { createStand } from '../../store/actions/warehouse/WarehouseAction';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
  textField: {
    width: '100%',
  },
  gridItem: {
    width: '100%',
    marginTop: '1em',
  },
});

class AddStand extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenAddShelve: false,
      pathId: props.pathId,
      pathName: props.pathName,
      pathNum: props.pathNum,
      warehouseId: props.warehouseId,
      warehouseName: props.warehouseName,
      warehouseNum: props.warehouseNum,
      standNum: null,
      standName: null,
      typeOfPins: null,
      numOfShelves: null,
      numOfPins: null,
      length: null,
      width: null,
      height: null,
    };
  }

  handleOpenAddShelve = () => {
    this.setState({
      isOpenAddShelve: true,
    });
  };

  handleCloseAddShelve = () => {
    this.setState({
      isOpenAddShelve: false,
    });
  };
  handleChangeStand = e => {
    this.setState({
      standNum: e.target.value,
      standName: 'stand ' + e.target.value,
    });
  };

  handleChangeShelve = e => {
    if (e.target.value == 5) {
      this.setState({
        numOfShelves: e.target.value,
      });
    }
  };

  handleChangeTypeOfPins = e => {
    if (e.target.value === 'small')
      this.setState({
        typeOfPins: e.target.value,
        numOfPinsPerShelve: 5,
        length: 92,
        width: 44,
        height: 50,
      });
    else
      this.setState({
        typeOfPins: e.target.value,
        numOfPinsPerShelve: 2,
        length: 60,
        width: 20,
        height: 60,
      });
  };

  handleSubmit = () => {
    const updateStand = {
      pathId: this.state.pathId,
      pathName: this.state.pathName,
      pathNum: parseInt(this.state.pathNum),
      standNum: parseInt(this.state.standNum),
      standName: this.state.standName,
      warehouseId: this.state.warehouseId,
      warehouseName: this.state.warehouseName,
      warehouseNum: this.state.warehouseNum,
      typeOfPins: this.state.typeOfPins,
      numOfShelves: parseInt(this.state.numOfShelves),
      numOfPins: parseInt(
        this.state.numOfPinsPerShelve * this.state.numOfShelves
      ),
      numOfPinsPerShelve: parseInt(this.state.numOfPinsPerShelve),
      pinSpace: {
        length: parseInt(this.state.length),
        width: parseInt(this.state.width),
        height: parseInt(this.state.height),
      },
      isFull: false,
    };
    this.props.createStand(updateStand);
  };

  render() {
    const { classes, stands, pathName, pathId, pathNum } = this.props;
    return (
      <React.Fragment>
        <DialogTitle id="form-dialog-title">Add Stand</DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item className={classes.gridItem}>
              <TextField
                id="standNum"
                label="Stand Num"
                className={classes.textField}
                onChange={this.handleChangeStand}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                id="numOfShelves"
                label="Num Of Shelves"
                className={classes.textField}
                onChange={this.handleChangeShelve}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                select
                label="Type of pins"
                className={classes.textField}
                onChange={e => this.handleChangeTypeOfPins(e)}
              >
                <MenuItem value="small">
                  <Typography>Small</Typography>
                </MenuItem>
                <MenuItem value="large">
                  <Typography>Large</Typography>
                </MenuItem>
              </TextField>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleSubmit}
          >
            Create
          </Button>
        </DialogActions>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
    stands: state.firestore.ordered.stands,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    createStand: standData => dispatch(createStand(standData)),
  };
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionStands,
        where: [
          // ['warehouseId', '==', props.warehouseId],
          ['pathId', '==', props.pathId],
        ],
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(AddStand);
