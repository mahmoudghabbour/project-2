import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { constantsConfig } from '../../config/ConstanstsConfig';
import { withStyles } from '@material-ui/core/styles';
import Card from 'components/Card/Card.js';
import CardHeader from 'components/Card/CardHeader.js';
import CardBody from 'components/Card/CardBody.js';
import GridContainer from 'components/Grid/GridContainer.js';
import InventoryTable from 'components/Table/InventoryTable';

const styles = theme => ({
  fab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
    zIndex: 2,
  },
});
class InventoryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenShipmentInRequest: false,
    };
  }
  render() {
    const { classes, inventoryNotifications } = this.props;
    return (
      <React.Fragment>
        <GridContainer>
          <Card plain>
            <CardHeader plain color="primary">
              <h4 className={classes.cardTitleWhite}>Inventory Table</h4>
              <p className={classes.cardCategoryWhite}>Propratise is Table</p>
            </CardHeader>
            <CardBody>
              <InventoryTable
                tableHeaderColor="primary"
                tableHead={[
                  'Serial Number',
                  'Seller',
                  'Receiver',
                  'Source Location',
                  'warehouse',
                ]}
                tableData={inventoryNotifications}
              />
            </CardBody>
          </Card>
        </GridContainer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
    inventoryNotifications: state.firestore.ordered.inventoryNotifications,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionInventoryNotifications,
        storeAs: 'inventoryNotifications',
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(InventoryList);
