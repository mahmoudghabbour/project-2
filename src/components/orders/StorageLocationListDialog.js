import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import firebase from 'firebase/app';
import { firestoreConnect } from 'react-redux-firebase';
import { withStyles } from '@material-ui/core/styles';
import { constantsConfig } from '../../config/ConstanstsConfig';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogActions from '@material-ui/core/DialogActions';
import Divider from '@material-ui/core/Divider';
import {
  updatePin,
  archivedOrder,
} from '../../store/actions/orders/OrdersActions';

const styles = theme => ({
  circularProgress: {
    color: 'white',
  },
  button: {
    width: '10em',
  },
  titleTypography: {
    color: '#8e24aa',
    fontFamily: 'Segoe UI',
    fontWeight: 'bold',
    fontSize: '1.2rem',
  },
  resultTypography: {
    fontFamily: 'Segoe UI',
    fontWeight: 'bold',
    fontSize: '0.8rem',
  },
  itemsTypography: {
    fontFamily: 'Segoe UI',
    fontWeight: 'bold',
    fontSize: '1rem',
  },
});

function isInt(value) {
  return (
    !isNaN(value) &&
    parseInt(Number(value)) == value &&
    !isNaN(parseInt(value, 10))
  );
}

function getArrayMax(array) {
  return Math.max.apply(null, array);
}

class StorageLocationListDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sellerProfile: null,
      sellerId: props.sellerId,
      orderItem: props.orderItem,
      testQuantity: 0,
      numOfPins: 0,
      indexOfItems: props.indexOfItems,
      numOfTypes: props.numOfTypes,
      pinsArray: [],
    };
  }
  componentDidMount() {
    this.handleGetProfile(this.props.sellerId);
    this.handleGetRequests(this.state.orderItem);
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.orderItem.id != nextProps.orderItem.id) {
      this.setState({
        sellerProfile: null,
        sellerId: nextProps.sellerId,
        orderItem: nextProps.orderItem,
        testQuantity: 0,
        numOfPins: 0,
        pinsArray: [],
        indexOfItems: nextProps.indexOfItems,
        numOfTypes: nextProps.numOfTypes,
      });
      this.handleGetProfile(this.props.sellerId);
      this.handleGetRequests(nextProps.orderItem);
    }
  }

  handleGetProfile = async sellerId => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionUsers)
      .where('id', '==', sellerId);
    const snapshot = await query.get();
    const sellerProfile = snapshot.docs.map(doc => doc.data());
    this.setState({
      sellerProfile: sellerProfile[0],
    });
  };

  handleGetRequests = async orderItem => {
    var pinsArray = [];
    var numOfPins = 0;
    const pinsOfItemsQuery = firebase
      .firestore()
      .collection(constantsConfig.collectionPins)
      .where('sku', '==', orderItem.sku)
      .where('sellerId', '==', this.state.sellerId);
    const snapshot = await pinsOfItemsQuery.get();
    const pinsOfItems = snapshot.docs.map(doc => doc.data());

    for (let i = 0; i < pinsOfItems.length; i++) {
      if (
        (pinsOfItems[i].capacity != 0 &&
          pinsOfItems[i].capacity < pinsOfItems[i].maxCapacity) ||
        pinsOfItems.length === 1
      ) {
        var itemPath =
          'w' +
          1 +
          'p' +
          pinsOfItems[i].pathNum +
          's' +
          pinsOfItems[i].standNum +
          's' +
          pinsOfItems[i].shelveNum +
          'p' +
          pinsOfItems[i].pinNum;
        var pin = pinsOfItems[i];
        pinsArray.push({ itemPath, pin, availableSpace: true });
        break;
      }
    }
    if (pinsArray.length > 0) {
      // there is a bin isn't full
      if (
        orderItem.quantity <=
        pinsArray[0].pin.maxCapacity - pinsArray[0].pin.capacity
      ) {
        // all items in one available bin
        numOfPins = 1;
        this.setState({
          numOfPins: numOfPins,
        });
      } else {
        numOfPins =
          (orderItem.quantity -
            (pinsArray[0].pin.maxCapacity - pinsArray[0].pin.capacity)) /
          pinsArray[0].pin.maxCapacity;
        if (isInt(numOfPins)) {
          numOfPins += pinsArray.length;
          this.setState({
            numOfPins: numOfPins,
          });
        } else {
          numOfPins = parseInt(numOfPins);
          numOfPins += 1 + pinsArray.length;
          this.setState({
            numOfPins: numOfPins,
          });
        }
      }
    } else {
      // all bins is full
      numOfPins = orderItem.quantity / pinsOfItems[0].maxCapacity;
      if (isInt(numOfPins)) {
        this.setState({
          numOfPins: numOfPins,
        });
      } else {
        numOfPins = parseInt(numOfPins);
        numOfPins += 1;
        this.setState({
          numOfPins: numOfPins,
        });
      }
    }
    this.handleGetPins(numOfPins, pinsArray, pinsOfItems, orderItem);
  };

  handleGetPins = (numOfPins, pinsArray, pinsOfItems, orderItem) => {
    if (pinsArray.length > 0) {
      if (numOfPins > 1) {
        for (let i = 0; i < numOfPins; i++) {
          if (pinsOfItems[i].isFull === true) {
            var itemPath =
              'w' +
              1 +
              'p' +
              pinsOfItems[i].pathNum +
              's' +
              pinsOfItems[i].standNum +
              's' +
              pinsOfItems[i].shelveNum +
              'p' +
              pinsOfItems[i].pinNum;
            var pin = pinsOfItems[i];
            pinsArray.push({ itemPath, pin });
          }
        }
      }
    } else {
      for (let i = 0; i < numOfPins; i++) {
        var itemPath =
          'w' +
          1 +
          'p' +
          pinsOfItems[i].pathNum +
          's' +
          pinsOfItems[i].standNum +
          's' +
          pinsOfItems[i].shelveNum +
          'p' +
          pinsOfItems[i].pinNum;
        var pin = pinsOfItems[i];
        pinsArray.push({ itemPath, pin });
      }
    }
    this.setState({
      pinsArray: pinsArray,
    });
    var testQuantity = orderItem.quantity;
    for (let i = 0; i < pinsArray.length; i++) {
      if (pinsArray[i].availableSpace) {
        testQuantity -=
          pinsArray[i].pin.maxCapacity - pinsArray[i].pin.capacity;
      } else {
        if (pinsArray[i].pin.id === pinsArray[pinsArray.length - 1].pin.id) {
        } else {
          testQuantity -= pinsArray[i].pin.maxCapacity;
        }
      }
    }
    this.setState({
      testQuantity: testQuantity,
    });
    this.handleUpdatePins(numOfPins, pinsArray, orderItem);
  };

  handleUpdatePins = (numOfPins, pinsArray, orderItem) => {
    var quantity = orderItem.quantity;
    for (let i = 0; i < pinsArray.length; i++) {
      if (pinsArray[0].availableSpace) {
        // if there is a bin doesn't complete
        if (numOfPins === 1) {
          // if all items in one bin
          var numOfItemsInPin =
            pinsArray[i].pin.maxCapacity - pinsArray[i].pin.capacity;
          var updatePin = {
            capacity:
              orderItem.quantity < numOfItemsInPin
                ? pinsArray[i].pin.capacity + orderItem.quantity
                : null,
            maxCapacity:
              orderItem.quantity < numOfItemsInPin
                ? parseInt(pinsArray[i].pin.maxCapacity)
                : null,
            seller:
              orderItem.quantity < numOfItemsInPin
                ? this.state.sellerProfile
                : null,
            sellerId:
              orderItem.quantity < numOfItemsInPin
                ? pinsArray[i].pin.sellerId
                : null,
            sku: orderItem.quantity < numOfItemsInPin ? orderItem.sku : null,
            isFull: false,
            pinId: pinsArray[i].pin.id,
          };
          this.props.updatePin(updatePin);
        } else {
          // if items in more than bin
          if (pinsArray[i].availableSpace) {
            quantity -=
              pinsArray[i].pin.maxCapacity - pinsArray[i].pin.capacity;

            var updatePin = {
              capacity: null,
              maxCapacity: null,
              seller: null,
              sellerId: null,
              sku: null,
              isFull: false,
              pinId: pinsArray[i].pin.id,
            };
            this.props.updatePin(updatePin);
          } else if (
            pinsArray[i].pin.id === pinsArray[pinsArray.length - 1].pin.id
          ) {
            // items from the last bin
            var numOfItemsInPin =
              pinsArray[i].pin.maxCapacity - pinsArray[i].pin.capacity;
            var updatePin = {
              capacity:
                quantity < numOfItemsInPin
                  ? pinsArray[i].pin.capacity + quantity
                  : null,
              maxCapacity:
                quantity < numOfItemsInPin
                  ? parseInt(pinsArray[i].pin.maxCapacity)
                  : null,
              seller:
                quantity < numOfItemsInPin ? this.state.sellerProfile : null,
              sellerId:
                quantity < numOfItemsInPin ? pinsArray[i].pin.sellerId : null,
              sku: quantity < numOfItemsInPin ? orderItem.sku : null,
              isFull: false,
              pinId: pinsArray[i].pin.id,
            };
            this.props.updatePin(updatePin);
          } else {
            quantity -=
              pinsArray[i].pin.maxCapacity - pinsArray[i].pin.capacity;
            var updatePin = {
              capacity: null,
              maxCapacity: null,
              seller: null,
              sellerId: null,
              sku: null,
              isFull: false,
              pinId: pinsArray[i].pin.id,
            };
            this.props.updatePin(updatePin);
          }
        }
      } else {
        if (pinsArray[i].pin.id === pinsArray[pinsArray.length - 1].pin.id) {
          // items from the last bin
          // quantity -= pinsArray[i].pin.maxCapacity - pinsArray[i].pin.capacity;

          var numOfItemsInPin =
            pinsArray[i].pin.maxCapacity - pinsArray[i].pin.capacity;
          // quantity -= numOfItemsInPin;
          var updatePin = {
            capacity:
              quantity < numOfItemsInPin
                ? pinsArray[i].pin.capacity + quantity
                : null,
            maxCapacity:
              quantity < numOfItemsInPin
                ? parseInt(pinsArray[i].pin.maxCapacity)
                : null,
            seller:
              quantity < numOfItemsInPin ? this.state.sellerProfile : null,
            sellerId:
              quantity < numOfItemsInPin ? pinsArray[i].pin.sellerId : null,
            sku: quantity < numOfItemsInPin ? orderItem.sku : null,
            isFull: false,
            pinId: pinsArray[i].pin.id,
          };
          this.props.updatePin(updatePin);
        } else {
          quantity -= pinsArray[i].pin.maxCapacity - pinsArray[i].pin.capacity;
          var updatePin = {
            capacity: null,
            maxCapacity: null,
            seller: null,
            sellerId: null,
            sku: null,
            isFull: false,
            pinId: pinsArray[i].pin.id,
          };
          this.props.updatePin(updatePin);
        }
      }
    }
  };

  handleFinish = () => {
    var updateOrder = {
      id: this.props.order.id,
      items: this.props.order.items,
      seller: this.props.order.seller,
      sellerId: this.props.order.sellerId,
      sellerLocation: this.props.order.sellerLocation,
      warehouseId: this.props.order.warehouseId,
      warehouseName: this.props.order.warehouseName,
      warehouseNum: this.props.order.warehouseNum,
    };
    this.props.archivedOrder(updateOrder);
  };

  render() {
    const { classes, spinnerOrder } = this.props;
    return (
      <React.Fragment>
        <Grid
          container
          direction="row"
          justify="space-around"
          alignItems="center"
        >
          <Grid item>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Typography className={classes.titleTypography}>SKU</Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.resultTypography}>
                  {this.state.orderItem.sku}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Typography className={classes.titleTypography}>
                  Total quantity
                </Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.resultTypography}>
                  {this.state.orderItem.quantity}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Typography className={classes.titleTypography}>
                  Dimensions
                </Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.resultTypography}>
                  {this.state.orderItem.dimenesions.length +
                    '*' +
                    this.state.orderItem.dimenesions.width +
                    '*' +
                    this.state.orderItem.dimenesions.height}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Grid
          container
          direction="row"
          justify="space-evenly"
          alignItems="center"
          style={{ marginTop: '1em' }}
        >
          <Grid item>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Typography className={classes.titleTypography}>
                  Date
                </Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.resultTypography}>
                  {new Intl.DateTimeFormat('en-US', {
                    year: 'numeric',
                    month: '2-digit',
                    day: '2-digit',
                  }).format(this.state.orderItem.date)}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Typography className={classes.titleTypography}>
                  Description
                </Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.resultTypography}>
                  {this.state.orderItem.description}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Divider />
        {this.state.spinner === true ? (
          <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            className={classes.gridCircularProgress}
          >
            <Grid item xs={3}>
              <CircularProgress className={classes.circularProgress} />
            </Grid>
          </Grid>
        ) : (
          <React.Fragment>
            {this.state.pinsArray &&
              this.state.pinsArray.map(pin => {
                return (
                  <React.Fragment>
                    <Grid
                      container
                      direction="row"
                      justify="space-around"
                      alignItems="center"
                    >
                      <Grid item>
                        <Grid
                          container
                          direction="column"
                          justifyContent="center"
                          alignItems="center"
                        >
                          <Grid item>
                            <Typography className={classes.titleTypography}>
                              Path
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography className={classes.resultTypography}>
                              {pin.pin.pathNum}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid item>
                        <Grid
                          container
                          direction="column"
                          justifyContent="center"
                          alignItems="center"
                        >
                          <Grid item>
                            <Typography className={classes.titleTypography}>
                              Stand
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography className={classes.resultTypography}>
                              {pin.pin.standNum}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="space-around"
                      alignItems="center"
                      style={{ marginLeft: '-0.5em' }}
                    >
                      <Grid item>
                        <Grid
                          container
                          direction="column"
                          justifyContent="center"
                          alignItems="center"
                        >
                          <Grid item>
                            <Typography className={classes.titleTypography}>
                              Shelve
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography className={classes.resultTypography}>
                              {pin.pin.shelveNum}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid item>
                        <Grid
                          container
                          direction="column"
                          justifyContent="center"
                          alignItems="center"
                        >
                          <Grid item>
                            <Typography className={classes.titleTypography}>
                              Bin
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography className={classes.resultTypography}>
                              {pin.pin.pinNum}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="center"
                      alignItems="center"
                    >
                      <Grid item>
                        <Typography
                          align="center"
                          className={classes.titleTypography}
                        >
                          Quantity
                        </Typography>
                        <Typography
                          align="center"
                          className={classes.resultTypography}
                        >
                          {pin.availableSpace
                            ? this.state.pinsArray[
                                this.state.pinsArray.length - 1
                              ].pin.id === pin.pin.id
                              ? this.state.orderItem.quantity
                              : pin.pin.maxCapacity - pin.pin.capacity
                            : this.state.pinsArray[
                                this.state.pinsArray.length - 1
                              ].pin.id === pin.pin.id
                            ? this.state.testQuantity
                            : pin.pin.maxCapacity}
                        </Typography>
                      </Grid>
                    </Grid>
                    <Divider />
                  </React.Fragment>
                );
              })}
          </React.Fragment>
        )}
        <DialogActions>
          <Grid container direction="row" justify="center" alignItems="center">
            <Grid item>
              {this.state.indexOfItems === this.state.numOfTypes - 1 ? (
                <div>
                  {spinnerOrder === 'loading' ? (
                    <Button
                      color="primary"
                      variant="contained"
                      className={classes.button}
                    >
                      <CircularProgress
                        className={classes.circularProgress}
                        size={25}
                      />
                    </Button>
                  ) : (
                    <Grid
                      container
                      direction="row"
                      justify="space-around"
                      alignItems="center"
                      spacing={2}
                    >
                      <Grid item>
                        <Button
                          color="primary"
                          variant="contained"
                          className={classes.button}
                          // onClick={this.handleFinish}
                        >
                          Print
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          color="primary"
                          variant="contained"
                          className={classes.button}
                          onClick={this.handleFinish}
                        >
                          Finish
                        </Button>
                      </Grid>
                    </Grid>
                  )}
                </div>
              ) : (
                <Button
                  color="primary"
                  variant="contained"
                  className={classes.button}
                  onClick={e => this.props.handleNext(e)}
                >
                  Next
                </Button>
              )}
            </Grid>
          </Grid>
        </DialogActions>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    updatePin: pin => dispatch(updatePin(pin)),
    archivedOrder: pin => dispatch(archivedOrder(pin)),
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(StorageLocationListDialog);
