import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { withStyles } from '@material-ui/core/styles';
import { constantsConfig } from '../../config/ConstanstsConfig';
import StorageLocationListDialog from './StorageLocationListDialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';

const styles = theme => ({});

class StorageLocationDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderItems: props.orderItems,
      orderItemsNumber: props.orderItems.length,
      orderId: props.orderId,
      indexOfItems: 0,
    };
  }
  handleNext = e => {
    this.setState({
      indexOfItems: this.state.indexOfItems + 1,
    });
  };
  render() {
    const { sellerId, spinnerOrder, order } = this.props;
    return (
      <React.Fragment>
        <DialogTitle id="form-dialog-title">Order</DialogTitle>
        <DialogContent>
          <StorageLocationListDialog
            order={order}
            sellerId={sellerId}
            spinnerOrder={spinnerOrder}
            numOfTypes={this.state.orderItems.length}
            indexOfItems={this.state.indexOfItems}
            handleNext={this.handleNext}
            orderItem={this.state.orderItems[this.state.indexOfItems]}
            orderId={this.state.orderId}
          />
        </DialogContent>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(StorageLocationDialog);
