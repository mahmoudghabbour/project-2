import React from 'react';
import { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  titleTypography: {
    color: '#8e24aa',
    fontFamily: 'Segoe UI',
    fontWeight: 'bold',
    fontSize: '1.2rem',
  },
  resultTypography: {
    fontFamily: 'Segoe UI',
    fontWeight: 'bold',
    fontSize: '0.8rem',
  },
  itemsTypography: {
    fontFamily: 'Segoe UI',
    fontWeight: 'bold',
    fontSize: '1rem',
  },
});

class OrderDetailsDialog extends Component {
  render() {
    const { classes, orderDetails } = this.props;
    return (
      <React.Fragment>
        <DialogTitle id="alert-dialog-slide-title">Order Details</DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="row"
            justify="space-around"
            alignItems="center"
          >
            <Grid item>
              <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
              >
                <Grid item>
                  <Typography className={classes.titleTypography}>
                    Order Id
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography className={classes.resultTypography}>
                    {this.props.orderDetails.id}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
              >
                <Grid item>
                  <Typography className={classes.titleTypography}>
                    Seller Name
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography className={classes.resultTypography}>
                    {this.props.orderDetails.seller
                      ? this.props.orderDetails.seller.name
                      : null}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
              >
                <Grid item>
                  <Typography className={classes.titleTypography}>
                    Warehouse
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography className={classes.resultTypography}>
                    {this.props.orderDetails.warehouseName}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid
            container
            direction="row"
            justify="space-evenly"
            alignItems="center"
            style={{ marginTop: '1em' }}
          >
            <Grid item>
              <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
              >
                <Grid item>
                  <Typography className={classes.titleTypography}>
                    Seller Location
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography className={classes.resultTypography}>
                    {this.props.orderDetails.sellerLocation}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
              >
                <Grid item>
                  <Typography className={classes.titleTypography}>
                    Order Date
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography className={classes.resultTypography}>
                    {new Intl.DateTimeFormat('en-US', {
                      year: 'numeric',
                      month: '2-digit',
                      day: '2-digit',
                    }).format(this.props.orderDetails.createdTimestamp)}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Divider />
          {/* Items */}
          <React.Fragment>
            {this.props.orderDetails.items &&
              this.props.orderDetails.items.map(item => {
                return (
                  <Grid
                    container
                    direction="column"
                    justifyContent="center"
                    alignItems="flex-start"
                  >
                    <Grid item>
                      <Typography className={classes.itemsTypography}>
                        name: {item.name}
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography className={classes.itemsTypography}>
                        SKU: {item.sku}
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography className={classes.itemsTypography}>
                        date:{' '}
                        {new Intl.DateTimeFormat('en-US', {
                          year: 'numeric',
                          month: '2-digit',
                          day: '2-digit',
                        }).format(item.date)}
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography className={classes.itemsTypography}>
                        quantity: {item.quantity}
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography className={classes.itemsTypography}>
                        dimenesions:{' '}
                        {item.dimenesions.length +
                          '*' +
                          item.dimenesions.width +
                          '*' +
                          item.dimenesions.height}
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography className={classes.itemsTypography}>
                        Description: {item.description}
                      </Typography>
                    </Grid>
                  </Grid>
                );
              })}
          </React.Fragment>
        </DialogContent>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(OrderDetailsDialog);
