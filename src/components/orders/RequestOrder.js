import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import firebase from 'firebase/app';
import { constantsConfig } from 'config/ConstanstsConfig';
import { requestOrder } from '../../store/actions/orders/OrdersActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import ChooseItems from './ChooseItems';
import { createNotification } from '../../store/actions/shipments/ShipmentsActions';

const styles = theme => ({
  textField: {
    width: '100%',
  },
  gridItem: {
    width: '100%',
  },
  inputDate: {
    padding: 5,
    marginTop: '0.5em',
    fontSize: '1rem',
    width: '95%',
    border: '1px solid',
    borderRadius: '0.5em',
    // borderColor:
  },
  circularProgress: {
    color: 'white',
  },
});

class RequestOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refNum: null,
      seller: null,
      sellerId: props.sellerId,
      sellerLocation: '',
      items: [],
      openItemsDialog: false,
    };
  }

  componentDidMount() {
    this.handleGetProfile(this.props.sellerId);
  }

  handleGetProfile = async sellerId => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionUsers)
      .where('id', '==', sellerId);
    const snapshot = await query.get();
    const sellerProfile = snapshot.docs.map(doc => doc.data());
    this.setState({
      seller: sellerProfile[0],
    });
  };

  handleOpenItemsDialog = () => {
    this.setState({
      openItemsDialog: true,
    });
  };

  handleCloseItemsDialog = () => {
    this.setState({
      openItemsDialog: false,
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleChangeItems = (e, item) => {
    this.state.items.push(item);
  };

  handleUpdateItems = (e, newItems) => {
    this.setState({
      items: newItems,
    });
  };

  handleSubmit = () => {
    var updateOrder = {
      items: this.state.items,
      sellerId: this.state.sellerId,
      seller: this.state.seller,
      sellerLocation: this.state.sellerLocation,
      warehouseNum: 1,
      warehouseName: 'Al-Riyad warehouse',
      warehouseId: 'xlYgGqYUTDKMslCv9ois',
    };
    this.props.requestOrder(updateOrder);
    var updateNotification = {
      clickTimestamp: 0,
      notificationType: 'order',
      userId: 'oe5cL2oeoPbe8Ny4pr8s0kPHL633',
    };
    this.props.createNotification(updateNotification);
  };

  render() {
    const { classes, itemsCards } = this.props;
    return (
      <React.Fragment>
        <Dialog
          open={this.state.openItemsDialog}
          onClose={this.handleCloseItemsDialog}
          aria-labelledby="draggable-dialog-title"
        >
          <ChooseItems
            itemsCards={itemsCards}
            handleUpdateItems={this.handleUpdateItems}
            items={this.state.items}
            handleChangeItems={this.handleChangeItems}
            handleCloseItemsDialog={this.handleCloseItemsDialog}
          />
        </Dialog>
        <DialogTitle id="form-dialog-title">Order Request</DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item className={classes.gridItem}>
              <TextField
                id="sellerLocation"
                label="Seller location"
                className={classes.textField}
                onChange={this.handleChange}
              />
            </Grid>
            {/* <Grid item className={classes.gridItem}>
              <TextField
                select
                label="Warehouse"
                className={classes.textField}
                // onChange={e =>
                //   this.handleChangeWarehouse(
                //     e,
                //     warehouse.warehouseName,
                //     warehouse.warehouseNum,
                //     warehouse.id
                //   )
                // }
              >
                {warehouses &&
                  warehouses.map(warehouse => (
                    <MenuItem
                      value={warehouse}
                      onClick={e =>
                        this.handleChangeWarehouse(
                          e,
                          warehouse.warehouseName,
                          warehouse.warehouseNum,
                          warehouse.id
                        )
                      }
                    >
                      {warehouse.warehouseName}
                    </MenuItem>
                  ))}
              </TextField>
            </Grid> */}
            <Grid item>
              <Button
                variant="outlined"
                color="primary"
                style={{ marginTop: '0.5em' }}
                onClick={this.handleOpenItemsDialog}
              >
                Choose Items
              </Button>
              {this.state.openItemsDialog === false &&
              this.state.items.length != 0 ? (
                <Typography align="center">Selected items</Typography>
              ) : null}
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          {this.props.spinnerOrder === 'loading' ? (
            <Button variant="contained" color="primary">
              <CircularProgress
                className={classes.circularProgress}
                size={25}
              />
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleSubmit}
            >
              Create
            </Button>
          )}
        </DialogActions>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    itemsCards: state.firestore.ordered.itemsCards,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    requestOrder: creds => dispatch(requestOrder(creds)),
    createNotification: notification =>
      dispatch(createNotification(notification)),
  };
};

export default compose(
  withStyles(styles),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionItemsCards,
        where: ['sellerId', '==', props.sellerId],
        storeAs: 'itemsCards',
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(RequestOrder);
