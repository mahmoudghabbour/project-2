import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Card from 'components/Card/Card.js';
import CardHeader from 'components/Card/CardHeader.js';
import CardBody from 'components/Card/CardBody.js';
import GridContainer from 'components/Grid/GridContainer.js';
import Fab from '@material-ui/core/Fab';
import Dialog from '@material-ui/core/Dialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import AddIcon from '@material-ui/icons/Add';
import OrdersTable from 'components/Table/OrdersTable';
import RequestOrder from './RequestOrder';
import { constantsConfig } from 'config/ConstanstsConfig';
import StorageLocationDialog from './StorageLocationDialog';
import OrdersUserTable from 'components/Table/OrdersUserTable';
import OrderDetailsDialog from './OrderDetailsDialog';
import { removeOrder } from '../../store/actions/orders/OrdersActions';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const styles = theme => ({
  fab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
    zIndex: 2,
  },
  textField: {
    width: '100%',
  },
  gridItem: {
    width: '100%',
  },
  gridCircularProgress: {
    minHeight: '100vh',
  },
});

class OrdersList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      isOpenOrderRequest: false,
      spinnerOrder: false,
      openSnackbar: false,
      isOpenStorageLocationDialog: false,
      orderId: null,
      seller: null,
      sellerProfile: null,
      order: null,
      openDetailsDialog: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.spinnerOrder === 'loading') {
      this.setState({
        spinnerOrder: true,
      });
    }
    if (nextProps.spinnerOrder === 'successful') {
      this.handleCloseOrderRequest();
      this.handleCloseStorageLocationDialog();
      this.setState({
        spinnerOrder: false,
        openSnackbar: true,
      });
    }
  }

  handleChangeTabs = (event, newValue) => {
    this.setState({
      value: newValue,
    });
  };

  handleOpenOrderRequest = () => {
    this.setState({
      isOpenOrderRequest: true,
    });
  };

  handleCloseOrderRequest = () => {
    this.setState({
      isOpenOrderRequest: false,
    });
  };

  handleCloseSnackbar = () => {
    this.setState({
      openSnackbar: false,
    });
  };

  handleCloseStorageLocationDialog = () => {
    this.setState({
      isOpenStorageLocationDialog: false,
    });
  };

  handleOpenDetailsDialog = (e, orderDetails) => {
    this.setState({
      orderDetails: orderDetails,
      openDetailsDialog: true,
    });
  };

  handleCloseDetailsDialog = () => {
    this.setState({
      openDetailsDialog: false,
    });
  };

  handleRemoveOrderRequest = (e, orderId) => {
    this.props.removeOrder(orderId);
  };

  handleOpenStorageLocationDialog = (
    e,
    orderId,
    orderItems,
    seller,
    sellerId,
    order
  ) => {
    this.setState({
      isOpenStorageLocationDialog: true,
      orderItems: orderItems,
      orderId: orderId,
      seller: seller,
      sellerId: sellerId,
      order: order,
    });
  };

  render() {
    const {
      classes,
      uid,
      myOrders,
      spinnerOrder,
      userProfile,
      orderRequests,
    } = this.props;
    if (userProfile)
      return (
        <React.Fragment>
          <Dialog
            fullWidth={true}
            open={this.state.isOpenOrderRequest}
            onClose={this.handleCloseOrderRequest}
          >
            <RequestOrder sellerId={uid} spinnerOrder={spinnerOrder} />
          </Dialog>
          <Dialog
            fullWidth={true}
            open={this.state.isOpenStorageLocationDialog}
            onClose={this.handleCloseStorageLocationDialog}
          >
            <StorageLocationDialog
              order={this.state.order}
              spinnerOrder={spinnerOrder}
              sellerId={this.state.sellerId}
              sellerProfile={this.state.sellerProfile}
              orderItems={this.state.orderItems}
              handleCloseStorageLocationDialog={
                this.handleCloseStorageLocationDialog
              }
            />
          </Dialog>
          <Dialog
            fullWidth={true}
            open={this.state.openDetailsDialog}
            onClose={this.handleCloseDetailsDialog}
          >
            <OrderDetailsDialog orderDetails={this.state.orderDetails} />
          </Dialog>
          <Snackbar
            open={this.state.openSnackbar}
            autoHideDuration={6000}
            onClose={this.handleCloseSnackbar}
          >
            <Alert onClose={this.handleCloseSnackbar} severity="success">
              Done
            </Alert>
          </Snackbar>
          {userProfile.isAdmin ? (
            <React.Fragment>
              <AppBar position="static" color="default">
                <Tabs
                  value={this.state.value}
                  onChange={this.handleChangeTabs}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  aria-label="full width tabs example"
                >
                  <Tab label="Orders Requests" {...a11yProps(0)} />
                  <Tab label="My orders" {...a11yProps(1)} />
                </Tabs>
              </AppBar>
              <TabPanel value={this.state.value} index={0}>
                <GridContainer>
                  <Card plain>
                    <CardHeader plain color="primary">
                      <h4 className={classes.cardTitleWhite}>
                        Orders Requests
                      </h4>
                      <p className={classes.cardCategoryWhite}>
                        Propratise is Table
                      </p>
                    </CardHeader>
                    <CardBody>
                      <OrdersTable
                        tableHeaderColor="primary"
                        tableHead={[
                          'Order ID',
                          'Seller',
                          'Seller Location',
                          'warehouse',
                          '',
                          '',
                          '',
                        ]}
                        tableData={orderRequests}
                        handleOpenStorageLocationDialog={
                          this.handleOpenStorageLocationDialog
                        }
                        handleOpenDetailsDialog={this.handleOpenDetailsDialog}
                        handleRemoveOrderRequest={this.handleRemoveOrderRequest}
                      />
                    </CardBody>
                  </Card>
                </GridContainer>
              </TabPanel>
              <TabPanel value={this.state.value} index={1}>
                <GridContainer>
                  <Card plain>
                    <CardHeader plain color="primary">
                      <h4 className={classes.cardTitleWhite}>Orders List</h4>
                      <p className={classes.cardCategoryWhite}>
                        Propratise is Table
                      </p>
                    </CardHeader>
                    <CardBody>
                      <OrdersUserTable
                        tableHeaderColor="primary"
                        tableHead={[
                          'Order ID',
                          'Seller',
                          'Seller Location',
                          'warehouse',
                          '',
                        ]}
                        tableData={myOrders}
                        handleOpenDetailsDialog={this.handleOpenDetailsDialog}
                      />
                    </CardBody>
                  </Card>
                </GridContainer>
              </TabPanel>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <GridContainer>
                <Card plain>
                  <CardHeader plain color="primary">
                    <h4 className={classes.cardTitleWhite}>Orders List</h4>
                    <p className={classes.cardCategoryWhite}>
                      Propratise is Table
                    </p>
                  </CardHeader>
                  <CardBody>
                    <OrdersUserTable
                      tableHeaderColor="primary"
                      tableHead={[
                        'Order ID',
                        'Seller',
                        'Seller Location',
                        'warehouse',
                        '',
                      ]}
                      tableData={myOrders}
                      handleOpenDetailsDialog={this.handleOpenDetailsDialog}
                    />
                  </CardBody>
                </Card>
              </GridContainer>
            </React.Fragment>
          )}
          <Fab
            aria-label="add"
            className={classes.fab}
            color="primary"
            onClick={this.handleOpenOrderRequest}
          >
            <AddIcon />
          </Fab>
        </React.Fragment>
      );
    else
      return (
        <div>
          <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            className={classes.gridCircularProgress}
          >
            <Grid item xs={3}>
              <CircularProgress className={classes.circularProgress} />
            </Grid>
          </Grid>
        </div>
      );
  }
}

const mapStateToProps = (state, props) => {
  return {
    uid: state.firebase.auth.uid,
    spinnerOrder: state.ordersReducer.spinnerOrder,
    myOrders: state.firestore.ordered.myOrders
      ? state.firestore.ordered.myOrders
      : null,
    orderRequests: state.firestore.ordered.orderRequests
      ? state.firestore.ordered.orderRequests
      : null,
    userProfile: state.firestore.ordered.userProfile
      ? state.firestore.ordered.userProfile[0]
      : null,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    removeOrder: orderId => dispatch(removeOrder(orderId)),
  };
};

export default compose(
  // firestoreConnect(props => {
  //   return [
  //     {
  //       collection: constantsConfig.collectionWarehouses,
  //     },
  //   ];
  // }),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionUsers,
        where: ['id', '==', props.auth.uid],
        storeAs: 'userProfile',
      },
    ];
  }),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionOrders,
        storeAs: 'orderRequests',
      },
    ];
  }),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionOrdersArchived,
        where: ['sellerId', '==', props.auth.uid],
        storeAs: 'myOrders',
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(OrdersList);
