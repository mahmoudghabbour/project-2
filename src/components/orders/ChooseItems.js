import React from 'react';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import ChooseItemsSummary from './ChooseItemsSummary';

function ChooseItems(props) {
  const {
    itemsCards,
    handleChangeItems,
    items,
    handleUpdateItems,
    handleCloseItemsDialog,
  } = props;
  const [checked, setChecked] = React.useState(false);

  const handleChange = event => {
    setChecked(event.target.checked);
  };
  return (
    <React.Fragment>
      <DialogTitle id="form-dialog-title">Select items</DialogTitle>
      <DialogContent>
        {itemsCards &&
          itemsCards.map(item => {
            return (
              <ChooseItemsSummary
                item={item}
                checked={checked}
                items={items}
                handleChange={handleChange}
                handleChangeItems={handleChangeItems}
                handleUpdateItems={handleUpdateItems}
              />
            );
          })}
      </DialogContent>
      <DialogActions>
        <Button
          color="primary"
          variant="contained"
          onClick={handleCloseItemsDialog}
        >
          Update
        </Button>
      </DialogActions>
    </React.Fragment>
  );
}

export default ChooseItems;
