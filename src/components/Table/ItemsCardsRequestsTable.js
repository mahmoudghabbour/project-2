import React from 'react';
import PropTypes from 'prop-types';
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';
// core components
import styles from 'assets/jss/material-dashboard-react/components/tableStyle.js';

// const useStyles = makeStyles(styles);

const useStyles = makeStyles(theme => ({
  tableHeadCell: {
    color: '#8e24aa',
    fontFamily: 'Segoe UI',
    fontWeight: 'bolder',
  },
  acceptButton: {
    backgroundColor: '#4caf50',
    color: 'white',
  },
  rejectButton: {
    color: 'white',
    backgroundColor: '#f50057',
  },
}));

export default function ItemsCardsRequestsTable(props) {
  const classes = useStyles();
  const { tableHead, tableData, tableHeaderColor } = props;
  return (
    <div className={classes.tableResponsive}>
      <Table className={classes.table}>
        {tableHead !== undefined ? (
          <TableHead className={classes[tableHeaderColor + 'TableHeader']}>
            <TableRow className={classes.tableHeadRow}>
              {tableHead.map((prop, key) => {
                return (
                  <TableCell
                    className={classes.tableCell + ' ' + classes.tableHeadCell}
                    key={key}
                  >
                    {prop}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
        ) : null}
        <TableBody>
          {tableData &&
            tableData.map((prop, key) => {
              let date = new Intl.DateTimeFormat('en-US', {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit',
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit',
              }).format(prop.date);
              return (
                <TableRow key={key} className={classes.tableBodyRow}>
                  <TableCell className={classes.tableCell}>
                    {prop.itemSerialNumber}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.sku}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.name}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.description}
                  </TableCell>
                  {prop.dimenesions ? (
                    <TableCell className={classes.tableCell}>
                      {prop.dimenesions.length +
                        '*' +
                        prop.dimenesions.width +
                        '*' +
                        prop.dimenesions.height}
                    </TableCell>
                  ) : null}
                  <TableCell className={classes.tableCell}>{date}</TableCell>

                  <TableCell className={classes.tableCell}>
                    <Button
                      variant="contained"
                      className={classes.acceptButton}
                      onClick={e => props.handleAcceptItemCard(e, prop)}
                    >
                      Accept
                    </Button>
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    <Button
                      variant="contained"
                      className={classes.rejectButton}
                      onClick={e => props.handleRemoveItemCard(e, prop.id)}
                    >
                      Reject
                    </Button>
                  </TableCell>
                </TableRow>
              );
            })}
        </TableBody>
      </Table>
    </div>
  );
}

ItemsCardsRequestsTable.defaultProps = {
  tableHeaderColor: 'gray',
};

ItemsCardsRequestsTable.propTypes = {
  tableHeaderColor: PropTypes.oneOf([
    'warning',
    'primary',
    'danger',
    'success',
    'info',
    'rose',
    'gray',
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
};
