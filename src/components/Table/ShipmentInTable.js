import React from 'react';
import PropTypes from 'prop-types';
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';

// core components
import styles from 'assets/jss/material-dashboard-react/components/tableStyle.js';

// const useStyles = makeStyles(styles);
const useStyles = makeStyles(theme => ({
  tableHeadCell: {
    color: '#8e24aa',
    fontFamily: 'Segoe UI',
    fontWeight: 'bolder',
  },
  acceptButton: {
    backgroundColor: '#4caf50',
    color: 'white',
  },
  rejectButton: {
    color: 'white',
    backgroundColor: '#f50057',
  },
}));

export default function ShipmentInTable(props) {
  const classes = useStyles();
  const { tableHead, tableData, tableHeaderColor, handleSubmit } = props;
  return (
    <div className={classes.tableResponsive}>
      <Table className={classes.table}>
        {tableHead !== undefined ? (
          <TableHead className={classes[tableHeaderColor + 'TableHeader']}>
            <TableRow className={classes.tableHeadRow}>
              {tableHead.map((prop, key) => {
                return (
                  <TableCell
                    className={classes.tableCell + ' ' + classes.tableHeadCell}
                    key={key}
                  >
                    {prop}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
        ) : null}
        <TableBody>
          {tableData &&
            tableData.map((prop, key) => {
              return (
                <TableRow key={key} className={classes.tableBodyRow}>
                  <TableCell className={classes.tableCell}>
                    {prop.shipmentInSerialNumber}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.seller ? prop.seller.name : null}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.sourceLocation}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.shipmentInDate}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.warehouseName}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.warehouseNum}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    <Button
                      color="primary"
                      variant="contained"
                      onClick={e => props.handleOpenDetailsDialog(e, prop)}
                    >
                      Details
                    </Button>
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    <Button
                      variant="contained"
                      className={classes.acceptButton}
                      onClick={e => props.handleSubmit(e, prop)}
                    >
                      Accept
                    </Button>
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    <Button
                      variant="contained"
                      className={classes.rejectButton}
                      onClick={e =>
                        props.handleRemoveShipmentInRequest(e, prop.id)
                      }
                    >
                      Reject
                    </Button>
                  </TableCell>
                </TableRow>
              );
            })}
        </TableBody>
      </Table>
    </div>
  );
}

ShipmentInTable.defaultProps = {
  tableHeaderColor: 'gray',
};

ShipmentInTable.propTypes = {
  tableHeaderColor: PropTypes.oneOf([
    'warning',
    'primary',
    'danger',
    'success',
    'info',
    'rose',
    'gray',
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
};
