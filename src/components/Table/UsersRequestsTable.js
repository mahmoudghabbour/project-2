import React from 'react';
import PropTypes from 'prop-types';
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';
// core components
import styles from 'assets/jss/material-dashboard-react/components/tableStyle.js';

// const useStyles = makeStyles(styles);
const useStyles = makeStyles(theme => ({
  tableHeadCell: {
    color: '#8e24aa',
    fontFamily: 'Segoe UI',
    fontWeight: 'bolder',
  },
  acceptButton: {
    backgroundColor: '#4caf50',
    color: 'white',
  },
  rejectButton: {
    color: 'white',
    backgroundColor: '#f50057',
  },
}));

export default function UsersRequestsTable(props) {
  const classes = useStyles();
  const { tableHead, tableData, tableHeaderColor } = props;
  return (
    <div className={classes.tableResponsive}>
      <Table className={classes.table}>
        {tableHead !== undefined ? (
          <TableHead className={classes[tableHeaderColor + 'TableHeader']}>
            <TableRow className={classes.tableHeadRow}>
              {tableHead.map((prop, key) => {
                return (
                  <TableCell
                    className={classes.tableCell + ' ' + classes.tableHeadCell}
                    key={key}
                  >
                    {prop}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
        ) : null}
        <TableBody>
          {tableData &&
            tableData.map((prop, key) => {
              return (
                <TableRow key={key} className={classes.tableBodyRow}>
                  <TableCell className={classes.tableCell}>
                    {prop.name}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.email}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.mobile}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.bank ? prop.bank.bankName : null}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.bank ? prop.bank.accountNum : null}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.bank ? prop.bank.managerEmail : null}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.onlineStoreLink}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    <Button onClick={e => props.handleAcceptUser(e, prop)}>
                      Accept
                    </Button>
                  </TableCell>
                </TableRow>
              );
            })}
        </TableBody>
      </Table>
    </div>
  );
}

UsersRequestsTable.defaultProps = {
  tableHeaderColor: 'gray',
};

UsersRequestsTable.propTypes = {
  tableHeaderColor: PropTypes.oneOf([
    'warning',
    'primary',
    'danger',
    'success',
    'info',
    'rose',
    'gray',
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
};
