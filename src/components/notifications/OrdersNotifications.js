import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
// import { createOrder } from 'store/actions/orders/OrdersActions';
import { constantsConfig } from 'config/ConstanstsConfig';
import { firestoreConnect } from 'react-redux-firebase';

class OrdersNotifications extends Component {
  handleAcceptOrder = (e, order) => {};
  render() {
    return (
      <React.Fragment>
        {this.props.orders &&
          this.props.orders.map((order) => {
            return (
              <React.Fragment>
                <Typography>Order Name: {order.orderName} </Typography>
                <Button onClick={(e) => this.handleAcceptOrder(e, order)}>
                  Accept
                </Button>
                <Button>reject</Button>
              </React.Fragment>
            );
          })}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    orders: state.firestore.ordered.ordersRequests
      ? state.firestore.ordered.ordersRequests
      : null
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    // createOrder: (orderData) => dispatch(createOrder(orderData))
  };
};

export default compose(
  firestoreConnect(() => {
    return [
      {
        collection: constantsConfig.collectionOrdersRequest,
        storeAs: 'ordersRequests'
      }
    ];
  }),
  connect(mapStateToProps, mapDispatchToProps)
)(OrdersNotifications);
