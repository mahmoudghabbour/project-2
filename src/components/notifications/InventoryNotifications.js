import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { constantsConfig } from '../../config/ConstanstsConfig';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class InventoryNotifications extends Component {
  render() {
    const { inventoryNotifications } = this.props;
    return (
      <React.Fragment>
        {inventoryNotifications &&
          inventoryNotifications.map((notification) => {
            return (
              <React.Fragment>
                <Typography>Seller: {notification.seller}</Typography>
                <Typography>Receiver: {notification.receiver}</Typography>
                <Button>Accept</Button>
              </React.Fragment>
            );
          })}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    inventoryNotifications: state.firestore.ordered.inventoryNotifications,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    // acceptShipmentinRequest: (request) =>
    //   dispatch(acceptShipmentinRequest(request)),
  };
};

export default compose(
  firestoreConnect((props) => {
    return [
      {
        collection: constantsConfig.collectionInventoryNotifications,
        storeAs: 'inventoryNotifications',
      },
    ];
  }),
  connect(mapStateToProps, mapDispatchToProps)
)(InventoryNotifications);
