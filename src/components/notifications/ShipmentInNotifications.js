import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import Typography from '@material-ui/core/Typography';
import { Button } from '@material-ui/core';
import { constantsConfig } from 'config/ConstanstsConfig';
import { acceptShipmentinRequest } from '../../store/actions/shipments/ShipmentsActions';

class ShipmentInNotifications extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSubmit = (e, notification) => {
    this.props.acceptShipmentinRequest(notification);
  };
  render() {
    const { shipmentsIn } = this.props;
    return (
      <React.Fragment>
        {shipmentsIn &&
          shipmentsIn.map((notification) => {
            return (
              <React.Fragment>
                <Typography>{notification.seller}</Typography>
                <Button onClick={(e) => this.handleSubmit(e, notification)}>
                  Accept
                </Button>
                <Button>reject</Button>
              </React.Fragment>
            );
          })}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    shipmentsIn: state.firestore.ordered.shipmentsIn,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    acceptShipmentinRequest: (request) =>
      dispatch(acceptShipmentinRequest(request)),
  };
};

export default compose(
  firestoreConnect((props) => {
    return [
      {
        collection: constantsConfig.collectionShipmentInRequest,
        storeAs: 'shipmentsIn',
      },
    ];
  }),
  connect(mapStateToProps, mapDispatchToProps)
)(ShipmentInNotifications);
