import React from 'react';
import classNames from 'classnames';
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Hidden from '@material-ui/core/Hidden';
import Poppers from '@material-ui/core/Popper';
import Divider from '@material-ui/core/Divider';
import Menu from '@material-ui/core/Menu';

// @material-ui/icons
import Person from '@material-ui/icons/Person';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Notifications from '@material-ui/icons/Notifications';
import Search from '@material-ui/icons/Search';
// core components
import CustomInput from 'components/CustomInput/CustomInput.js';
import Button from 'components/CustomButtons/Button.js';
import { signOut } from '../../store/actions/auth/AuthActions';
import { constantsConfig } from '../../config/ConstanstsConfig';
import { updateNotification } from '../../store/actions/shipments/ShipmentsActions';

import styles from 'assets/jss/material-dashboard-react/components/headerLinksStyle.js';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';

const useStyles = makeStyles(styles);

function AdminNavbarLinks(props) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const [openNotification, setOpenNotification] = React.useState(null);
  const [openProfile, setOpenProfile] = React.useState(null);
  const handleClickNotification = event => {
    if (openNotification && openNotification.contains(event.target)) {
      setOpenNotification(null);
    } else {
      setOpenNotification(event.currentTarget);
    }
  };
  const handleCloseNotification = () => {
    setOpenNotification(null);
  };
  const handleUpdateNotification = (e, notificationId) => {
    props.updateNotification(notificationId);
  };
  const handleClickProfile = event => {
    if (openProfile && openProfile.contains(event.target)) {
      setOpenProfile(null);
    } else {
      setOpenProfile(event.currentTarget);
    }
  };
  const handleCloseProfile = () => {
    setOpenProfile(null);
  };
  const handleSignOut = () => {
    props.signOut();
  };
  const notifications = props.notifications;
  console.log('props', props);
  return (
    <div>
      {/* <div className={classes.searchWrapper}>
        <CustomInput
          formControlProps={{
            className: classes.margin + ' ' + classes.search
          }}
          inputProps={{
            placeholder: 'Search',
            inputProps: {
              'aria-label': 'Search'
            }
          }}
        />
        <Button color="white" aria-label="edit" justIcon round>
          <Search />
        </Button>
      </div> */}

      <div className={classes.manager}>
        <Button
          color={window.innerWidth > 959 ? 'transparent' : 'white'}
          justIcon={window.innerWidth > 959}
          simple={!(window.innerWidth > 959)}
          aria-owns={openNotification ? 'notification-menu-list-grow' : null}
          aria-haspopup="true"
          onClick={handleClick}
          className={classes.buttonLink}
        >
          <Notifications className={classes.icons} />
          {props.numNotifications &&
          props.auth.uid === 'oe5cL2oeoPbe8Ny4pr8s0kPHL633' ? (
            <span className={classes.notifications}>
              {props.numNotifications}
            </span>
          ) : null}
          <Hidden mdUp implementation="css">
            <p onClick={handleCloseNotification} className={classes.linkText}>
              Notification
            </p>
          </Hidden>
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          {notifications &&
            notifications.map(notification => {
              return (
                <MenuItem
                  onClick={e => handleUpdateNotification(e, notification.id)}
                  style={
                    notification.clickTimestamp == 0
                      ? { backgroundColor: '#8e24aa', color: 'white' }
                      : { backgroundColor: 'white' }
                  }
                  // className={
                  //   notification.clickTimstamp === 0
                  //     ? classes.notificationUnShowed
                  //     : classes.notificationShowed
                  // }
                >
                  {notification.notificationType === 'itemCard' ? (
                    <div>You have a new item card request</div>
                  ) : notification.notificationType === 'shipmentIn' ? (
                    <div>You have a new shipment in request</div>
                  ) : (
                    <div>You have a new order request</div>
                  )}
                </MenuItem>
              );
            })}
        </Menu>
        {/* <Poppers
          open={Boolean(openNotification)}
          anchorEl={openNotification}
          transition
          disablePortal
          className={
            classNames({ [classes.popperClose]: !openNotification }) +
            ' ' +
            classes.popperNav
          }
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="notification-menu-list-grow"
              style={{
                transformOrigin:
                  placement === 'bottom' ? 'center top' : 'center bottom',
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleCloseNotification}>
                  <MenuList role="menu">
                    <MenuItem
                      onClick={handleCloseNotification}
                      className={classes.dropdownItem}
                    >
                      Mike John responded to your email
                    </MenuItem>
                    <MenuItem
                      onClick={handleCloseNotification}
                      className={classes.dropdownItem}
                    >
                      You have 5 new tasks
                    </MenuItem>
                    <MenuItem
                      onClick={handleCloseNotification}
                      className={classes.dropdownItem}
                    >
                      You{"'"}re now friend with Andrew
                    </MenuItem>
                    <MenuItem
                      onClick={handleCloseNotification}
                      className={classes.dropdownItem}
                    >
                      Another Notification
                    </MenuItem>
                    <MenuItem
                      onClick={handleCloseNotification}
                      className={classes.dropdownItem}
                    >
                      Another One
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Poppers> */}
      </div>
      <div className={classes.manager}>
        <Button
          color={window.innerWidth > 959 ? 'transparent' : 'white'}
          justIcon={window.innerWidth > 959}
          simple={!(window.innerWidth > 959)}
          aria-owns={openProfile ? 'profile-menu-list-grow' : null}
          aria-haspopup="true"
          onClick={handleClickProfile}
          className={classes.buttonLink}
        >
          <Person className={classes.icons} />
          <Hidden mdUp implementation="css">
            <p className={classes.linkText}>Profile</p>
          </Hidden>
        </Button>
        <Poppers
          open={Boolean(openProfile)}
          anchorEl={openProfile}
          transition
          disablePortal
          className={
            classNames({ [classes.popperClose]: !openProfile }) +
            ' ' +
            classes.popperNav
          }
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="profile-menu-list-grow"
              style={{
                transformOrigin:
                  placement === 'bottom' ? 'center top' : 'center bottom',
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleCloseProfile}>
                  <MenuList role="menu">
                    <MenuItem
                      onClick={handleCloseProfile}
                      className={classes.dropdownItem}
                    >
                      Profile
                    </MenuItem>
                    {/* <MenuItem
                      onClick={handleCloseProfile}
                      className={classes.dropdownItem}
                    >
                      Settings
                    </MenuItem> */}
                    <Divider light />
                    <MenuItem
                      onClick={handleSignOut}
                      className={classes.dropdownItem}
                    >
                      Logout
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Poppers>
      </div>
    </div>
  );
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
    notifications: state.firestore.ordered.notifications
      ? state.firestore.ordered.notifications
      : null,
    numNotifications: state.firestore.ordered.numNotifications
      ? state.firestore.ordered.numNotifications
        ? state.firestore.ordered.numNotifications.length
        : null
      : null,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    signOut: () => dispatch(signOut()),
    updateNotification: notificationId =>
      dispatch(updateNotification(notificationId)),
  };
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionNotifications,
        orderBy: ['clickTimestamp', 'asc'],
        storeAs: 'notifications',
      },
    ];
  }),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionNotifications,
        where: ['clickTimestamp', '==', 0],
        storeAs: 'numNotifications',
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(AdminNavbarLinks);
