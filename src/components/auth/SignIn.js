import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { signIn } from '../../store/actions/auth/AuthActions';
import { Button, TextField, Typography } from '@material-ui/core';
import {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  CircularProgress,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
const styles = theme => ({
  Form: {
    width: 500,
  },
  circularProgress: {
    color: 'white',
  },
  errorMessage: {
    color: 'red',
    fontWeight: 'bolder',
  },
});

class SignIn extends Component {
  state = {
    email: '',
    password: '',
  };
  handleToggle = () => {
    this.setState({
      open: this.state.open,
    });
  };
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    var updateSignIn = {
      email: this.state.email,
      password: this.state.password,
    };
    this.props.signIn(updateSignIn);
  };
  render() {
    const {
      authError,
      auth,
      spinner,
      isSuccessfulSignedIn,
      classes,
    } = this.props;
    if (isSuccessfulSignedIn === true) return <Redirect to="/admin/home" />;
    return (
      <Fragment>
        <DialogTitle id="form-dialog-title" style={{ align: 'center' }}>
          Sign In
        </DialogTitle>
        <DialogContent>
          <TextField
            id="email"
            type="email"
            label="Email"
            onChange={this.handleChange}
            className={classes.Form}
          />
          <br />
          <TextField
            type="password"
            label="Password"
            id="password"
            onChange={this.handleChange}
            className={classes.Form}
          />
        </DialogContent>
        <Typography align="center" className={classes.errorMessage}>
          {authError ? <p>{authError}</p> : null}
        </Typography>
        <DialogActions>
          {spinner !== true ? (
            <Button
              color="primary"
              variant="contained"
              onClick={this.handleSubmit}
            >
              LogIn
            </Button>
          ) : (
            <Button color="primary" variant="contained">
              <CircularProgress
                className={classes.circularProgress}
                size={25}
              />
            </Button>
          )}
        </DialogActions>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth,
    spinner: state.auth.spinner,
    authError: state.auth.authError,
    isSuccessfulSignedIn: state.auth.isSuccessfulSignedIn,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signIn: creds => dispatch(signIn(creds)),
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SignIn)
);
