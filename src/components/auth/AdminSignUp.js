import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { adminSignUp } from '../../store/actions/auth/AuthActions';
import { Button, TextField } from '@material-ui/core';
import {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/style.css';
import DatePicker from 'react-date-picker';

const styles = theme => ({
  Form: {
    width: 500,
  },
});

class AdminSignUp extends Component {
  state = {
    name: '',
    email: '',
    mobile: '',
    role: '',
    warehouse: '',
    birthDate: '',
    password: '',
  };
  handleToggle = () => {
    this.setState({
      open: this.state.open,
    });
  };
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };
  handleChangeMobile = e => {
    this.setState({
      mobile: e,
    });
  };
  handleChangeBirthDate = e => {
    this.setState({
      birthDate: e,
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    var updateSignUp = {
      name: this.state.name,
      email: this.state.email,
      mobile: parseInt(this.state.mobile),
      role: this.state.role,
      warehouse: this.state.warehouse,
      birthDate: this.state.birthDate,
    };
    var password = this.state.password;
    this.props.adminSignUp({ updateSignUp, password });
  };
  render() {
    const { auth, authError, classes } = this.props;
    // if (auth.uid) return <Redirect to="/" />;
    return (
      <Fragment>
        <DialogTitle id="form-dialog-title">Create Admin</DialogTitle>
        <DialogContent>
          <DialogContentText>Please fill out the form below.</DialogContentText>
          <TextField
            id="name"
            type="name"
            label="Name"
            onChange={this.handleChange}
            className={classes.Form}
          />
          <br />
          <TextField
            id="email"
            type="email"
            label="Email"
            onChange={this.handleChange}
            className={classes.Form}
          />
          <br />
          <PhoneInput
            placeholder="Enter phone number"
            onChange={this.handleChangeMobile}
          />
          <TextField
            id="role"
            type="text"
            label="Role"
            onChange={this.handleChange}
            className={classes.Form}
          />
          <TextField
            id="warehouse"
            type="text"
            label="Warehouse"
            onChange={this.handleChange}
            className={classes.Form}
          />
          <DatePicker
            value={this.state.birthDate}
            onChange={this.handleChangeBirthDate}
          />
          <br />
          <TextField
            id="password"
            type="password"
            label="Password"
            onChange={this.handleChange}
            className={classes.Form}
          />
        </DialogContent>
        <DialogActions>
          <Button color="primary" variant="raised" onClick={this.handleSubmit}>
            SignUp
          </Button>
        </DialogActions>
        <div className="center red-text">
          {authError ? <p>{authError}</p> : null}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    adminSignUp: creds => dispatch(adminSignUp(creds)),
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AdminSignUp)
);
