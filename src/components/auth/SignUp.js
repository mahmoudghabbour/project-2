import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { usersRequests } from '../../store/actions/auth/AuthActions';
import { Button, TextField } from '@material-ui/core';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
  CircularProgress,
} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import 'react-phone-number-input/style.css';
import ReactPhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/material.css';
import 'react-phone-number-input/style.css';
const styles = theme => ({
  textField: {
    width: '100%',
  },
  gridItem: {
    width: '100%',
    marginTop: '0.5em',
  },
  circularProgress: {
    color: 'white',
  },
  errorMessage: {
    color: 'red',
    fontWeight: 'bolder',
  },
});

var CryptoJS = require('crypto-js');

class SignUp extends Component {
  state = {
    name: '',
    email: '',
    mobile: '',
    cities: '',
    onlineStoreLink: '',
    companyName: '',
    password: '',
    bank: {
      bankName: '',
      accountNum: 0,
      bankFee: '',
      ibanNumber: 0,
      managerMobile: 0,
      managerEmail: '',
    },
  };
  handleToggle = () => {
    this.setState({
      open: this.state.open,
    });
  };
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };
  handleChangeMobile = (e, type) => {
    if (type === 'user')
      this.setState({
        mobile: e,
      });
    else
      this.setState({
        managerMobile: e,
      });
  };
  handleChangeManagerMobile = e => {
    this.setState({
      managerMobile: e,
    });
  };
  handleChangeBirthDate = e => {
    this.setState({
      birthDate: e,
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    var ciphertext = CryptoJS.AES.encrypt(
      JSON.stringify(this.state.password),
      'my-secret-key@123'
    ).toString();
    var updateSignUp = {
      name: this.state.name,
      email: this.state.email,
      password: ciphertext,
      mobile: parseInt(this.state.mobile),
      cities: this.state.cities,
      onlineStoreLink: this.state.onlineStoreLink,
      companyName: this.state.companyName,
      bank: {
        bankName: this.state.bankName,
        accountNum: parseInt(this.state.accountNum),
        bankFee: this.state.bankFee,
        ibanNumber: parseInt(this.state.ibanNumber),
        managerMobile: parseInt(this.state.managerMobile),
        managerEmail: this.state.managerEmail,
      },
    };
    var password = this.state.password;
    this.props.usersRequests(updateSignUp);
  };
  render() {
    const {
      auth,
      authError,
      isSuccessfulSignedIn,
      spinner,
      classes,
    } = this.props;
    // if (auth.uid) return <Redirect to="/" />;
    return (
      <Fragment>
        <DialogTitle id="form-dialog-title">Sign Up</DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item className={classes.gridItem}>
              <TextField
                id="name"
                type="name"
                label="Name"
                onChange={this.handleChange}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                id="email"
                type="email"
                label="Email"
                onChange={this.handleChange}
                className={classes.textField}
              />
            </Grid>
            <Grid
              item
              className={classes.gridItem}
              style={{ marginTop: '2em' }}
            >
              <ReactPhoneInput
                placeholder="Enter phone number"
                defaultCountry={'us'}
                onChange={e => this.handleChangeMobile(e, 'user')}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                id="cities"
                type="text"
                label="Cities"
                onChange={this.handleChange}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                id="onlineStoreLink"
                type="text"
                label="Online store link"
                onChange={this.handleChange}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                id="companyName"
                type="text"
                label="Company Name"
                onChange={this.handleChange}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                id="bankName"
                type="text"
                label="Bank Name"
                onChange={this.handleChange}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                id="accountNum"
                type="number"
                label="Account Num"
                onChange={this.handleChange}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                id="bankFee"
                type="text"
                label="Bank Fee"
                onChange={this.handleChange}
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                id="ibanNumber"
                type="text"
                label="Iban Number"
                onChange={this.handleChange}
                className={classes.textField}
              />
            </Grid>
            <Grid
              item
              className={classes.gridItem}
              style={{ marginTop: '2em' }}
            >
              <Typography align="left"></Typography>
              <ReactPhoneInput
                label="Manager Number"
                placeholder="Enter manager number"
                defaultCountry={'us'}
                onChange={e => this.handleChangeMobile(e, 'manager')}
              />
            </Grid>
            {/* <DatePicker
            value={this.state.birthDate}
            onChange={this.handleChangeBirthDate}
          /> */}
            <Grid item className={classes.gridItem}>
              <TextField
                id="managerEmail"
                type="text"
                label="Manager E-mail"
                onChange={this.handleChange}
                margin="normal"
                className={classes.textField}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                id="password"
                type="password"
                label="Password"
                onChange={this.handleChange}
                className={classes.textField}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <Typography align="center" className={classes.errorMessage}>
          {authError ? <p>{authError}</p> : null}
        </Typography>
        <DialogActions>
          {spinner !== true ? (
            <Button
              color="primary"
              variant="contained"
              onClick={this.handleSubmit}
            >
              Sign Up
            </Button>
          ) : (
            <Button color="primary" variant="contained">
              <CircularProgress
                className={classes.circularProgress}
                size={25}
              />
            </Button>
          )}
        </DialogActions>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth,
    spinner: state.auth.spinner,
    authError: state.auth.authError,
    isSuccessfulSignedIn: state.auth.isSuccessfulSignedIn,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    usersRequests: creds => dispatch(usersRequests(creds)),
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SignUp)
);
