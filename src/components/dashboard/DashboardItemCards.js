import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import ItemCardsList from 'components/shipments/in/ItemCardsList';

class DashboardItemCards extends Component {
  render() {
    const { auth } = this.props;
    return (
      <React.Fragment>
        <ItemCardsList auth={auth} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardItemCards);
