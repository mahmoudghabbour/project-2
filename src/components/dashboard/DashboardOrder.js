import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import OrdersList from 'components/orders/OrdersList';

class DashboardOrder extends Component {
  render() {
    const { auth } = this.props;
    return (
      <React.Fragment>
        <OrdersList auth={auth} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardOrder);
