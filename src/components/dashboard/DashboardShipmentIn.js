import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import ShipmentInList from 'components/shipments/in/ShipmentInList';

class DashboardShipmentIn extends Component {
  render() {
    const { auth } = this.props;
    return (
      <React.Fragment>
        <ShipmentInList auth={auth} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardShipmentIn);
