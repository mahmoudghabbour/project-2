import React, { Component } from 'react';
import Scanner from './Scanner';
import Result from './Result';

class TestReadBarcode2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scanning: false,
      results: [],
    };
    this._scan = this._scan.bind(this);
    this._onDetected = this._onDetected.bind(this);
  }

  _scan() {
    this.setState({ scanning: !this.state.scanning });
  }

  _onDetected(result) {
    this.setState({ results: this.state.results.concat([result]) });
  }
  render() {
    return (
      <div>
        <button onClick={this._scan}>
          {this.state.scanning ? 'Stop' : 'Start'}
        </button>
        <ul className="results">
          {this.state.results.map(result => {
            return <Result key={result.codeResult.code} result={result} />;
          })}
        </ul>
        {this.state.scanning ? (
          <Scanner onDetected={this.state._onDetected} />
        ) : null}
      </div>
    );
  }
}
export default TestReadBarcode2;
// import React, { useState, useEffect } from 'react';
// import { Text, View, StyleSheet, Button } from 'react-native';
// import { BarCodeScanner } from 'expo-barcode-scanner';

// export default function TestReadBarcode2() {
//   const [hasPermission, setHasPermission] = useState(null);
//   const [scanned, setScanned] = useState(false);

// //   useEffect(() => {
// //     (async () => {
// //       const { status } = await BarCodeScanner.requestPermissionsAsync();
// //       setHasPermission(status === 'granted');
// //     })();
// //   }, []);

//   const handleBarCodeScanned = ({ type, data }) => {
//     setScanned(true);
//     alert(`Bar code with type ${type} and data ${data} has been scanned!`);
//   };

//   if (hasPermission === null) {
//     return <Text>Requesting for camera permission</Text>;
//   }
//   if (hasPermission === false) {
//     return <Text>No access to camera</Text>;
//   }

//   return (
//     <View /*style={styles.container}*/>
//       <BarCodeScanner
//         onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
//         style={StyleSheet.absoluteFillObject}
//       />
//       {scanned && <Button title={'Tap to Scan Again'} onPress={() => setScanned(false)} />}
//     </View>
//   );
// }
