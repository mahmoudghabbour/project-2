import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { constantsConfig } from 'config/ConstanstsConfig';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import ChooseItems from './ChooseItems';
import {
  requestShipmentIn,
  createNotification,
} from '../../../store/actions/shipments/ShipmentsActions';

const styles = theme => ({
  textField: {
    width: '100%',
  },
  gridItem: {
    width: '100%',
  },
  inputDate: {
    padding: 5,
    marginTop: '0.5em',
    fontSize: '1rem',
    width: '95%',
    border: '1px solid',
    borderRadius: '0.5em',
    // borderColor:
  },
  circularProgress: {
    color: 'white',
  },
});

class ShipmentInRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refNum: null,
      seller: '',
      receiver: '',
      warehouseName: '',
      warehouseNum: null,
      warehouseId: null,
      shipmentInDate: null,
      date: null,
      sourceLocation: '',
      mainfest: [],
      openItemsDialog: false,
    };
  }

  handleOpenItemsDialog = () => {
    this.setState({
      openItemsDialog: true,
    });
  };

  handleCloseItemsDialog = () => {
    this.setState({
      openItemsDialog: false,
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleChangeWarehouse = (e, warehouseName, warehouseNum, warehouseId) => {
    this.setState({
      warehouseName: warehouseName,
      warehouseNum: warehouseNum,
      warehouseId: warehouseId,
    });
  };

  handleChangeMainfest = (e, item) => {
    this.state.mainfest.push(item);
  };

  handleUpdateMainfest = (e, newMainfest) => {
    this.setState({
      mainfest: newMainfest,
    });
  };

  handleSubmit = () => {
    var updateShipmentInRequest = {
      refNum: this.state.refNum,
      seller: this.props.userProfile,
      warehouseName: this.state.warehouseName,
      warehouseNum: this.state.warehouseNum,
      warehouseId: this.state.warehouseId,
      shipmentInDate: this.state.shipmentInDate,
      sourceLocation: this.state.sourceLocation,
      sellerId: this.props.auth.uid,
      mainfest: this.state.mainfest,
    };
    this.props.requestShipmentIn(updateShipmentInRequest);
    var updateNotification = {
      clickTimestamp: 0,
      notificationType: 'shipmentIn',
      userId: 'oe5cL2oeoPbe8Ny4pr8s0kPHL633',
    };
    this.props.createNotification(updateNotification);
  };

  render() {
    const { classes, itemsCards, warehouses, shipmentInState } = this.props;
    return (
      <React.Fragment>
        <Dialog
          open={this.state.openItemsDialog}
          onClose={this.handleCloseItemsDialog}
          aria-labelledby="draggable-dialog-title"
        >
          <ChooseItems
            itemsCards={itemsCards}
            handleUpdateMainfest={this.handleUpdateMainfest}
            mainfest={this.state.mainfest}
            handleChangeMainfest={this.handleChangeMainfest}
            handleCloseItemsDialog={this.handleCloseItemsDialog}
          />
        </Dialog>
        <DialogTitle id="form-dialog-title">Shipment In Request</DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item className={classes.gridItem}>
              <TextField
                id="refNum"
                label="Ref Num"
                className={classes.textField}
                onChange={this.handleChange}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                select
                label="Warehouse"
                className={classes.textField}
                // onChange={e =>
                //   this.handleChangeWarehouse(
                //     e,
                //     warehouse.warehouseName,
                //     warehouse.warehouseNum,
                //     warehouse.id
                //   )
                // }
              >
                {warehouses &&
                  warehouses.map(warehouse => (
                    <MenuItem
                      value={warehouse}
                      onClick={e =>
                        this.handleChangeWarehouse(
                          e,
                          warehouse.warehouseName,
                          warehouse.warehouseNum,
                          warehouse.id
                        )
                      }
                    >
                      {warehouse.warehouseName}
                    </MenuItem>
                  ))}
              </TextField>
            </Grid>
            {/* <Grid item className={classes.gridItem}>
              <TextField
                id="warehouse"
                label="Warehouse"
                className={classes.textField}
                onChange={this.handleChange}
              />
            </Grid> */}
            <Grid item className={classes.gridItem}>
              <input
                id="shipmentInDate"
                type="date"
                label="Shipment In Date"
                className={classes.inputDate}
                onChange={this.handleChange}
              />
            </Grid>
            <Grid item className={classes.gridItem}>
              <TextField
                id="sourceLocation"
                label="Source Location"
                className={classes.textField}
                onChange={this.handleChange}
              />
            </Grid>
            <Grid item>
              <Button
                variant="outlined"
                color="primary"
                style={{ marginTop: '0.5em' }}
                onClick={this.handleOpenItemsDialog}
              >
                Choose Items
              </Button>
              {this.state.openItemsDialog === false &&
              this.state.mainfest.length != 0 ? (
                <Typography align="center">Selected items</Typography>
              ) : null}
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          {shipmentInState === 'loading' ? (
            <Button variant="contained" color="primary">
              <CircularProgress
                className={classes.circularProgress}
                size={25}
              />
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleSubmit}
            >
              Create
            </Button>
          )}
        </DialogActions>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    uid: state.firebase.auth.uid,
    itemsCards: state.firestore.ordered.itemsCards
      ? state.firestore.ordered.itemsCards
      : null,
    warehouses: state.firestore.ordered.warehouses,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    requestShipmentIn: request => dispatch(requestShipmentIn(request)),
    createNotification: notification =>
      dispatch(createNotification(notification)),
  };
};

export default compose(
  withStyles(styles),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionItemsCards,
        where: ['sellerId', '==', props.auth.uid],
        storeAs: 'itemsCards',
      },
    ];
  }),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionWarehouses,
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ShipmentInRequest);
