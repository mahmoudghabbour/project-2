import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  titleTypography: {
    color: theme.palette.primary.main,
    fontFamily: 'Segoe UI',
    fontWeight: 'bold',
    fontSize: '1.5rem',
    marginBottom: theme.spacing(2),
  },
  resultTypography: {
    fontFamily: 'Segoe UI',
    fontWeight: 'bold',
    fontSize: '1rem',
    marginBottom: theme.spacing(1),
  },
  itemsTypography: {
    fontFamily: 'Segoe UI',
    fontSize: '1rem',
    marginBottom: theme.spacing(1),
  },
  divider: {
    margin: `${theme.spacing(2)}px 0`,
  },
});

const ShipmentInDetailsDialog = ({ classes, shipmentInDetails }) => {
  return (
    <React.Fragment>
      <DialogTitle className={classes.titleTypography}>
        Shipment In Details
      </DialogTitle>
      <DialogContent>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <Typography className={classes.resultTypography}>
              Ref Num: {shipmentInDetails.refNum}
            </Typography>
          </Grid>
          <Grid item xs={4}>
            <Typography className={classes.resultTypography}>
              Serial Number: {shipmentInDetails.shipmentInSerialNumber}
            </Typography>
          </Grid>
          <Grid item xs={4}>
            <Typography className={classes.resultTypography}>
              Date: {shipmentInDetails.shipmentInDate}
            </Typography>
          </Grid>
        </Grid>

        <Grid container spacing={3} style={{ marginTop: '1em' }}>
          <Grid item xs={6}>
            <Typography className={classes.resultTypography}>
              Seller Name:{' '}
              {shipmentInDetails.seller ? shipmentInDetails.seller.name : null}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography className={classes.resultTypography}>
              Source Location: {shipmentInDetails.sourceLocation}
            </Typography>
          </Grid>
        </Grid>

        <Divider className={classes.divider} />

        {/* Items */}
        {shipmentInDetails.mainfest &&
          shipmentInDetails.mainfest.map((item, index) => (
            <div key={index}>
              <Typography variant="h6" className={classes.itemsTypography}>
                {item.name}
              </Typography>
              <Typography className={classes.itemsTypography}>
                SKU: {item.sku}
              </Typography>
              <Typography className={classes.itemsTypography}>
                Date:{' '}
                {new Intl.DateTimeFormat('en-US', {
                  year: 'numeric',
                  month: '2-digit',
                  day: '2-digit',
                }).format(item.date)}
              </Typography>
              <Typography className={classes.itemsTypography}>
                Quantity: {item.quantity}
              </Typography>
              <Typography className={classes.itemsTypography}>
                Dimensions:{' '}
                {`${item.dimenesions.length} x ${item.dimenesions.width} x ${item.dimenesions.height}`}
              </Typography>
              <Typography className={classes.itemsTypography}>
                Description: {item.description}
              </Typography>
              {index < shipmentInDetails.mainfest.length - 1 && (
                <Divider className={classes.divider} />
              )}
            </div>
          ))}
      </DialogContent>
    </React.Fragment>
  );
};

export default withStyles(styles)(ShipmentInDetailsDialog);
