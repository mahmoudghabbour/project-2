import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import firebase from 'firebase/app';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import Divider from '@material-ui/core/Divider';
import { constantsConfig } from '../../../config/ConstanstsConfig';
import {
  storageItemInWarehouse,
  removeDeliveryRequest,
  updatePin,
} from '../../../store/actions/warehouse/WarehouseAction';

const styles = theme => ({
  gridCircularProgress: {
    minHeight: '10vh',
    backgroundColor: theme.palette.primary.background,
  },
  circularProgress: {
    color: 'white',
  },
  button: {
    width: '10em',
  },
  titleTypography: {
    color: '#8e24aa',
    fontFamily: 'Segoe UI',
    fontWeight: 'bold',
    fontSize: '1.2rem',
  },
  resultTypography: {
    fontFamily: 'Segoe UI',
    fontWeight: 'bold',
    fontSize: '0.8rem',
  },
  itemsTypography: {
    fontFamily: 'Segoe UI',
    fontWeight: 'bold',
    fontSize: '1rem',
  },
});

function isInt(value) {
  return (
    !isNaN(value) &&
    parseInt(Number(value)) == value &&
    !isNaN(parseInt(value, 10))
  );
}

function getArrayMax(array) {
  return Math.max.apply(null, array);
}
function calculateBestQuantity(caseNum, dimensions) {
  var pinHeight = 60,
    pinLength = 60,
    pinWidth = 30,
    itemHeight = dimensions.height,
    itemLength = dimensions.length,
    itemWidth = dimensions.width,
    heightQuantity = 0,
    lengthQuantity = 0,
    widthQuantity = 0,
    totalQuantity = 0;
  if (caseNum === 1) {
    heightQuantity = pinHeight / itemHeight;
    lengthQuantity = pinLength / itemWidth;
    widthQuantity = pinWidth / itemLength;
    totalQuantity = heightQuantity * lengthQuantity * widthQuantity;
    return totalQuantity;
  }
  if (caseNum === 2) {
    heightQuantity = pinHeight / itemHeight;
    lengthQuantity = pinWidth / itemWidth;
    widthQuantity = pinLength / itemLength;
    totalQuantity = heightQuantity * lengthQuantity * widthQuantity;
    return totalQuantity;
  }
  if (caseNum === 3) {
    heightQuantity = pinHeight / itemWidth;
    lengthQuantity = pinWidth / itemHeight;
    widthQuantity = pinLength / itemLength;
    totalQuantity = heightQuantity * lengthQuantity * widthQuantity;
    return totalQuantity;
  }
  if (caseNum === 4) {
    heightQuantity = pinHeight / itemWidth;
    lengthQuantity = pinLength / itemHeight;
    widthQuantity = pinWidth / itemLength;
    totalQuantity = heightQuantity * lengthQuantity * widthQuantity;
    return totalQuantity;
  }
  if (caseNum === 5) {
    heightQuantity = pinHeight / itemLength;
    lengthQuantity = pinLength / itemHeight;
    widthQuantity = pinWidth / itemWidth;
    totalQuantity = heightQuantity * lengthQuantity * widthQuantity;
    return totalQuantity;
  }
  if (caseNum === 6) {
    heightQuantity = pinHeight / itemLength;
    lengthQuantity = pinWidth / itemHeight;
    widthQuantity = pinLength / itemWidth;
    totalQuantity = heightQuantity * lengthQuantity * widthQuantity;
    return totalQuantity;
  }
}

class StorageDialogList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sellerId: props.sellerId,
      warehouseItems: props.warehouseItem,
      shipmentInSerialNumber: props.shipmentInSerialNumber,
      pathOfPin: '',
      num: 0,
      numOfPins: 0,
      numOfTypes: this.props.numOfTypes,
      indexOfItems: this.props.indexOfItems,
      pinsArray: [],
      quantityArray: [],
      maxQuantityForPin: 0,
      numArray: [1, 2, 3, 4, 5, 6],
      spinner: true,
      lastPin: null,
    };
  }
  componentDidMount() {
    var numOfPins = null;
    for (let i = 0; i < 6; i++) {
      const totalQuantity = calculateBestQuantity(
        i + 1,
        this.state.warehouseItems[0].dimenesions
      );
      this.state.quantityArray.push(totalQuantity);
    }
    this.setState({
      maxQuantityForPin: getArrayMax(this.state.quantityArray),
    });
    if (
      this.state.warehouseItems.length < getArrayMax(this.state.quantityArray)
    ) {
      this.setState({
        numOfPins: 1,
      });
      numOfPins = 1;
    } else {
      var numOfPins =
        this.state.warehouseItems.length /
        getArrayMax(this.state.quantityArray);
      if (isInt(numOfPins)) {
        this.setState({
          numOfPins: numOfPins,
        });
        numOfPins = numOfPins;
      } else {
        numOfPins = parseInt(numOfPins);
        numOfPins += 1;
        this.setState({
          numOfPins: numOfPins,
        });
        numOfPins = numOfPins;
      }
    }
    this.handleGetProfile(this.props.sellerId);
    this.handleGetRequests(numOfPins, this.state.warehouseItems);
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.warehouseItems[0].id != nextProps.warehouseItem[0].id) {
      var quantityArray = [];
      this.setState({
        warehouseItems: nextProps.warehouseItem,
        shipmentInSerialNumber: nextProps.shipmentInSerialNumber,
        pathOfPin: '',
        num: 0,
        numOfPins: 0,
        pinsArray: [],
        numOfTypes: nextProps.numOfTypes,
        indexOfItems: nextProps.indexOfItems,
        quantityArray: quantityArray,
        maxQuantityForPin: 0,
        spinner: true,
        lastPin: null,
      });
      var numOfPins = null;
      for (let i = 0; i < 6; i++) {
        const totalQuantity = calculateBestQuantity(
          i + 1,
          nextProps.warehouseItem[0].dimenesions
        );
        quantityArray.push(totalQuantity);
      }
      this.setState({
        maxQuantityForPin: getArrayMax(quantityArray),
      });
      if (nextProps.warehouseItem.length < getArrayMax(quantityArray)) {
        this.setState({
          numOfPins: 1,
        });
        numOfPins = 1;
      } else {
        var numOfPins =
          nextProps.warehouseItem.length / getArrayMax(quantityArray);
        if (isInt(numOfPins)) {
          this.setState({
            numOfPins: numOfPins,
          });
          numOfPins = numOfPins;
        } else {
          numOfPins = parseInt(numOfPins);
          numOfPins += 1;
          this.setState({
            numOfPins: numOfPins,
          });
          numOfPins = numOfPins;
        }
      }
      this.handleGetProfile(this.props.sellerId);
      this.handleGetRequests(numOfPins, nextProps.warehouseItem);
    }
  }

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleGetProfile = async sellerId => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionUsers)
      .where('id', '==', sellerId);
    const snapshot = await query.get();
    const sellerProfile = snapshot.docs.map(doc => doc.data());
    this.setState({
      sellerProfile: sellerProfile[0],
    });
  };

  handleGetRequests = async (numOfPins, warehouseItems) => {
    var pinsArray = [];
    const availablePinsQuery = firebase
      .firestore()
      .collection(constantsConfig.collectionPins)
      .where('sku', '==', warehouseItems[0].sku)
      .where('sellerId', '==', this.state.sellerId);
    const snapshot = await availablePinsQuery.get();
    const availablePins = snapshot.docs.map(doc => doc.data());
    if (availablePins.length > 0) {
      var newNumOfPins = 0;
      for (let l = 0; l < availablePins.length; l++) {
        if (
          availablePins[l].capacity != 0 &&
          availablePins[l].capacity < availablePins[l].maxCapacity
        ) {
          var itemPath =
            'w' +
            1 +
            'p' +
            availablePins[l].pathNum +
            's' +
            availablePins[l].standNum +
            's' +
            availablePins[l].shelveNum +
            'p' +
            availablePins[l].pinNum;
          var pin = availablePins[l];
          pinsArray.push({ itemPath, pin, availableSpace: true });
          break;
        }
      }
      if (pinsArray.length > 0) {
        var remainingItems = warehouseItems.length - pinsArray[0].pin.capacity;
        if (
          remainingItems > 0 &&
          remainingItems < getArrayMax(this.state.quantityArray)
        ) {
          newNumOfPins = 1;
        } else {
          if (remainingItems > 0) {
            var newNumOfPins =
              remainingItems / getArrayMax(this.state.quantityArray);
            if (isInt(newNumOfPins)) {
              newNumOfPins = newNumOfPins;
            } else {
              newNumOfPins = parseInt(newNumOfPins);
              newNumOfPins += 1;
            }
          }
        }
        if (newNumOfPins == 0) {
          this.handleSubmit(pinsArray, warehouseItems);
        } else {
          const pathsQuery = firebase
            .firestore()
            .collection(constantsConfig.collectionPaths)
            .where('warehouseNum', '==', 1)
            .orderBy('pathNum', 'asc');
          const snapshot = await pathsQuery.get();
          const paths = snapshot.docs.map(doc => doc.data());
          for (let i = 0; i < paths.length; i++) {
            const standsQuery = firebase
              .firestore()
              .collection(constantsConfig.collectionStands)
              .where('pathNum', '==', i + 1)
              .orderBy('standNum', 'asc');
            const snapshot = await standsQuery.get();
            const stands = snapshot.docs.map(doc => doc.data());
            for (let j = 0; j < stands.length; j++) {
              if (stands[j].isFull === false) {
                const shelvesQuery = firebase
                  .firestore()
                  .collection(constantsConfig.collectionShelves)
                  .where('standNum', '==', j + 1)
                  .orderBy('shelveNum', 'asc');
                const snapshot = await shelvesQuery.get();
                const shelves = snapshot.docs.map(doc => doc.data());
                for (let k = 0; k < shelves.length; k++) {
                  if (shelves[i].isFull === false) {
                    const pinsQuery = firebase
                      .firestore()
                      .collection(constantsConfig.collectionPins)
                      .where('shelveNum', '==', k + 1)
                      .orderBy('pinNum', 'asc');
                    const snapshot = await pinsQuery.get();
                    const pins = snapshot.docs.map(doc => doc.data());
                    for (let z = 0; z < pins.length; z++) {
                      if (pins[z].isFull === false && pins[z].sku === null) {
                        var itemPath =
                          'w' +
                          1 +
                          'p' +
                          (i + 1) +
                          's' +
                          (j + 1) +
                          's' +
                          (k + 1) +
                          'p' +
                          (z + 1);
                        var pin = pins[z];
                        pinsArray.push({ itemPath, pin });
                        if (pinsArray.length - 1 == newNumOfPins) {
                          this.setState({
                            pinsArray: pinsArray,
                          });
                          this.handleSubmit(pinsArray, warehouseItems);
                          return;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        // if not available pins have capacity
        const pathsQuery = firebase
          .firestore()
          .collection(constantsConfig.collectionPaths)
          .where('warehouseNum', '==', 1)
          .orderBy('pathNum', 'asc');
        const snapshot = await pathsQuery.get();
        const paths = snapshot.docs.map(doc => doc.data());
        for (let i = 0; i < paths.length; i++) {
          const standsQuery = firebase
            .firestore()
            .collection(constantsConfig.collectionStands)
            .where('pathNum', '==', i + 1)
            .orderBy('standNum', 'asc');
          const snapshot = await standsQuery.get();
          const stands = snapshot.docs.map(doc => doc.data());
          for (let j = 0; j < stands.length; j++) {
            if (stands[j].isFull === false) {
              const shelvesQuery = firebase
                .firestore()
                .collection(constantsConfig.collectionShelves)
                .where('standNum', '==', j + 1)
                .orderBy('shelveNum', 'asc');
              const snapshot = await shelvesQuery.get();
              const shelves = snapshot.docs.map(doc => doc.data());
              for (let k = 0; k < shelves.length; k++) {
                if (shelves[i].isFull === false) {
                  const pinsQuery = firebase
                    .firestore()
                    .collection(constantsConfig.collectionPins)
                    .where('shelveNum', '==', k + 1)
                    .orderBy('pinNum', 'asc');
                  const snapshot = await pinsQuery.get();
                  const pins = snapshot.docs.map(doc => doc.data());
                  for (let z = 0; z < pins.length; z++) {
                    if (pins[z].isFull === false && pins[z].sku === null) {
                      var itemPath =
                        'w' +
                        1 +
                        'p' +
                        (i + 1) +
                        's' +
                        (j + 1) +
                        's' +
                        (k + 1) +
                        'p' +
                        (z + 1);
                      var pin = pins[z];
                      pinsArray.push({ itemPath, pin });

                      if (pinsArray.length == numOfPins) {
                        this.setState({
                          pinsArray: pinsArray,
                        });
                        this.handleSubmit(pinsArray, warehouseItems);
                        return;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      const pathsQuery = firebase
        .firestore()
        .collection(constantsConfig.collectionPaths)
        .where('warehouseNum', '==', 1)
        .orderBy('pathNum', 'asc');
      const snapshot = await pathsQuery.get();
      const paths = snapshot.docs.map(doc => doc.data());
      for (let i = 0; i < paths.length; i++) {
        const standsQuery = firebase
          .firestore()
          .collection(constantsConfig.collectionStands)
          .where('pathNum', '==', i + 1)
          .orderBy('standNum', 'asc');
        const snapshot = await standsQuery.get();
        const stands = snapshot.docs.map(doc => doc.data());
        for (let j = 0; j < stands.length; j++) {
          if (stands[j].isFull === false) {
            const shelvesQuery = firebase
              .firestore()
              .collection(constantsConfig.collectionShelves)
              .where('standNum', '==', j + 1)
              .orderBy('shelveNum', 'asc');
            const snapshot = await shelvesQuery.get();
            const shelves = snapshot.docs.map(doc => doc.data());
            for (let k = 0; k < shelves.length; k++) {
              if (shelves[i].isFull === false) {
                const pinsQuery = firebase
                  .firestore()
                  .collection(constantsConfig.collectionPins)
                  .where('shelveNum', '==', k + 1)
                  .orderBy('pinNum', 'asc');
                const snapshot = await pinsQuery.get();
                const pins = snapshot.docs.map(doc => doc.data());
                for (let z = 0; z < pins.length; z++) {
                  if (pins[z].isFull === false && pins[z].sku === null) {
                    var itemPath =
                      'w' +
                      1 +
                      'p' +
                      (i + 1) +
                      's' +
                      (j + 1) +
                      's' +
                      (k + 1) +
                      'p' +
                      (z + 1);
                    var pin = pins[z];
                    pinsArray.push({ itemPath, pin });

                    if (pinsArray.length == numOfPins) {
                      this.setState({
                        pinsArray: pinsArray,
                      });
                      this.handleSubmit(pinsArray, warehouseItems);
                      return;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  };

  handleSubmit = (pinsArray, warehouseItems) => {
    var lastPin = warehouseItems.length;
    for (let i = 0; i < pinsArray.length - 1; i++) {
      if (pinsArray[i].availableSpace) {
        lastPin -= pinsArray[i].pin.capacity;
      } else {
        if (pinsArray[i].pin.id === pinsArray[pinsArray.length - 1].pin.id) {
          // lastPin += this.state.maxQuantityForPin;
        } else lastPin -= this.state.maxQuantityForPin;
      }
    }
    this.setState({
      pinsArray: pinsArray,
      lastPin: lastPin,
      spinner: false,
    });
    let j = 0,
      indexOfPin = 0,
      newIndexOfWarehouseItems = 1;

    if (pinsArray[0].availableSpace) {
      // is available pins
      if (warehouseItems.length <= pinsArray[0].pin.capacity) {
        // all items in one available pin
        for (let i = 0; i < warehouseItems.length; i++) {
          var pin = pinsArray[0].pin;
          var itemPath = pinsArray[0].itemPath;
          var pinId = pinsArray[0].pin.id;
          var sellerId = this.state.sellerId;
          var seller = this.state.sellerProfile;
          var item = {
            barcode: warehouseItems[i].barcode,
            date: warehouseItems[i].date,
            description: warehouseItems[i].description,
            dimenesions: warehouseItems[i].dimenesions,
            itemId: warehouseItems[i].id,
            itemSerialNumber: warehouseItems[i].itemSerialNumber,
            name: warehouseItems[i].name,
            sku: warehouseItems[i].sku,
          };
          var updateWarehouseItem = {
            ...item,
            pinId,
            itemPath,
            sellerId,
            seller,
          };
          this.props.storageItemInWarehouse(updateWarehouseItem);
          if (i === warehouseItems.length - 1) {
            var maxQuantityForPin = pinsArray[0].pin.maxCapacity;
            var warehouseItemsLength =
              warehouseItems.length +
              (pinsArray[0].pin.maxCapacity - pinsArray[0].pin.capacity);
            var sku = warehouseItems[0].sku;
            var updatePin = {
              maxQuantityForPin,
              warehouseItemsLength,
              pinId,
              sku,
              seller,
              sellerId,
            };
            this.props.updatePin(updatePin);
          }
        }
      } else {
        // some items in available pins and others in empty pins
        var newWarehouseItems = [];
        for (
          let i = pinsArray[0].pin.capacity;
          i < warehouseItems.length;
          i++
        ) {
          newWarehouseItems.push(warehouseItems[i]);
        }
        for (let i = 0; i < pinsArray[0].pin.capacity; i++) {
          var pin = pinsArray[0].pin;
          var itemPath = pinsArray[0].itemPath;
          var pinId = pinsArray[0].pin.id;
          var sellerId = this.state.sellerId;
          var seller = this.state.sellerProfile;
          var item = {
            barcode: warehouseItems[i].barcode,
            date: warehouseItems[i].date,
            description: warehouseItems[i].description,
            dimenesions: warehouseItems[i].dimenesions,
            itemId: warehouseItems[i].id,
            itemSerialNumber: warehouseItems[i].itemSerialNumber,
            name: warehouseItems[i].name,
            sku: warehouseItems[i].sku,
          };
          var updateWarehouseItem = {
            ...item,
            pinId,
            itemPath,
            sellerId,
            seller,
          };
          this.props.storageItemInWarehouse(updateWarehouseItem);
          if (i === pinsArray[0].pin.capacity - 1) {
            var maxQuantityForPin = pinsArray[0].pin.maxCapacity;
            var warehouseItemsLength = pinsArray[0].pin.maxCapacity;
            var sku = warehouseItems[0].sku;
            var updatePin = {
              maxQuantityForPin,
              warehouseItemsLength,
              pinId,
              sku,
              seller,
              sellerId,
            };
            this.props.updatePin(updatePin);
          }
        }
        // other items
        if (pinsArray.length - 1 == 1) {
          // if other items need 1 pin or less
          for (let i = 0; i < newWarehouseItems.length; i++) {
            var pin = pinsArray[1].pin;
            var itemPath = pinsArray[1].itemPath;
            var pinId = pinsArray[1].pin.id;
            var sellerId = this.state.sellerId;
            var seller = this.state.sellerProfile;
            var item = {
              barcode: newWarehouseItems[i].barcode,
              date: newWarehouseItems[i].date,
              description: newWarehouseItems[i].description,
              dimenesions: newWarehouseItems[i].dimenesions,
              itemId: newWarehouseItems[i].id,
              itemSerialNumber: newWarehouseItems[i].itemSerialNumber,
              name: newWarehouseItems[i].name,
              sku: newWarehouseItems[i].sku,
            };
            var updateWarehouseItem = {
              ...item,
              pinId,
              itemPath,
              sellerId,
              seller,
            };
            this.props.storageItemInWarehouse(updateWarehouseItem);
            if (i === newWarehouseItems.length - 1) {
              var maxQuantityForPin = this.state.maxQuantityForPin;
              var warehouseItemsLength = newWarehouseItems.length;
              var sku = newWarehouseItems[0].sku;
              var updatePin = {
                maxQuantityForPin,
                warehouseItemsLength,
                pinId,
                sku,
                seller,
                sellerId,
              };
              this.props.updatePin(updatePin);
            }
          }
        } else {
          // if other items need more than 1 pin
          for (let i = 0; i < newWarehouseItems.length; i++) {
            var pin = pinsArray[newIndexOfWarehouseItems].pin;
            var itemPath = pinsArray[newIndexOfWarehouseItems].itemPath;
            var pinId = pinsArray[newIndexOfWarehouseItems].pin.id;
            var sellerId = this.state.sellerId;
            var seller = this.state.sellerProfile;
            var item = {
              barcode: newWarehouseItems[i].barcode,
              date: newWarehouseItems[i].date,
              description: newWarehouseItems[i].description,
              dimenesions: newWarehouseItems[i].dimenesions,
              itemId: newWarehouseItems[i].id,
              itemSerialNumber: newWarehouseItems[i].itemSerialNumber,
              name: newWarehouseItems[i].name,
              sku: newWarehouseItems[i].sku,
            };
            var updateWarehouseItem = {
              ...item,
              pinId,
              itemPath,
              sellerId,
              seller,
            };
            this.props.storageItemInWarehouse(updateWarehouseItem);
            j++;
            if (
              j === this.state.maxQuantityForPin &&
              i != warehouseItems.length - 1
            ) {
              var maxQuantityForPin = this.state.maxQuantityForPin;
              var sku = warehouseItems[0].sku;
              var updatePin = {
                maxQuantityForPin,
                pinId,
                sku,
                seller,
                sellerId,
              };
              this.props.updatePin(updatePin);
              newIndexOfWarehouseItems += 1;
              j = 0;
            }
            if (
              (j === this.state.maxQuantityForPin &&
                i === newWarehouseItems.length - 1) ||
              (i === newWarehouseItems.length - 1 && pinsArray.length - 1)
            ) {
              var maxQuantityForPin = this.state.maxQuantityForPin;
              var sku = warehouseItems[0].sku;
              var updatePin = {
                maxQuantityForPin,
                warehouseItemsLength: j,
                pinId,
                sku,
                seller,
                sellerId,
              };
              this.props.updatePin(updatePin);
            }
          }
        }
      }
    } else {
      // item for the first time
      if (warehouseItems.length <= this.state.maxQuantityForPin) {
        // item in one pin
        for (let i = 0; i < warehouseItems.length; i++) {
          var pin = pinsArray[0].pin;
          var itemPath = pinsArray[0].itemPath;
          var pinId = pinsArray[0].pin.id;
          var sellerId = this.state.sellerId;
          var seller = this.state.sellerProfile;
          var item = {
            barcode: warehouseItems[i].barcode,
            date: warehouseItems[i].date,
            description: warehouseItems[i].description,
            dimenesions: warehouseItems[i].dimenesions,
            itemId: warehouseItems[i].id,
            itemSerialNumber: warehouseItems[i].itemSerialNumber,
            name: warehouseItems[i].name,
            sku: warehouseItems[i].sku,
          };
          var updateWarehouseItem = {
            ...item,
            pinId,
            itemPath,
            sellerId,
            seller,
          };
          this.props.storageItemInWarehouse(updateWarehouseItem);
          if (i === warehouseItems.length - 1) {
            var maxQuantityForPin = this.state.maxQuantityForPin;
            var warehouseItemsLength = warehouseItems.length;
            var sku = warehouseItems[0].sku;
            var updatePin = {
              maxQuantityForPin,
              warehouseItemsLength,
              pinId,
              sku,
              seller,
              sellerId,
            };
            this.props.updatePin(updatePin);
          }
        }
      } else {
        // items in multiples pins
        for (let i = 0; i < warehouseItems.length; i++) {
          var pin = pinsArray[indexOfPin].pin;
          var itemPath = pinsArray[indexOfPin].itemPath;
          var pinId = pinsArray[indexOfPin].pin.id;
          var sellerId = this.state.sellerId;
          var seller = this.state.sellerProfile;
          var item = {
            barcode: warehouseItems[i].barcode,
            date: warehouseItems[i].date,
            description: warehouseItems[i].description,
            dimenesions: warehouseItems[i].dimenesions,
            itemId: warehouseItems[i].id,
            itemSerialNumber: warehouseItems[i].itemSerialNumber,
            name: warehouseItems[i].name,
            sku: warehouseItems[i].sku,
          };
          var updateWarehouseItem = {
            ...item,
            pinId,
            itemPath,
            sellerId,
            seller,
          };
          this.props.storageItemInWarehouse(updateWarehouseItem);
          j++;
          if (
            j === this.state.maxQuantityForPin &&
            i != warehouseItems.length - 1
          ) {
            var maxQuantityForPin = this.state.maxQuantityForPin;
            var sku = warehouseItems[0].sku;
            var updatePin = {
              maxQuantityForPin,
              pinId,
              sku,
              seller,
              sellerId,
            };
            this.props.updatePin(updatePin);
            indexOfPin += 1;
            j = 0;
          }
          if (
            (j === this.state.maxQuantityForPin &&
              i === warehouseItems.length - 1) ||
            (i === warehouseItems.length - 1 && pinsArray.length - 1)
          ) {
            var maxQuantityForPin = this.state.maxQuantityForPin;
            var sku = warehouseItems[0].sku;
            var updatePin = {
              maxQuantityForPin,
              warehouseItemsLength: j,
              pinId,
              sku,
              seller,
              sellerId,
            };
            this.props.updatePin(updatePin);
          }
        }
      }
    }
  };

  handleDelete = () => {
    const newNumArray = [...this.state.numArray];
    newNumArray.splice(1, 1);

    this.setState({
      numArray: newNumArray,
    });
  };

  handleFinish = e => {
    this.props.removeDeliveryRequest(this.state.shipmentInSerialNumber);
  };
  render() {
    const { classes, deliverySpinner } = this.props;
    console.log('this.state', this.state);
    return (
      <React.Fragment>
        <Grid
          container
          direction="row"
          justify="space-around"
          alignItems="center"
        >
          <Grid item>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Typography className={classes.titleTypography}>SKU</Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.resultTypography}>
                  {this.state.warehouseItems[0].sku}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Typography className={classes.titleTypography}>
                  Total quantity
                </Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.resultTypography}>
                  {this.state.warehouseItems.length}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Typography className={classes.titleTypography}>
                  Dimensions
                </Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.resultTypography}>
                  {this.state.warehouseItems[0].dimenesions.height +
                    '*' +
                    this.state.warehouseItems[0].dimenesions.width +
                    '*' +
                    this.state.warehouseItems[0].dimenesions.length}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Grid
          container
          direction="row"
          justify="space-evenly"
          alignItems="center"
          style={{ marginTop: '1em' }}
        >
          <Grid item>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Typography className={classes.titleTypography}>
                  Date
                </Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.resultTypography}>
                  {new Intl.DateTimeFormat('en-US', {
                    year: 'numeric',
                    month: '2-digit',
                    day: '2-digit',
                  }).format(this.state.warehouseItems[0].date)}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Typography className={classes.titleTypography}>
                  Description
                </Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.resultTypography}>
                  {this.state.warehouseItems[0].description}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Typography className={classes.titleTypography}>
                  Max capacity
                </Typography>
              </Grid>
              <Grid item>
                <Typography className={classes.resultTypography}>
                  {this.state.maxQuantityForPin}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Divider />
        {this.state.spinner === true ? (
          <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            className={classes.gridCircularProgress}
          >
            <Grid item xs={3}>
              <CircularProgress className={classes.circularProgress} />
            </Grid>
          </Grid>
        ) : (
          <React.Fragment>
            {this.state.pinsArray &&
              this.state.pinsArray.map(pin => {
                return (
                  <React.Fragment>
                    <Grid
                      container
                      direction="row"
                      justify="space-around"
                      alignItems="center"
                    >
                      <Grid item>
                        <Grid
                          container
                          direction="column"
                          justifyContent="center"
                          alignItems="center"
                        >
                          <Grid item>
                            <Typography className={classes.titleTypography}>
                              Path
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography className={classes.resultTypography}>
                              {pin.pin.pathNum}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid item>
                        <Grid
                          container
                          direction="column"
                          justifyContent="center"
                          alignItems="center"
                        >
                          <Grid item>
                            <Typography className={classes.titleTypography}>
                              Stand
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography className={classes.resultTypography}>
                              {pin.pin.standNum}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="space-around"
                      alignItems="center"
                      style={{ marginLeft: '-0.5em' }}
                    >
                      <Grid item>
                        <Grid
                          container
                          direction="column"
                          justifyContent="center"
                          alignItems="center"
                        >
                          <Grid item>
                            <Typography className={classes.titleTypography}>
                              Shelve
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography className={classes.resultTypography}>
                              {pin.pin.shelveNum}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid item>
                        <Grid
                          container
                          direction="column"
                          justifyContent="center"
                          alignItems="center"
                        >
                          <Grid item>
                            <Typography className={classes.titleTypography}>
                              Bin
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography className={classes.resultTypography}>
                              {pin.pin.pinNum}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="center"
                      alignItems="center"
                    >
                      <Grid item>
                        <Typography
                          align="center"
                          className={classes.titleTypography}
                        >
                          Quantity
                        </Typography>
                        <Typography
                          align="center"
                          className={classes.resultTypography}
                        >
                          {pin.availableSpace
                            ? pin.pin.id ===
                              this.state.pinsArray[
                                this.state.pinsArray.length - 1
                              ].pin.id
                              ? this.state.warehouseItems.length
                              : pin.pin.capacity
                            : pin.pin.id ===
                              this.state.pinsArray[
                                this.state.pinsArray.length - 1
                              ].pin.id
                            ? this.state.lastPin
                            : this.state.maxQuantityForPin}
                        </Typography>
                        {/* <Typography align="center">
                          {pin.availableSpace
                            ? pin.pin.capacity
                            : pin.pin.id ===
                              this.state.pinsArray[
                                this.state.pinsArray.length - 1
                              ].id
                            ? this.state.lastPin
                            : this.state.maxQuantityForPin}
                        </Typography> */}
                      </Grid>
                    </Grid>
                    <Divider />
                  </React.Fragment>
                );
              })}
          </React.Fragment>
        )}
        <DialogActions>
          <Grid container direction="row" justify="center" alignItems="center">
            <Grid item>
              {this.state.indexOfItems === this.state.numOfTypes - 1 ? (
                <div>
                  {deliverySpinner === 'loading' ? (
                    <Button
                      color="primary"
                      variant="contained"
                      className={classes.button}
                    >
                      <CircularProgress
                        className={classes.circularProgress}
                        size={25}
                      />
                    </Button>
                  ) : (
                    <Grid
                      container
                      direction="row"
                      justify="space-around"
                      alignItems="center"
                      spacing={2}
                    >
                      <Grid item>
                        <Button
                          color="primary"
                          variant="contained"
                          className={classes.button}
                          // onClick={this.handleFinish}
                        >
                          Print
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          color="primary"
                          variant="contained"
                          className={classes.button}
                          onClick={this.handleFinish}
                        >
                          Finish
                        </Button>
                      </Grid>
                    </Grid>
                  )}
                </div>
              ) : (
                <Button
                  color="primary"
                  variant="contained"
                  className={classes.button}
                  onClick={e => this.props.handleNext(e)}
                >
                  Next
                </Button>
              )}
            </Grid>
          </Grid>
        </DialogActions>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    storageItemInWarehouse: item => dispatch(storageItemInWarehouse(item)),
    removeDeliveryRequest: shipmentInSerialNumber =>
      dispatch(removeDeliveryRequest(shipmentInSerialNumber)),
    updatePin: pin => dispatch(updatePin(pin)),
  };
};

export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(StorageDialogList);
