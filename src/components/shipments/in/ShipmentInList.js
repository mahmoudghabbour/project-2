import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import PropTypes from 'prop-types';
import { constantsConfig } from '../../../config/ConstanstsConfig';
import { withStyles } from '@material-ui/core/styles';
import ShipmentInTable from '../../Table/ShipmentInTable';
import ShipmentInUserTable from '../../Table/ShipmentInUserTable';
import Card from 'components/Card/Card.js';
import CardHeader from 'components/Card/CardHeader.js';
import CardBody from 'components/Card/CardBody.js';
import GridContainer from 'components/Grid/GridContainer.js';
import Fab from '@material-ui/core/Fab';
import Dialog from '@material-ui/core/Dialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import AddIcon from '@material-ui/icons/Add';
import ShipmentInRequest from './ShipmentInRequest';
import {
  acceptShipmentinRequest,
  removeShipmentInRequest,
} from '../../../store/actions/shipments/ShipmentsActions';
import ShipmentInDetailsDialog from './ShipmentInDetailsDilaog';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const styles = theme => ({
  fab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
    zIndex: 2,
  },
  textField: {
    width: '100%',
  },
  gridItem: {
    width: '100%',
  },
  gridCircularProgress: {
    minHeight: '100vh',
  },
});
class ShipmentInList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenShipmentInRequest: false,
      value: 0,
      receiver: null,
      isOpenReceiverDialog: false,
      receiver: null,
      notification: null,
      openSnackbar: false,
      openDetailsDialog: false,
      shipmentInDetails: null,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.shipmentInState === 'successful') {
      this.handleCloseShipmentInRequest();
      this.setState({
        openSnackbar: true,
      });
    }
  }

  handleOpenShipmentInRequest = () => {
    this.setState({
      isOpenShipmentInRequest: true,
    });
  };

  handleChangeTabs = (event, newValue) => {
    this.setState({
      value: newValue,
    });
  };

  handleCloseShipmentInRequest = () => {
    this.setState({
      isOpenShipmentInRequest: false,
    });
  };

  handleOpenReceiverDialog = (e, notification) => {
    this.setState({
      isOpenReceiverDialog: true,
      notification: notification,
    });
  };

  handleCloseReceiverDialog = () => {
    this.setState({
      isOpenReceiverDialog: false,
    });
  };

  handleCloseSnackbar = () => {
    this.setState({
      openSnackbar: false,
    });
  };

  handleChangeReceiver = (e, receiver) => {
    this.setState({
      receiver: receiver,
    });
  };

  handleOpenDetailsDialog = (e, shipmentInDetails) => {
    this.setState({
      shipmentInDetails: shipmentInDetails,
      openDetailsDialog: true,
    });
  };

  handleCloseDetailsDialog = () => {
    this.setState({
      openDetailsDialog: false,
    });
  };

  handleRemoveShipmentInRequest = (e, shipmentInId) => {
    this.props.removeShipmentInRequest(shipmentInId);
  };

  handleSubmit = (e, notification) => {
    var updateNotification = {
      ...notification,
    };
    this.props.acceptShipmentinRequest(updateNotification);
  };
  render() {
    const {
      classes,
      employees,
      userProfile,
      shipmentInList,
      shipmentInListHistory,
      shipmentInState,
    } = this.props;
    if (userProfile)
      return (
        <React.Fragment>
          <Dialog
            fullWidth={true}
            open={this.state.isOpenReceiverDialog}
            onClose={this.handleCloseReceiverDialog}
          >
            <DialogTitle>Choose Receiver</DialogTitle>
            <DialogContent>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid item className={classes.gridItem}>
                  <TextField
                    select
                    label="Receiver"
                    className={classes.textField}
                    // onChange={e =>
                    //   this.handleChangeWarehouse(
                    //     e,
                    //     warehouse.warehouseName,
                    //     warehouse.warehouseNum,
                    //     warehouse.id
                    //   )
                    // }
                  >
                    {employees &&
                      employees.map(employee => (
                        <MenuItem
                          value={employees}
                          onClick={e => this.handleChangeReceiver(e, employees)}
                        >
                          {employee.name}
                        </MenuItem>
                      ))}
                  </TextField>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleSubmit}>Done</Button>
            </DialogActions>
          </Dialog>
          <Dialog
            fullWidth={true}
            open={this.state.isOpenShipmentInRequest}
            onClose={this.handleCloseShipmentInRequest}
          >
            <ShipmentInRequest
              auth={this.props.auth}
              userProfile={userProfile}
              shipmentInState={shipmentInState}
            />
          </Dialog>
          <Dialog
            fullWidth={true}
            open={this.state.openDetailsDialog}
            onClose={this.handleCloseDetailsDialog}
          >
            <ShipmentInDetailsDialog
              shipmentInDetails={this.state.shipmentInDetails}
            />
          </Dialog>
          {userProfile.isAdmin ? (
            <React.Fragment>
              <Snackbar
                open={this.state.openSnackbar}
                autoHideDuration={6000}
                onClose={this.handleCloseSnackbar}
              >
                <Alert onClose={this.handleCloseSnackbar} severity="success">
                  Done
                </Alert>
              </Snackbar>
              <AppBar position="static" color="default">
                <Tabs
                  value={this.state.value}
                  onChange={this.handleChangeTabs}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  aria-label="full width tabs example"
                >
                  <Tab label="Shipments In Requests" {...a11yProps(0)} />
                  <Tab label="My Shipments In" {...a11yProps(1)} />
                </Tabs>
              </AppBar>
              <TabPanel value={this.state.value} index={0}>
                <GridContainer>
                  <Card plain>
                    <CardHeader plain color="primary">
                      <h4 className={classes.cardTitleWhite}>
                        Shipment In Table
                      </h4>
                      <p className={classes.cardCategoryWhite}>
                        Propratise is Table
                      </p>
                    </CardHeader>
                    <CardBody>
                      <ShipmentInTable
                        tableHeaderColor="primary"
                        tableHead={[
                          'Serial Number',
                          'Seller',
                          'Source Location',
                          'Date',
                          'Warehouse name',
                          'Warehouse num',
                          '',
                          '',
                          '',
                        ]}
                        tableData={shipmentInList}
                        handleOpenReceiverDialog={this.handleOpenReceiverDialog}
                        handleSubmit={this.handleSubmit}
                        handleOpenDetailsDialog={this.handleOpenDetailsDialog}
                        handleRemoveShipmentInRequest={
                          this.handleRemoveShipmentInRequest
                        }
                      />
                    </CardBody>
                  </Card>
                </GridContainer>
              </TabPanel>
              <TabPanel value={this.state.value} index={1}>
                <GridContainer>
                  <Card plain>
                    <CardHeader plain color="primary">
                      <h4 className={classes.cardTitleWhite}>
                        Shipment In Table
                      </h4>
                      <p className={classes.cardCategoryWhite}>
                        Propratise is Table
                      </p>
                    </CardHeader>
                    <CardBody>
                      <ShipmentInUserTable
                        tableHeaderColor="primary"
                        tableHead={[
                          'Serial Number',
                          'Seller',
                          'Source Location',
                          'Date',
                          'warehouse',
                          'Details',
                        ]}
                        tableData={shipmentInListHistory}
                        handleSubmit={this.handleSubmit}
                        handleOpenDetailsDialog={this.handleOpenDetailsDialog}
                      />
                    </CardBody>
                  </Card>
                </GridContainer>
              </TabPanel>
            </React.Fragment>
          ) : (
            <GridContainer>
              <Snackbar
                open={this.state.openSnackbar}
                autoHideDuration={6000}
                onClose={this.handleCloseSnackbar}
              >
                <Alert onClose={this.handleCloseSnackbar} severity="success">
                  Done
                </Alert>
              </Snackbar>
              <Card plain>
                <CardHeader plain color="primary">
                  <h4 className={classes.cardTitleWhite}>Shipment In Table</h4>
                  <p className={classes.cardCategoryWhite}>
                    Propratise is Table
                  </p>
                </CardHeader>
                <CardBody>
                  <ShipmentInUserTable
                    tableHeaderColor="primary"
                    tableHead={[
                      'Serial Number',
                      'Seller',
                      'Source Location',
                      'Date',
                      'warehouse',
                      '',
                    ]}
                    tableData={shipmentInListHistory}
                    handleOpenDetailsDialog={this.handleOpenDetailsDialog}
                    handleOpenDetailsDialog={this.handleOpenDetailsDialog}
                  />
                </CardBody>
              </Card>
            </GridContainer>
          )}
          <Fab
            aria-label="add"
            className={classes.fab}
            color="primary"
            onClick={this.handleOpenShipmentInRequest}
          >
            <AddIcon />
          </Fab>
        </React.Fragment>
      );
    else
      return (
        <div>
          <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            className={classes.gridCircularProgress}
          >
            <Grid item xs={3}>
              <CircularProgress className={classes.circularProgress} />
            </Grid>
          </Grid>
        </div>
      );
  }
}

const mapStateToProps = (state, props) => {
  return {
    employees: state.firestore.ordered.employees,
    shipmentInList: state.firestore.ordered.shipmentInList,
    shipmentInListHistory: state.firestore.ordered.shipmentInListHistory,
    shipmentInState: state.shipments.shipmentInState,
    userProfile: state.firestore.ordered.userProfile
      ? state.firestore.ordered.userProfile[0]
      : null,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    acceptShipmentinRequest: request =>
      dispatch(acceptShipmentinRequest(request)),
    removeShipmentInRequest: shipmentInId =>
      dispatch(removeShipmentInRequest(shipmentInId)),
  };
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionShipmentInRequest,
        storeAs: 'shipmentInList',
      },
    ];
  }),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionShipmentsInHistory,
        where: ['sellerId', '==', props.auth.uid],
        storeAs: 'shipmentInListHistory',
      },
    ];
  }),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionUsers,
        where: ['id', '==', props.auth.uid],
        storeAs: 'userProfile',
      },
    ];
  }),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionEmployees,
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(ShipmentInList);
