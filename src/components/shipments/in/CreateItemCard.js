import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

import {
  createItemCard,
  requestItemCard,
  createNotification,
} from '../../../store/actions/shipments/ShipmentsActions';

const styles = theme => ({
  textField: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  circularProgress: {
    color: 'white',
  },
});

class CreateItemCard extends Component {
  state = {
    name: '',
    sku: '',
    expireDate: null,
    dimensions: {
      height: 0,
      width: 0,
      length: 0,
    },
    description: '',
  };

  // Handle input changes
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  // Handle form submission
  handleSubmit = () => {
    const { name, sku, dimensions, description } = this.state;
    const {
      userProfile,
      createItemCard,
      requestItemCard,
      createNotification,
    } = this.props;

    const updateItemCard = {
      name,
      sku,
      dimensions: {
        height: parseInt(dimensions.height),
        width: parseInt(dimensions.width),
        length: parseInt(dimensions.length),
      },
      description,
      sellerId: userProfile.id,
      seller: { ...userProfile },
    };

    if (userProfile.isAdmin) {
      createItemCard(updateItemCard, true);
    } else {
      const updateNotification = {
        clickTimestamp: 0,
        notificationType: 'itemCard',
        userId: 'oe5cL2oeoPbe8Ny4pr8s0kPHL633', // Update with the actual userId
      };
      requestItemCard(updateItemCard);
      createNotification(updateNotification);
    }
  };

  render() {
    const { classes, createItemSuccessful } = this.props;

    return (
      <React.Fragment>
        <DialogTitle id="form-dialog-title">Create Item Card</DialogTitle>
        <DialogContent>
          <Grid container spacing={2}>
            {this.renderTextField('name', 'Name')}
            {this.renderTextField('sku', 'SKU')}
            {this.renderTextField('description', 'Description', true)}
            <Grid item xs={12}>
              <Typography variant="subtitle2" gutterBottom>
                Dimensions
              </Typography>
            </Grid>
            {this.renderTextField('height', 'Height')}
            {this.renderTextField('width', 'Width')}
            {this.renderTextField('length', 'Length')}
          </Grid>
        </DialogContent>
        <DialogActions>
          {createItemSuccessful === 'loading' ? (
            <Button variant="contained" color="primary" disabled>
              <CircularProgress
                className={classes.circularProgress}
                size={25}
              />
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleSubmit}
            >
              Create
            </Button>
          )}
        </DialogActions>
      </React.Fragment>
    );
  }

  // Helper method to render text fields
  renderTextField = (id, label, multiline = false) => (
    <Grid item xs={12}>
      <TextField
        id={id}
        label={label}
        variant="outlined"
        fullWidth
        multiline={multiline}
        rows={multiline ? 4 : 1}
        onChange={this.handleChange}
        className={this.props.classes.textField}
      />
    </Grid>
  );
}

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    createItemCard: (itemCard, isAdmin) =>
      dispatch(createItemCard(itemCard, isAdmin)),
    requestItemCard: itemCard => dispatch(requestItemCard(itemCard)),
    createNotification: notification =>
      dispatch(createNotification(notification)),
  };
};

export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(CreateItemCard);
