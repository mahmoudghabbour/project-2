import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { withStyles } from '@material-ui/core/styles';
import StorageDialogList from './StorageDialogList';
import { constantsConfig } from '../../../config/ConstanstsConfig';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';

const styles = theme => ({});

class StorageDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      warehouseItems: props.warehouseItems,
      warehouseItemsNumber: props.warehouseItems.length,
      shipmentInSerialNumber: props.shipmentInSerialNumber,
      indexOfItems: 0,
    };
  }
  handleNext = e => {
    // if (this.state.num != this.state.warehouseItemsNumber - 1) {
    this.setState({
      indexOfItems: this.state.indexOfItems + 1,
    });
    // }
  };
  render() {
    const { sellerProfile, sellerId, deliverySpinner } = this.props;
    return (
      <React.Fragment>
        <DialogTitle id="form-dialog-title">Storage</DialogTitle>
        <DialogContent>
          <StorageDialogList
            sellerId={sellerId}
            deliverySpinner={deliverySpinner}
            numOfTypes={this.state.warehouseItems.length}
            indexOfItems={this.state.indexOfItems}
            handleNext={this.handleNext}
            warehouseItem={
              this.state.warehouseItems[this.state.indexOfItems].items
            }
            shipmentInSerialNumber={this.state.shipmentInSerialNumber}
          />
        </DialogContent>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(StorageDialog);
