import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import firebase from 'firebase/app';
import { constantsConfig } from '../../../config/ConstanstsConfig';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';
import Dialog from '@material-ui/core/Dialog';
import GridContainer from 'components/Grid/GridContainer.js';
import Table from 'components/Table/Table.js';
import Card from 'components/Card/Card.js';
import CardHeader from 'components/Card/CardHeader.js';
import CardBody from 'components/Card/CardBody.js';
import AppBar from '@material-ui/core/AppBar';
import Snackbar from '@material-ui/core/Snackbar';
import Grid from '@material-ui/core/Grid';
import MuiAlert from '@material-ui/lab/Alert';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import AddIcon from '@material-ui/icons/Add';
import CreateItemCard from './CreateItemCard';
import ItemsCardsRequestsTable from 'components/Table/ItemsCardsRequestsTable';
import {
  createItemCard,
  removeItemCardRequest,
} from 'store/actions/shipments/ShipmentsActions';
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const styles = theme => ({
  fab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
    zIndex: 2,
  },
  gridCircularProgress: {
    minHeight: '100vh',
  },
});
class ItemCardsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenCreateItemCard: false,
      value: 0,
      itemCards: null,
      openSnackbar: false,
    };
  }
  componentDidMount() {
    this.handleGetRequests();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.createItemSuccessful === 'successful') {
      this.handleCloseCreateItemCard();
      this.setState({
        openSnackbar: true,
      });
    }
  }
  handleGetRequests = async () => {
    // Get last message of sender
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionItemsCards)
      .where('sellerId', '==', this.props.auth.uid);
    const snapshot = await query.get();
    const itemCards = snapshot.docs.map(doc => doc.data());
    this.setState({
      itemCards: itemCards,
    });
  };
  handleOpenCreateItemCard = () => {
    this.setState({
      isOpenCreateItemCard: true,
    });
  };
  handleCloseCreateItemCard = () => {
    this.setState({
      isOpenCreateItemCard: false,
    });
  };

  handleCloseSnackbar = () => {
    this.setState({
      openSnackbar: false,
    });
  };

  handleChangeTabs = (event, newValue) => {
    this.setState({
      value: newValue,
    });
  };

  handleRemoveItemCard = (e, itemId) => {
    this.props.removeItemCardRequest(itemId);
  };

  handleAcceptItemCard = (e, item) => {
    var updateItemCard = {
      name: item.name,
      sku: item.sku,
      date: item.date ? item.date : null,
      dimenesions: {
        height: parseInt(item.dimenesions.height),
        width: parseInt(item.dimenesions.width),
        length: parseInt(item.dimenesions.length),
      },
      description: item.description,
      sellerId: item.sellerId,
    };
    const documentId = item.id;
    this.props.createItemCard(updateItemCard, false, documentId);
  };

  render() {
    const {
      classes,
      itemCards,
      itemCardsRequests,
      auth,
      userProfile,
      createItemSuccessful,
    } = this.props;
    if (userProfile)
      return (
        <React.Fragment>
          <Dialog
            fullWidth={true}
            maxWidth={'sm'}
            open={this.state.isOpenCreateItemCard}
            onClose={this.handleCloseCreateItemCard}
          >
            <CreateItemCard
              auth={auth}
              userProfile={userProfile}
              createItemSuccessful={createItemSuccessful}
            />
          </Dialog>
          {userProfile.isAdmin ? (
            <React.Fragment>
              <Snackbar
                open={this.state.openSnackbar}
                autoHideDuration={6000}
                onClose={this.handleCloseSnackbar}
              >
                <Alert onClose={this.handleCloseSnackbar} severity="success">
                  Done
                </Alert>
              </Snackbar>
              <AppBar position="static" color="default">
                <Tabs
                  value={this.state.value}
                  onChange={this.handleChangeTabs}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  aria-label="full width tabs example"
                >
                  <Tab label="My Items Card" {...a11yProps(0)} />
                  <Tab label="Item Cards Requests" {...a11yProps(1)} />
                </Tabs>
              </AppBar>
              <TabPanel value={this.state.value} index={0}>
                <GridContainer>
                  <Card plain>
                    <CardHeader plain color="primary">
                      <h4 className={classes.cardTitleWhite}>
                        Item Cards List
                      </h4>
                      <p className={classes.cardCategoryWhite}>
                        Propratise is Table
                      </p>
                    </CardHeader>
                    <CardBody>
                      <Table
                        tableHeaderColor="primary"
                        tableHead={[
                          'Serial Number Item',
                          'SKU',
                          'Name',
                          'Description',
                          'Dimensions',
                          'Date',
                        ]}
                        tableData={itemCards}
                      />
                    </CardBody>
                  </Card>
                </GridContainer>
              </TabPanel>
              <TabPanel value={this.state.value} index={1}>
                <GridContainer>
                  <Card plain>
                    <CardHeader plain color="primary">
                      <h4 className={classes.cardTitleWhite}>
                        Item Cards List
                      </h4>
                      <p className={classes.cardCategoryWhite}>
                        Propratise is Table
                      </p>
                    </CardHeader>
                    <CardBody>
                      <ItemsCardsRequestsTable
                        tableHeaderColor="primary"
                        tableHead={[
                          'Serial Number Item',
                          'SKU',
                          'Name',
                          'Description',
                          'Dimensions',
                          'Date',
                          '',
                          '',
                        ]}
                        tableData={itemCardsRequests}
                        handleRemoveItemCard={this.handleRemoveItemCard}
                        handleAcceptItemCard={this.handleAcceptItemCard}
                      />
                    </CardBody>
                  </Card>
                </GridContainer>
              </TabPanel>
            </React.Fragment>
          ) : (
            <GridContainer>
              <Snackbar
                open={this.state.openSnackbar}
                autoHideDuration={6000}
                onClose={this.handleCloseSnackbar}
              >
                <Alert onClose={this.handleCloseSnackbar} severity="success">
                  Done
                </Alert>
              </Snackbar>
              <Card plain>
                <CardHeader plain color="primary">
                  <h4 className={classes.cardTitleWhite}>Item Cards List</h4>
                  <p className={classes.cardCategoryWhite}>
                    Propratise is Table
                  </p>
                </CardHeader>
                <CardBody>
                  <Table
                    tableHeaderColor="primary"
                    tableHead={[
                      'Serial Number Item',
                      'SKU',
                      'Name',
                      'Description',
                      'Dimensions',
                      'Date',
                    ]}
                    tableData={this.state.itemCards}
                  />
                </CardBody>
              </Card>
            </GridContainer>
          )}

          <Fab
            aria-label="add"
            className={classes.fab}
            color="primary"
            onClick={this.handleOpenCreateItemCard}
          >
            <AddIcon />
          </Fab>
        </React.Fragment>
      );
    else
      return (
        <div>
          <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            className={classes.gridCircularProgress}
          >
            <Grid item xs={3}>
              <CircularProgress className={classes.circularProgress} />
            </Grid>
          </Grid>
        </div>
      );
  }
}

const mapStateToProps = (state, props) => {
  return {
    itemCards: state.firestore.ordered.itemCards,
    createItemSuccessful: state.shipments.createItemSuccessful,
    itemCardsRequests: state.firestore.ordered.itemCardsRequests,
    userProfile: state.firestore.ordered.userProfile
      ? state.firestore.ordered.userProfile[0]
      : null,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    createItemCard: (itemCard, isAdmin, documentId) =>
      dispatch(createItemCard(itemCard, isAdmin, documentId)),
    removeItemCardRequest: itemCardId =>
      dispatch(removeItemCardRequest(itemCardId)),
  };
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionItemsCards,
        where: ['sellerId', '==', props.auth.uid],
        storeAs: 'itemCards',
      },
    ];
  }),
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionItemsCardsRequests,
        orderBy: ['createdTimestamp', 'desc'],
        storeAs: 'itemCardsRequests',
      },
    ];
  }),
  firestoreConnect(props => {
    if (props.auth)
      return [
        {
          collection: constantsConfig.collectionUsers,
          where: ['id', '==', props.auth.uid],
          storeAs: 'userProfile',
        },
      ];
    else return [];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(ItemCardsList);
