import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  textField: {
    width: '20em',
  },
  inputDate: {
    padding: 5,
    marginTop: '0.5em',
    fontSize: '1rem',
    width: '19em',
    border: '1px solid',
    borderRadius: '0.5em',
  },
}));

function checkIsChoosedItem(mainfests, item) {
  if (mainfests.length === 0) return false;
  else {
    for (let i = 0; i < mainfests.length; i++) {
      if (mainfests[i].id === item.id) return true;
    }
    return false;
  }
}

function ChooseItemsSummary(props) {
  const {
    item,
    handleUpdateMainfest,
    mainfest /*, checked, handleChange*/,
  } = props;
  const classes = useStyles();
  const firstCheck = checkIsChoosedItem(mainfest, item);
  const [checked, setChecked] = React.useState(firstCheck);
  const [quantity, setQuantity] = React.useState(0);
  const [date, setDate] = React.useState(null);
  var test = [];
  const handleChangeQuantity = (e, item) => {
    setQuantity(e.target.value);
    var quantity = parseInt(e.target.value);
    const newMainfest = mainfest.slice(); //copy the array
    for (let i = 0; i < mainfest.length; i++) {
      if (mainfest[i].id === item.id)
        newMainfest[i] = { ...mainfest[i], quantity };
    }
    handleUpdateMainfest(e, newMainfest);
  };
  const handleChangeDate = (e, item) => {
    setDate(e.target.value);
    var expireDate = e.target.value;
    const newMainfest = mainfest.slice(); //copy the array
    for (let i = 0; i < mainfest.length; i++) {
      if (mainfest[i].id === item.id)
        newMainfest[i] = { ...mainfest[i], expireDate };
    }
    handleUpdateMainfest(e, newMainfest);
  };
  const handleChange = (event, item) => {
    if (event.target.checked) {
      setChecked(event.target.checked);
      mainfest.push(item);
    } else {
      setChecked(event.target.checked);
      const newMainfest = mainfest.filter(element => element.id != item.id);
      handleUpdateMainfest(event, newMainfest);
    }
  };
  return (
    <React.Fragment>
      <Grid container>
        <Grid item style={{ marginLeft: '-1em' }}>
          <Checkbox
            checked={checked}
            onChange={e => handleChange(e, item)}
            color="primary"
            inputProps={{ 'aria-label': 'secondary checkbox' }}
          />
        </Grid>
        <Grid item>
          <Typography
            style={{ marginTop: '0.5em' }}
            onClick={e => props.handleChangeMainfest(e, item)}
          >
            {item.name}
          </Typography>
        </Grid>
      </Grid>
      {checked === true ? (
        <Grid
          container
          direction="column"
          justifyContent="center"
          alignItems="flex-start"
        >
          <Grid item>
            <TextField
              onChange={e => handleChangeQuantity(e, item)}
              id="quantity"
              label="quantity"
              className={classes.textField}
            />
          </Grid>
          <Grid item>
            <input
              type="date"
              label="Date"
              id="date"
              className={classes.inputDate}
              onChange={e => handleChangeDate(e, item)}
            />
          </Grid>
        </Grid>
      ) : (
        <Grid
          container
          direction="column"
          justifyContent="center"
          alignItems="flex-start"
        >
          <Grid item>
            <TextField
              id="quantity"
              label="quantity"
              disabled
              className={classes.textField}
            />{' '}
          </Grid>
          <Grid item>
            <input
              type="date"
              label="date"
              disabled
              className={classes.inputDate}
            />
          </Grid>
        </Grid>
      )}
    </React.Fragment>
  );
}

export default ChooseItemsSummary;
