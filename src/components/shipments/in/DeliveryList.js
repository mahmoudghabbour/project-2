import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { constantsConfig } from '../../../config/ConstanstsConfig';
import { withStyles } from '@material-ui/core/styles';
import Card from 'components/Card/Card.js';
import CardHeader from 'components/Card/CardHeader.js';
import CardBody from 'components/Card/CardBody.js';
import GridContainer from 'components/Grid/GridContainer.js';
import Dialog from '@material-ui/core/Dialog';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import {
  acceptShipmentinRequest,
  updateDeliveryStatus,
} from '../../../store/actions/shipments/ShipmentsActions';
import DeliveryTable from 'components/Table/DeliveryTable';
import BarcodeDialog from './BarcodeDialog';
import StorageDialog from './StorageDialog';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function isInt(value) {
  return (
    !isNaN(value) &&
    parseInt(Number(value)) == value &&
    !isNaN(parseInt(value, 10))
  );
}

const styles = theme => ({
  fab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
    zIndex: 2,
  },
});
class DeliveryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenShipmentInRequest: false,
      isOpenBarcodeDialog: false,
      isOpenStorageDialog: false,
      openSnackbar: false,
      item: null,
      shipmentInSerialNumber: null,
      warehouseItems: [],
    };
  }
  componentDidMount() {}

  componentWillReceiveProps(nextProps) {
    if (nextProps.deliverySpinner === 'successful') {
      this.handleCloseStorageDialog();
      this.setState({
        openSnackbar: true,
      });
    }
  }

  handleChangeStatus = (e, deliveryDocument, status) => {
    this.props.updateDeliveryStatus(deliveryDocument, status);
  };

  handleOpenBarcodeDialog = (e, item, shipmentInSerialNumber) => {
    this.setState({
      item: item,
      shipmentInSerialNumber: shipmentInSerialNumber,
      isOpenBarcodeDialog: true,
    });
  };

  handleCloseBarcodeDialog = () => {
    this.setState({
      isOpenBarcodeDialog: false,
      isOpenStorageDialog: true,
    });
  };

  handleCloseStorageDialog = () => {
    this.setState({
      isOpenStorageDialog: false,
    });
  };

  handleCloseSnackbar = () => {
    this.setState({
      openSnackbar: false,
    });
  };

  render() {
    const { classes, deliveryList, deliverySpinner } = this.props;
    return (
      <React.Fragment>
        <Dialog
          fullWidth={true}
          open={this.state.isOpenBarcodeDialog}
          onClose={this.handleCloseBarcodeDialog}
        >
          <BarcodeDialog
            item={this.state.item}
            warehouseItems={this.state.warehouseItems}
            handleCloseBarcodeDialog={this.handleCloseBarcodeDialog}
          />
        </Dialog>
        <Dialog
          fullWidth={true}
          open={this.state.isOpenStorageDialog}
          onClose={this.handleCloseStorageDialog}
        >
          <StorageDialog
            deliverySpinner={deliverySpinner}
            sellerId={
              this.state.item
                ? this.state.item.mainfest
                  ? this.state.item.mainfest[0].sellerId
                  : null
                : null
            }
            warehouseItems={this.state.warehouseItems}
            shipmentInSerialNumber={this.state.shipmentInSerialNumber}
          />
        </Dialog>
        <Snackbar
          open={this.state.openSnackbar}
          autoHideDuration={6000}
          onClose={this.handleCloseSnackbar}
        >
          <Alert onClose={this.handleCloseSnackbar} severity="success">
            Done
          </Alert>
        </Snackbar>
        <GridContainer>
          <Card plain>
            <CardHeader plain color="primary">
              <h4 className={classes.cardTitleWhite}>Delivery List</h4>
              <p className={classes.cardCategoryWhite}>Propratise is Table</p>
            </CardHeader>
            <CardBody>
              <DeliveryTable
                tableHeaderColor="primary"
                tableHead={[
                  'Serial Number',
                  'Seller',
                  'Status',
                  'Source Location',
                  'warehouse',
                  '',
                ]}
                tableData={deliveryList}
                handleChangeStatus={this.handleChangeStatus}
                handleOpenBarcodeDialog={this.handleOpenBarcodeDialog}
              />
            </CardBody>
          </Card>
        </GridContainer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
    deliverySpinner: state.warehouse.deliverySpinner,
    deliveryList: state.firestore.ordered.deliveryList,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    acceptShipmentinRequest: request =>
      dispatch(acceptShipmentinRequest(request)),
    updateDeliveryStatus: (deliveryDocument, status) =>
      dispatch(updateDeliveryStatus(deliveryDocument, status)),
  };
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionDelivery,
        storeAs: 'deliveryList',
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(DeliveryList);
