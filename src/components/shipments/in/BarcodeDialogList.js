import React from 'react';
import { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';

const styles = theme => ({
  textField: {
    width: '100%',
  },
  gridItem: {
    marginTop: '1em',
  },
});

class BarcodeDialogList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mainfestItem: props.mainfestItem,
      barcode: '',
      pending: props.mainfestItem.quantity,
      inserted: 0,
      items: [],
      totalNum: this.props.totalNum,
      currentNum: this.props.currentNum,
      /*itemsNumber: props.item.mainfest.length*/
    };
  }

  componentDidMount() {}

  componentWillReceiveProps(nextProps) {
    if (nextProps.mainfestItem.id != this.state.mainfestItem.id) {
      this.setState({
        mainfestItem: nextProps.mainfestItem,
        items: [],
        pending: nextProps.mainfestItem.quantity,
        inserted: 0,
        totalNum: nextProps.totalNum,
        currentNum: nextProps.currentNum,
      });
    }
  }

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleChangeItems = e => {
    const barcode = this.state.barcode;
    var mainfestItem = {
      date: this.state.mainfestItem.date,
      description: this.state.mainfestItem.description,
      dimenesions: this.state.mainfestItem.dimenesions,
      id: this.state.mainfestItem.id,
      itemSerialNumber: this.state.mainfestItem.itemSerialNumber,
      name: this.state.mainfestItem.name,
      sku: this.state.mainfestItem.sku,
    };
    this.state.items.push({ ...mainfestItem, barcode });
    this.setState({
      barcode: '',
      pending: this.state.pending - 1,
      inserted: this.state.inserted + 1,
    });
  };

  render() {
    const {
      classes,
      mainfestItem,
      currentNum,
      totalNum,
      handleNext,
      warehouseItems,
      handleCloseBarcodeDialog,
    } = this.props;

    return (
      <React.Fragment>
        <br />
        <Grid container direction="column" justify="center" alignItems="center">
          <Grid item style={{ width: '100%' }} className={classes.gridItem}>
            <TextField
              id="barcode"
              label="barcode"
              className={classes.textField}
              value={this.state.barcode}
              onChange={this.handleChange}
            />
          </Grid>
          <Grid item className={classes.gridItem}>
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleChangeItems}
            >
              Enter
            </Button>
          </Grid>
          <Grid item className={classes.gridItem}>
            <Typography>SKU: {mainfestItem.sku}</Typography>
          </Grid>
          <Grid
            item
            container
            direction="row"
            justify="space-around"
            alignItems="center"
            className={classes.gridItem}
          >
            <Grid item>
              <Typography>Total</Typography>
            </Grid>
            <Grid item>
              <Typography>Pending</Typography>
            </Grid>
            <Grid item>
              <Typography>Inserted</Typography>
            </Grid>
          </Grid>
          <Grid
            item
            container
            direction="row"
            justify="space-around"
            alignItems="center"
            className={classes.gridItem}
          >
            <Grid item>
              <Typography>{mainfestItem.quantity}</Typography>
            </Grid>
            <Grid item>
              <Typography>{this.state.pending}</Typography>
            </Grid>
            <Grid item>
              <Typography>{this.state.inserted}</Typography>
            </Grid>
          </Grid>
        </Grid>
        <DialogActions>
          {this.state.totalNum - 1 === this.state.currentNum ? (
            <Button
              variant="contained"
              color="primary"
              onClick={e => {
                handleNext(e, this.state.items);
                handleCloseBarcodeDialog();
              }}
            >
              Finish
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              onClick={
                e => handleNext(e, this.state.items)
                // this.handleChangeItems();
              }
            >
              Next
            </Button>
          )}
        </DialogActions>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(BarcodeDialogList);
