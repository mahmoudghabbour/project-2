import React from 'react';
import { Component } from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import BarcodeDialogList from './BarcodeDialogList';

class BarcodeDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemsNumber: props.item.mainfest.length,
      mainfest: props.item.mainfest,
      num: 0,
    };
  }

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleNext = (e, items) => {
    if (this.state.num != this.state.itemsNumber - 1) {
      this.setState({
        num: this.state.num + 1,
      });
    }
    this.props.warehouseItems.push({ items });
  };

  render() {
    return (
      <React.Fragment>
        <DialogTitle id="form-dialog-title">Inserting</DialogTitle>
        <DialogContent>
          <BarcodeDialogList
            mainfestItem={this.state.mainfest[this.state.num]}
            handleNext={this.handleNext}
            currentNum={this.state.num}
            totalNum={this.state.itemsNumber}
            warehouseItems={this.props.warehouseItems}
            handleCloseBarcodeDialog={this.props.handleCloseBarcodeDialog}
          />
        </DialogContent>
      </React.Fragment>
    );
  }
}

export default BarcodeDialog;
