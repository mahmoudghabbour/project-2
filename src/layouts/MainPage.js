import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import SignIn from '../components/auth/SignIn';
import SignUp from 'components/auth/SignUp';
import AdminSignUp from 'components/auth/AdminSignUp';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import saudiArabia from '../SVGIcons/saudi-arabia.svg';
import Group486 from '../SVGIcons/Group 486.svg';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import Button from '@material-ui/core/Button';
import Group164 from '../SVGIcons/Group 164.svg';
import Group786 from '../SVGIcons/Group 786.svg';
import Group798 from '../SVGIcons/Group 798.svg';
import Group860 from '../SVGIcons/Group 860.svg';
import facebookIcon from '../SVGIcons/Group 681.svg';
import instagramIcon from '../SVGIcons/Group 683.svg';
import twitterIcon from '../SVGIcons/Group 682.svg';
import emailIcon from '../SVGIcons/Group 804.svg';
import phoneIcon from '../SVGIcons/Group 802.svg';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import silverImage from '../images/Screenshot 2021-03-10 151017.png';
import goldImage from '../images/Screenshot 2021-03-10 151833.png';
import diamondImage from '../images/Screenshot 2021-03-10 151848.png';
import emptyImage from '../images/Screenshot 2021-03-10 152729.png';
import mapImage from '../images/Screenshot 2021-03-10 235440.png';
import group280 from '../SVGIcons/Group 280.svg';
// import Ellipse41 from "../../SVGIcons/Ellipse 41.svg";
import Path1573 from '../SVGIcons/Path 1573.svg';
import Path1661 from '../SVGIcons/Path 1661.svg';
import group697 from '../SVGIcons/Group 697.svg';
import Divider from '@material-ui/core/Divider';
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import Tabs from '@material-ui/core/Tabs';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const styles = theme => ({
  paper: {
    padding: 20,
    marginTop: 10,
    border: 'none',
  },
  sectionDesktop: {
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  title: {
    fontWeight: 'bolder',
    color: '#29017c',
  },
  phrases: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: '15px',
    fontFamily: 'Segoe UI',
  },
  partnerButton: {
    padding: '3%',
    float: 'right',
    fontSize: '25px',
    fontWeight: 'bolder',
    width: '10rem',
    backgroundColor: '#29017c',
    color: 'white',
    marginTop: '10%',
  },
  secondTitle: {
    color: '#29017c',
    fontWeight: 'bold',
    fontSize: '25px',
    fontFamily: 'Segoe UI',
  },
  secondTitleInOut: {
    fontWeight: 'bold',
    fontSize: '25px',
    fontFamily: 'Segoe UI',
    color: '#d84315',
  },
  phrasesSection2: {
    fontSize: '17px',
    color: 'black',
    fontFamily: 'Segoe UI',
  },
  ourWorkTypography: {
    color: '#29017c',
    fontWeight: 'bolder',
    fontFamily: 'Segoe UI',
    marginTop: '1rem',
  },
  flexCenter: {
    display: 'flex',
    justifyContent: 'center',
  },
  chooseButton: {
    backgroundColor: '#29017c',
    color: 'white',
    fontWeight: 'bolder',
    fontSize: '15px',
    padding: 5,
    fontFamily: 'Segoe UI',
    width: '30%',
  },
  rootCard: {
    maxWidth: 345,
  },
  media: {
    height: 260,
  },
  cardTitles: {
    color: '#29017c',
  },
  confirmButton: {
    color: 'white',
    backgroundColor: '#d84315',
    fontWeight: 'bolder',
    fontSize: '25px',
    width: '15%',
    padding: 5,
    fontFamily: 'Segoe UI',
  },
  bottomCard: {
    backgroundColor: 'white',
    color: 'white',
    width: '50%',
    padding: 10,
  },
  input: {
    padding: '5%',
    height: '5%',
    width: '70%',
    border: '1px solid #29017c',
    borderRadius: '5px',
  },
  divider: {
    color: '#29017c',
    backgroundColor: '#29017c',
    width: '99.7%',
    maxWidth: '100%',
    marginBottom: 1,
    border: '1px solid #29017c',
  },
  inputReview: {
    padding: '5%',
    height: '5%',
    width: '80%',
    border: '1px solid #29017c',
    borderRadius: '5px',
  },
  sendButton: {
    backgroundColor: '#29017c',
    color: 'white',
    marginLeft: '2rem',
    marginTop: '1rem',
    width: '20%',
    fontFamily: 'Segoe UI',
  },
  links: {
    marginLeft: '1rem',
    marginTop: '0.5rem',
    color: '#29017c',
  },
  bottomCardTypography: {
    color: '#29017c',
    marginRight: '2rem',
    fontFamily: 'Segoe UI',
  },
  flex: {
    display: 'flex',
  },
  containerLinks: {
    marginTop: '0.2rem',
  },
  verticalLine: {
    fontWeight: 'bolder',
    borderRadius: '35px',
    fontSize: '15px',
    color: '#29017c',
    letterSpacing: '3px',
  },
  phraseBottomDivider: {
    color: 'white',
    fontFamily: 'Segoe UI',
    fontSize: '10px',
    fontWeight: 'bold',
  },
  path: {
    marginTop: '1rem',
  },
  grow: {
    flexGrow: 1,
    // padding: 20,
  },
  appBar: {
    backgroundColor: 'white',
    // boxShadow: "0px 0px 0px 0px",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  sectionDesktopAppbar: {
    display: 'none',
    width: '30%',
    [theme.breakpoints.up('xs')]: {
      display: 'flex',
    },
  },
  sectionMobileAppbar: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  logoAppbar: { width: '165px', height: '58px' },
  tab: {
    color: 'black',
    minWidth: '20%',
    fontWeight: 'bold',
    fontFamily: 'Segoe UI',
  },
  signInButton: {
    // padding: "5%",
    width: '155px',
    height: '58px',
    float: 'right',
    marginRight: 'auto',
  },
  personIcon: {
    width: '27px',
    height: '21px',
  },
});

class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      isOpenSignIn: false,
      isOpenSignUp: false,
      isOpenAdminSignUp: false,
    };
  }
  handleChange = (event, newValue) => {
    this.setState({
      value: newValue,
    });
  };
  handleOpenAdminSignUp = () => {
    this.setState({
      isOpenAdminSignUp: true,
    });
  };
  handleCloseAdminSignUp = () => {
    this.setState({
      isOpenAdminSignUp: false,
    });
  };
  handleOpenSignIn = () => {
    this.setState({
      isOpenSignIn: true,
    });
  };
  handleCloseSignIn = () => {
    this.setState({
      isOpenSignIn: false,
    });
  };
  handleOpenSignUp = () => {
    this.setState({
      isOpenSignUp: true,
    });
  };
  handleCloseSignUp = () => {
    this.setState({
      isOpenSignUp: false,
    });
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        {/* <AppBarMainPage /> */}
        <Dialog open={this.state.isOpenSignIn} onClose={this.handleCloseSignIn}>
          <SignIn />
        </Dialog>
        <Dialog open={this.state.isOpenSignUp} onClose={this.handleCloseSignUp}>
          <SignUp />
        </Dialog>
        <Dialog
          open={this.state.isOpenAdminSignUp}
          onClose={this.handleCloseAdminSignUp}
        >
          <AdminSignUp />
        </Dialog>

        <div className={classes.grow}>
          <AppBar position="fixed" className={classes.appBar}>
            <Toolbar>
              <div className={classes.sectionDesktopAppbar}>
                <Button
                  variant="contained"
                  color="primary"
                  startIcon={
                    <img src={group697} className={classes.personIcon} alt="" />
                  }
                  className={classes.signInButton}
                  onClick={this.handleOpenSignIn}
                >
                  تسجيل الدخول
                </Button>
              </div>

              <div className={classes.grow} />

              <Tabs
                value={this.state.value}
                onChange={this.handleChange}
                indicatorColor="primary"
                style={{ width: '40%' }}
                aria-label="simple tabs example"
              >
                <Tab
                  label="كن شريكنا"
                  value={4}
                  className={classes.tab}
                  {...a11yProps(4)}
                />
                <Tab
                  label="آلية عملنا"
                  value={3}
                  className={classes.tab}
                  {...a11yProps(3)}
                />
                <Tab
                  label="خدماتنا"
                  value={2}
                  className={classes.tab}
                  {...a11yProps(2)}
                />
                <Tab
                  label="أهدافنا"
                  value={1}
                  className={classes.tab}
                  {...a11yProps(1)}
                />
                <Tab
                  label="من نحن"
                  value={0}
                  className={classes.tab}
                  {...a11yProps(0)}
                />
              </Tabs>
              <img src={group280} className={classes.logoAppbar} alt="" />
            </Toolbar>
          </AppBar>
          {/* {renderMobileMenu}
          {renderMenu} */}
        </div>
        <div style={{ marginTop: '2.5rem', backgroundColor: 'white' }}>
          <Container maxWidth="xl" fixed>
            <Grid container>
              <Grid item sm>
                <Paper className={classes.paper} variant="outlined">
                  <img src={saudiArabia} alt="" />
                </Paper>
              </Grid>
              <Grid item sm>
                <Paper
                  className={classes.paper}
                  variant="outlined"
                  style={{ padding: '150px 100px 150px 100px' }}
                >
                  <div className={classes.sectionDesktop}>
                    <Typography
                      variant="h3"
                      gutterBottom
                      align="right"
                      style={{ marginLeft: '4rem', color: '#d84315' }}
                    >
                      IN-OUT
                    </Typography>
                    <Typography
                      variant="h3"
                      gutterBottom
                      align="right"
                      style={{ marginLeft: 'auto' }}
                      className={classes.title}
                    >
                      نحن شركة
                    </Typography>
                  </div>
                  <Typography
                    variant="subtitle2"
                    gutterBottom
                    align="right"
                    className={classes.phrases}
                  >
                    رائدة في إدارة المنتجات في مخازنها و التي تتسع IN-OUT
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    gutterBottom
                    align="right"
                    className={classes.phrases}
                  >
                    لجميع أحجام و أنواع المنتجات المتداولة في المملكة العربية
                    السعودية
                  </Typography>
                  <Typography align="right" style={{ marginTop: '10%' }}>
                    <MoreHorizIcon />
                  </Typography>
                  <Button variant="contained" className={classes.partnerButton}>
                    كن شريكنا
                  </Button>
                </Paper>
              </Grid>
            </Grid>
            <div className={classes.flexCenter}>
              <img src={Path1573} alt="" className={classes.path} />
            </div>
            {/*part 2*/}
            <Grid container>
              <Grid item sm>
                <Paper className={classes.paper} variant="outlined">
                  <img src={Group486} alt="" />
                </Paper>
              </Grid>
              <Grid item sm>
                <Paper
                  className={classes.paper}
                  variant="outlined"
                  style={{ padding: '150px 100px 100px 100px' }}
                >
                  <Typography
                    variant="h3"
                    gutterBottom
                    align="right"
                    style={{ marginLeft: 'auto' }}
                    className={classes.title}
                  >
                    أهدافنا
                  </Typography>
                  <br />
                  <div className={classes.sectionDesktop}>
                    <Typography
                      variant="h3"
                      gutterBottom
                      align="right"
                      style={{ marginLeft: '6.5rem' }}
                      className={classes.secondTitleInOut}
                    >
                      IN-OUT
                    </Typography>
                    <Typography
                      variant="h3"
                      gutterBottom
                      align="right"
                      style={{ marginLeft: 'auto' }}
                      className={classes.secondTitle}
                    >
                      نحن نسعى في شركة
                    </Typography>
                  </div>
                  <Typography
                    variant="subtitle2"
                    gutterBottom
                    align="right"
                    style={{ marginTop: '20px' }}
                    className={classes.phrasesSection2}
                  >
                    التركيز الخدمات اللوجستية على تلبية احتياجـات التوريد و
                    التخزين و التوزيع و تقديم الدعم المطلوب مع الفعالية بجودة
                    عالية مما يتيح لشركائنا التركيز على عملائهم ومبيعاتهم
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    gutterBottom
                    align="right"
                    className={classes.phrasesSection2}
                  >
                    ان تكون شركتنا هي النمــــــوذج المــــــــثالي
                    والــــــــرائد وان يكـــــــــون لهــــــا وقـــع
                    وصــــــــــدى في كـــــــافـــــــة المياديــــــــــن
                  </Typography>
                </Paper>
              </Grid>
            </Grid>
            <div className={classes.flexCenter}>
              <img src={Path1573} alt="" className={classes.path} />
            </div>
            {/*part3*/}
            <Grid container>
              <Grid item sm>
                <Paper className={classes.paper} variant="outlined">
                  <img src={Group164} alt="" />
                </Paper>
              </Grid>
              <Grid item sm>
                <Paper
                  className={classes.paper}
                  variant="outlined"
                  style={{ padding: '150px 100px 150px 100px' }}
                >
                  <Typography
                    variant="h3"
                    gutterBottom
                    align="right"
                    style={{ marginLeft: 'auto' }}
                    className={classes.title}
                  >
                    خدماتنا
                  </Typography>
                  <br />
                  <div className={classes.sectionDesktop}>
                    <Typography
                      variant="h3"
                      gutterBottom
                      align="right"
                      style={{ marginLeft: '14rem' }}
                      className={classes.secondTitleInOut}
                    >
                      IN-OUT
                    </Typography>
                    <Typography
                      variant="h3"
                      gutterBottom
                      align="right"
                      style={{ marginLeft: 'auto' }}
                      className={classes.secondTitle}
                    >
                      في شركة
                    </Typography>
                  </div>
                  <Typography
                    variant="subtitle2"
                    gutterBottom
                    align="right"
                    style={{ marginTop: '20px' }}
                    className={classes.phrasesSection2}
                  >
                    نقوم باســتلام منتجات المتجر من الـمـالـك أو من مورد منتجاته
                    في مـــســـتودعــاتـنـا ويتم فرز وجرد المـنتجات والتأكد من
                    سلامتها قبل تخزينها حيث أن سياسة التخزين لدينا :
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    gutterBottom
                    align="right"
                    className={classes.phrasesSection2}
                  >
                    يجب أن تتم بطريقة ملائمة للمنتج وحسب تعليمات مصانع المنتجات
                    ومن ثم نقوم بتغليف المنتج بكل عناية وتجهيزها كي يتم
                    شــــحنها الى المشتري الى جميع مدن المملكة العربية السعودية
                  </Typography>
                </Paper>
              </Grid>
            </Grid>
            <div className={classes.flexCenter} style={{ marginTop: '-150px' }}>
              <img src={Path1573} alt="" className={classes.path} />
            </div>
            {/*part 4*/}
            <Typography
              variant="h3"
              gutterBottom
              align="center"
              className={classes.ourWorkTypography}
            >
              آلية عملنا
            </Typography>
            <div className={classes.flexCenter}>
              <img src={Group786} alt="" style={{ align: 'center' }} />
            </div>
          </Container>
          <div style={{ marginTop: '5%' }}>
            <div className={classes.flexCenter}>
              <img src={Group860} alt="" style={{ maxWidth: '100%' }} />
            </div>
          </div>
          <div style={{ marginTop: '5%' }}>
            <Typography
              variant="h3"
              gutterBottom
              align="center"
              className={classes.ourWorkTypography}
            >
              كن شريكنا
            </Typography>
          </div>
          <div style={{ marginTop: '5%' }}>
            <Grid
              container
              justify="center"
              spacing={1}
              style={{ padding: 0, width: '100%' }}
            >
              <Grid item>
                <Typography
                  variant="h3"
                  gutterBottom
                  className={classes.secondTitle}
                >
                  التي تناسبك
                </Typography>{' '}
              </Grid>
              <Grid key={2} item>
                <Typography
                  variant="h3"
                  gutterBottom
                  className={classes.secondTitleInOut}
                >
                  الباقة
                </Typography>{' '}
              </Grid>
              <Grid key={2} item>
                <Typography
                  variant="h3"
                  gutterBottom
                  className={classes.secondTitle}
                >
                  اختر
                </Typography>
              </Grid>
            </Grid>
          </div>
          {/*Grid part*/}
          <div style={{ marginTop: '5%' }}>
            <Grid
              container
              justify="center"
              spacing={3}
              style={{ padding: 0, width: '100%' }}
            >
              <Grid item>
                <Card className={classes.rootCard}>
                  <CardActionArea>
                    <CardMedia
                      className={classes.media}
                      image={emptyImage}
                      title="Contemplative Reptile"
                    />
                    <CardContent>
                      <Typography
                        gutterBottom
                        variant="h5"
                        component="h2"
                        align="center"
                        className={classes.cardTitles}
                      >
                        Silver
                      </Typography>
                      <img src={silverImage} alt="" />
                    </CardContent>
                  </CardActionArea>
                  <CardActions className={classes.flexCenter}>
                    <Button size="small" className={classes.chooseButton}>
                      اختيار
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
              <Grid item>
                <Card className={classes.rootCard}>
                  <CardActionArea>
                    <CardMedia
                      className={classes.media}
                      image={emptyImage}
                      title="Contemplative Reptile"
                    />
                    <CardContent>
                      <Typography
                        gutterBottom
                        variant="h5"
                        component="h2"
                        align="center"
                        className={classes.cardTitles}
                      >
                        Gold
                      </Typography>
                      <img src={goldImage} alt="" />
                    </CardContent>
                  </CardActionArea>
                  <CardActions className={classes.flexCenter}>
                    <Button size="small" className={classes.chooseButton}>
                      اختيار
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
              <Grid item>
                <Card className={classes.rootCard}>
                  <CardActionArea>
                    <CardMedia
                      className={classes.media}
                      image={emptyImage}
                      title="Contemplative Reptile"
                    />
                    <CardContent>
                      <Typography
                        gutterBottom
                        variant="h5"
                        component="h2"
                        align="center"
                        className={classes.cardTitles}
                      >
                        Diamond
                      </Typography>
                      <img src={diamondImage} alt="" />
                    </CardContent>
                  </CardActionArea>
                  <CardActions className={classes.flexCenter}>
                    <Button size="small" className={classes.chooseButton}>
                      اختيار
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            </Grid>
          </div>
          <div style={{ marginTop: '5%' }}>
            <div className={classes.flexCenter}>
              <Button variant="contained" className={classes.confirmButton}>
                تأكيد
              </Button>
            </div>
          </div>
          <div className={classes.flexCenter}>
            <img
              src={Path1661}
              alt=""
              className={classes.path}
              style={{ marginTop: '5rem' }}
            />
          </div>
          <div style={{ marginTop: '5rem' }}>
            <div className={classes.flexCenter}>
              <img src={Group798} alt="" />
            </div>
          </div>
          <Divider className={classes.divider} />
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div className={classes.bottomCard}>
              <div
                className={classes.flexCenter}
                style={{ color: '#29017c', fontFamily: 'Segoe UI' }}
              >
                إرسال مراجعة
              </div>
              <div className={classes.flexCenter} style={{ marginTop: '1rem' }}>
                <Grid container spacing={0} align="center">
                  <Grid item xs>
                    <Typography
                      align="right"
                      className={classes.bottomCardTypography}
                    >
                      البريد الإلكتروني
                    </Typography>
                  </Grid>
                  <Grid item xs>
                    <Typography
                      align="right"
                      className={classes.bottomCardTypography}
                    >
                      الاسم
                    </Typography>
                  </Grid>
                </Grid>
              </div>
              <div className={classes.flexCenter}>
                <Grid container spacing={0} align="center">
                  <Grid item xs>
                    <input className={classes.input} />
                  </Grid>
                  <Grid item xs>
                    <input className={classes.input} />
                  </Grid>
                </Grid>
              </div>
              <div>
                <Typography
                  align="right"
                  className={classes.bottomCardTypography}
                >
                  المراجعة
                </Typography>
              </div>
              <div className={classes.flexCenter}>
                <input className={classes.inputReview} />
              </div>
              <Button variant="contained" className={classes.sendButton}>
                إرسال
              </Button>
            </div>
            <div className={classes.flexCenter}>
              <img src={group280} className={classes.logo} alt="" />
            </div>
            <div className={classes.bottomCard}>
              <Grid container spacing={0} align="center">
                <Grid item xs style={{ padding: '1.5rem 1rem 1rem 3rem' }}>
                  <div className={classes.containerLinks}>
                    <div className={classes.flex}>
                      <div>
                        <img src={facebookIcon} alt="" />
                      </div>
                      <div className={classes.links}>www.facebook.com</div>
                    </div>
                  </div>
                  <div className={classes.containerLinks}>
                    <div className={classes.flex}>
                      <div>
                        <img src={instagramIcon} alt="" />
                      </div>
                      <div className={classes.links}>www.facebook.com</div>
                    </div>
                  </div>
                  <div className={classes.containerLinks}>
                    <div className={classes.flex}>
                      <div>
                        <img src={twitterIcon} alt="" />
                      </div>
                      <div className={classes.links}>www.facebook.com</div>
                    </div>
                  </div>
                  <div className={classes.containerLinks}>
                    <div className={classes.flex}>
                      <div>
                        <img src={emailIcon} alt="" />
                      </div>
                      <div className={classes.links}>www.facebook.com</div>
                    </div>
                  </div>
                  <div className={classes.containerLinks}>
                    <div className={classes.flex}>
                      <div>
                        <img src={phoneIcon} alt="" />
                      </div>
                      <div className={classes.links}>+966666666666</div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs>
                  <img src={mapImage} alt="" />
                </Grid>
              </Grid>
            </div>
          </div>
          <div
            style={{
              width: '100%',
              backgroundColor: '#29017c',
              padding: '2px 0px 2px 0px',
            }}
          >
            <div className={classes.flexCenter}>
              <div className={classes.phraseBottomDivider}>
                powered by MASA IT group
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(MainPage);
