import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: 'AIzaSyBJ8mToXwGGlicX3AelxZArzp2LcXXQQv0',
  authDomain: 'project-2-18f95.firebaseapp.com',
  projectId: 'project-2-18f95',
  storageBucket: 'project-2-18f95.appspot.com',
  messagingSenderId: '445912535130',
  appId: '1:445912535130:web:23cf6848356704b4c58863'
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;
