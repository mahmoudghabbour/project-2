/*!

=========================================================
* Material Dashboard React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from '@material-ui/icons/Dashboard';
import Person from '@material-ui/icons/Person';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import BubbleChart from '@material-ui/icons/BubbleChart';
import LocationOn from '@material-ui/icons/LocationOn';
import Notifications from '@material-ui/icons/Notifications';
import Unarchive from '@material-ui/icons/Unarchive';
import Language from '@material-ui/icons/Language';
// core components/views for Admin layout
import DashboardPage from 'views/Dashboard/Dashboard.js';
import UserProfile from 'views/UserProfile/UserProfile.js';
import TableList from 'views/TableList/TableList.js';
import Typography from 'views/Typography/Typography.js';
import Icons from 'views/Icons/Icons.js';
import Maps from 'views/Maps/Maps.js';
import NotificationsPage from 'views/Notifications/Notifications.js';
import UpgradeToPro from 'views/UpgradeToPro/UpgradeToPro.js';
// core components/views for RTL layout
import RTLPage from 'views/RTLPage/RTLPage.js';
import ItemCardsList from 'components/shipments/in/ItemCardsList';
import ShipmentInList from 'components/shipments/in/ShipmentInList';
import DeliveryList from 'components/shipments/in/DeliveryList';
import OrdersList from 'components/orders/OrdersList';
import InventoryList from 'components/inventory/InventoryList';
import Warehouse from 'components/warehouse/Warehouse';
import UsersList from 'components/users/UsersList';
import DashboardShipmentIn from 'components/dashboard/DashboardShipmentIn';
import DashboardItemCards from 'components/dashboard/DashboardItemCards';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import LoyaltyIcon from '@material-ui/icons/Loyalty';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import DashboardOrder from 'components/dashboard/DashboardOrder';

const dashboardRoutes = [
  {
    path: '/home',
    name: 'Home',
    rtlName: 'لوحة القيادة',
    icon: Dashboard,
    component: DashboardPage,
    layout: '/admin',
  },
  {
    path: '/deliverylist',
    name: 'Delivery',
    rtlName: 'إخطارات',
    icon: LocalShippingIcon,
    component: DeliveryList,
    layout: '/admin',
  },
  {
    path: '/shipmentsinlist',
    name: 'Shipment In',
    rtlName: 'قائمة الجدول',
    icon: 'content_paste',
    component: DashboardShipmentIn,
    layout: '/admin',
  },
  // {
  //   path: '/inventorylist',
  //   name: 'Inventory',
  //   rtlName: 'طباعة',
  //   icon: LibraryBooks,
  //   component: InventoryList,
  //   layout: '/admin',
  // },
  {
    path: '/warehouse',
    name: 'Warehouse',
    rtlName: 'طباعة',
    icon: HomeWorkIcon,
    component: Warehouse,
    layout: '/admin',
  },
  {
    path: '/orders',
    name: 'Orders',
    rtlName: 'الرموز',
    icon: ShoppingCartIcon,
    component: DashboardOrder,
    layout: '/admin',
  },
  {
    path: '/itemcards',
    name: 'Item Cards',
    rtlName: 'الرموز',
    icon: LoyaltyIcon,
    component: DashboardItemCards,
    layout: '/admin',
  },
  {
    path: '/userslist',
    name: 'Users',
    rtlName: 'الرموز',
    icon: Person,
    component: UsersList,
    layout: '/admin',
  },
  {
    path: '/user',
    name: 'User Profile',
    rtlName: 'ملف تعريفي للمستخدم',
    icon: Person,
    component: UserProfile,
    layout: '/admin',
  },
  // {
  //   path: '/maps',
  //   name: 'Maps',
  //   rtlName: 'خرائط',
  //   icon: LocationOn,
  //   component: Maps,
  //   layout: '/admin',
  // },

  // {
  //   path: '/rtl-page',
  //   name: 'RTL Support',
  //   rtlName: 'پشتیبانی از راست به چپ',
  //   icon: Language,
  //   component: RTLPage,
  //   layout: '/rtl',
  // },
  // {
  //   path: '/upgrade-to-pro',
  //   name: 'Upgrade To PRO',
  //   rtlName: 'التطور للاحترافية',
  //   icon: Unarchive,
  //   component: UpgradeToPro,
  //   layout: '/admin',
  // },
];

export default dashboardRoutes;
