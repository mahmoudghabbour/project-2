import { constantsConfig } from '../../../config/ConstanstsConfig';

export const requestShipmentIn = request => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    dispatch({ type: 'SHIPMENT_IN_SPINNER' });
    firestore
      .collection(constantsConfig.collectionShipmentInRequest)
      .add({})
      .then(resp => {
        firestore
          .collection(constantsConfig.collectionShipmentInRequest)
          .doc(resp.id)
          .set({ ...request, id: resp.id, shipmentInSerialNumber: resp.id })
          .then(resp => {
            dispatch({ type: 'CREATE_SHIPMENT_IN_REQUEST' });
          })
          .catch(err => {
            dispatch({ type: 'CREATE_SHIPMENT_IN_REQUEST_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'CREATE_SHIPMENT_IN_REQUEST_ERROR', err });
      });
  };
};

export const requestItemCard = itemCard => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    dispatch({ type: 'ITEM_SPINNER' });
    firestore
      .collection(constantsConfig.collectionItemsCardsRequests)
      .add({})
      .then(resp => {
        firestore
          .collection(constantsConfig.collectionItemsCardsRequests)
          .doc(resp.id)
          .set({
            ...itemCard,
            id: resp.id,
            itemSerialNumber: resp.id,
            date: Date.now(),
            createdTimestamp: Date.now(),
          })
          .then(resp => {
            dispatch({ type: 'CREATE_ITEM_CARD' });
          })
          .catch(err => {
            dispatch({ type: 'CREATE_ITEM_CARD_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'CREATE_ITEM_CARD_ERROR', err });
      });
  };
};

export const createItemCard = (itemCard, isAdmin, documentId) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    if (isAdmin === true) {
      dispatch({ type: 'ITEM_SPINNER' });
      firestore
        .collection(constantsConfig.collectionItemsCards)
        .add({})
        .then(resp => {
          firestore
            .collection(constantsConfig.collectionItemsCards)
            .doc(resp.id)
            .set({
              ...itemCard,
              id: resp.id,
              itemSerialNumber: resp.id,
              date: Date.now(),
            })
            .then(resp => {
              dispatch({ type: 'CREATE_ITEM_CARD' });
            })
            .catch(err => {
              dispatch({ type: 'CREATE_ITEM_CARD_ERROR', err });
            });
        })
        .catch(err => {
          dispatch({ type: 'CREATE_ITEM_CARD_ERROR', err });
        });
    } else {
      dispatch({ type: 'ITEM_SPINNER' });
      firestore
        .collection(constantsConfig.collectionItemsCards)
        .add({})
        .then(resp => {
          firestore
            .collection(constantsConfig.collectionItemsCards)
            .doc(resp.id)
            .set({
              ...itemCard,
              id: resp.id,
              itemSerialNumber: resp.id,
              date: Date.now(),
            })
            .then(resp => {
              firestore
                .collection(constantsConfig.collectionItemsCardsRequests)
                .doc(documentId)
                .delete()
                .then(() => {
                  dispatch({ type: 'CREATE_ITEM_CARD' });
                })
                .catch(err => {
                  dispatch({ type: 'CREATE_ITEM_CARD_ERROR', err });
                });
              dispatch({ type: 'CREATE_ITEM_CARD' });
            })
            .catch(err => {
              dispatch({ type: 'CREATE_ITEM_CARD_ERROR', err });
            });
        })
        .catch(err => {
          dispatch({ type: 'CREATE_ITEM_CARD_ERROR', err });
        });
    }
  };
};

export const acceptShipmentinRequest = request => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firestore
      .collection(constantsConfig.collectionDelivery)
      .doc(request.id)
      .set({
        ...request,
        createdTimestamp: Date.now(),
        status: 'waiting',
      })
      .then(() => {
        dispatch({ type: 'CREATE_DELIVERY_NOTIFICATIONS' });
        firestore
          .collection(constantsConfig.collectionShipmentsInHistory)
          .doc(request.id)
          .set({
            ...request,
            createdTimestamp: Date.now(),
          })
          .then(() => {
            firestore
              .collection(constantsConfig.collectionShipmentInRequest)
              .doc(request.id)
              .delete()
              .then(() => {
                dispatch({ type: 'CREATE_SHIPMENT_IN_REQUEST' });
              })
              .catch(err => {
                dispatch({ type: 'CREATE_SHIPMENT_IN_REQUEST_ERROR', err });
              });
          })
          .catch(err => {
            dispatch({ type: 'CREATE_SHIPMENT_IN_REQUEST_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'CREATE_DELIVERY_NOTIFICATIONS_ERROR', err });
      });
    // firestore
    //   .collection(constantsConfig.collectionInventoryNotifications)
    //   .doc(request.id)
    //   .set({
    //     ...request,
    //     createdTimestamp: Date.now(),
    //   })
    //   .then(() => {
    //     firestore
    //       .collection(constantsConfig.collectionShipmentInRequest)
    //       .doc(request.id)
    //       .delete()
    //       .then(() => {
    //         dispatch({ type: 'CREATE_INVENTORY_NOTIFICATIONS' });
    //         firestore
    //           .collection(constantsConfig.collectionDelivery)
    //           .doc(request.id)
    //           .set({
    //             ...request,
    //           })
    //           .then(() => {
    //             dispatch({ type: 'CREATE_DELIVERY_NOTIFICATIONS' });
    //           })
    //           .catch(err => {
    //             dispatch({ type: 'CREATE_DELIVERY_NOTIFICATIONS_ERROR', err });
    //           });
    //       })
    //       .catch(err => {
    //         dispatch({ type: 'CREATE_INVENTORY_NOTIFICATIONS_ERROR', err });
    //       });
    //   })
    //   .catch(err => {
    //     dispatch({ type: 'CREATE_INVENTORY_NOTIFICATIONS_ERROR', err });
    //   });
  };
};

export const updateDeliveryStatus = (deliveryDocument, status) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firestore
      .collection(constantsConfig.collectionDelivery)
      .doc(deliveryDocument.id)
      .update({
        status: status,
      })
      .then(() => {
        dispatch({ type: 'UPDATE_DELIVERY_STATUS' });
      })
      .catch(err => {
        dispatch({ type: 'UPDATE_DELIVERY_STATUS_ERROR', err });
      });
  };
};

export const removeShipmentInRequest = shipmentInId => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firestore
      .collection(constantsConfig.collectionShipmentInRequest)
      .doc(shipmentInId)
      .delete()
      .then(() => {
        dispatch({ type: 'UPDATE_DELIVERY_STATUS' });
      })
      .catch(err => {
        dispatch({ type: 'UPDATE_DELIVERY_STATUS_ERROR', err });
      });
  };
};

export const removeItemCardRequest = itemCardId => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    console.log('itemCardId', itemCardId);
    firestore
      .collection(constantsConfig.collectionItemsCardsRequests)
      .doc(itemCardId)
      .delete()
      .then(() => {
        dispatch({ type: 'REMOVE_ITEM_CARD' });
      })
      .catch(err => {
        dispatch({ type: 'REMOVE_ITEM_CARD_ERROR', err });
      });
  };
};

export const createNotification = notification => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firestore
      .collection(constantsConfig.collectionNotifications)
      .add({})
      .then(resp => {
        firestore
          .collection(constantsConfig.collectionNotifications)
          .doc(resp.id)
          .set({
            ...notification,
            id: resp.id,
          })
          .then(resp => {
            dispatch({ type: 'CREATE_NOTIFICATION' });
          })
          .catch(err => {
            dispatch({ type: 'CREATE_NOTIFICATION_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'CREATE_NOTIFICATION_ERROR', err });
      });
  };
};

export const updateNotification = notificationId => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    console.log('apo', notificationId);
    firestore
      .collection('notifications')
      .doc(notificationId)
      .update({
        clickTimestamp: Date.now(),
      })
      .then(resp => {
        console.log('success');
        dispatch({ type: 'UPDATE_NOTIFICATION' });
      })
      .catch(err => {
        console.log('err', err);
        dispatch({ type: 'UPDATE_NOTIFICATION_ERROR', err });
      });
  };
};
