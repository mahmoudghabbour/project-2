import { constantsConfig } from '../../../config/ConstanstsConfig';

export const requestOrder = order => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    dispatch({ type: 'SPINNER_ORDER' });
    firestore
      .collection(constantsConfig.collectionOrders)
      .add({})
      .then(resp => {
        firestore
          .collection(constantsConfig.collectionOrders)
          .doc(resp.id)
          .set({ ...order, id: resp.id, createdTimestamp: Date.now() })
          .then(resp => {
            dispatch({ type: 'REQUEST_ORDER_SUCCESS' });
          })
          .catch(err => {
            dispatch({ type: 'REQUEST_ORDER_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'REQUEST_ORDER_ERROR', err });
      });
  };
};

export const updatePin = pin => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    console.log('pinAction', pin);
    firestore
      .collection(constantsConfig.collectionPins)
      .doc(pin.pinId)
      .update({
        capacity: pin.capacity,
        maxCapacity: pin.maxCapacity,
        seller: pin.seller,
        sellerId: pin.sellerId,
        sku: pin.sku,
        isFull: pin.isFull,
      })
      .then(() => {
        dispatch({ type: 'UPDATE_PIN' });
      })
      .catch(err => {
        dispatch({ type: 'UPDATE_PIN_ERROR', err });
      });
  };
};

export const archivedOrder = order => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    console.log('order', order);
    dispatch({ type: 'SPINNER_ORDER' });
    firestore
      .collection(constantsConfig.collectionOrdersArchived)
      .doc(order.id)
      .set({
        ...order,
        createdTimestamp: Date.now(),
      })
      .then(() => {
        firestore
          .collection(constantsConfig.collectionOrders)
          .doc(order.id)
          .delete()
          .then(() => {
            dispatch({ type: 'CREATE_ORDER_SUCCESS' });
          })
          .catch(err => {
            dispatch({ type: 'CREATE_ORDER_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'CREATE_ORDER_ERROR', err });
      });
  };
};

export const removeOrder = orderId => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    console.log('orderId', orderId);
    dispatch({ type: 'SPINNER_ORDER' });
    firestore
      .collection(constantsConfig.collectionOrders)
      .doc(orderId)
      .delete()
      .then(() => {
        dispatch({ type: 'CREATE_ORDER_SUCCESS' });
      })
      .catch(err => {
        dispatch({ type: 'CREATE_ORDER_ERROR', err });
      });
  };
};
