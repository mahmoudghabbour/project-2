import { constantsConfig } from 'config/ConstanstsConfig';

export const signIn = credentials => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    dispatch({ type: 'START_SPINNER' });
    firebase
      .auth()
      .signInWithEmailAndPassword(credentials.email, credentials.password)
      .then(() => {
        dispatch({ type: 'LOGIN_SUCCESS' });
      })
      .catch(err => {
        dispatch({ type: 'LOGIN_ERROR', err });
      });
  };
};

export const signOut = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .auth()
      .signOut()
      .then(() => {
        dispatch({ type: 'SIGNOUT_SUCCESS' });
      });
  };
};

export const signUp = newUser => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firebase
      .auth()
      .createUserWithEmailAndPassword(
        newUser.updateSignUp.email,
        newUser.password
      )
      .then(resp => {
        return firestore
          .collection(constantsConfig.collectionUsers)
          .doc(resp.user.uid)
          .set({
            ...newUser.updateSignUp,
            id: resp.user.uid,
            createdTimestamp: Date.now(),
          })
          .then(() => {
            firestore
              .collection(constantsConfig.collectionUsersRequests)
              .doc(newUser.documentId)
              .delete()
              .then(() => {
                dispatch({ type: 'SIGNUP_SUCCESS' });
              })
              .catch(err => {
                dispatch({ type: 'SIGNUP_ERROR', err });
              });
          })
          .catch(err => {
            dispatch({ type: 'SIGNUP_ERROR', err });
          });
      })
      .then(() => {
        dispatch({ type: 'SIGNUP_SUCCESS' });
      })
      .catch(err => {
        dispatch({ type: 'SIGNUP_ERROR', err });
      });
  };
};

export const usersRequests = newUser => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    dispatch({ type: 'START_SPINNER' });
    firestore
      .collection(constantsConfig.collectionUsersRequests)
      .add({
        ...newUser,
        createdTimestamp: Date.now(),
      })
      .then(resp => {
        dispatch({ type: 'USER_REQUEST' });
      })
      .catch(err => {
        dispatch({ type: 'USER_REQUEST_ERROR', err });
      });
  };
};

export const createAccountForUser = credentials => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firebase
      .auth()
      .createUserWithEmailAndPassword('ahmad@test.com', 'test11')
      .then(userCredential => {
        // Signed in
        var user = userCredential.user;
        // ...
      })
      .catch(error => {
        var errorCode = error.code;
        var errorMessage = error.message;
        // ..
      });
  };
};

export const addNewUser = credentials => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firestore
      .collection(constantsConfig.collectionEmployees)
      .add({})
      .then(resp => {
        firestore
          .collection(constantsConfig.collectionEmployees)
          .doc(resp.id)
          .set({
            ...credentials,
            id: resp.id,
            createdTimestamp: Date.now(),
          })
          .then(() => {
            dispatch({ type: 'ADD_NEW_USER' });
          })
          .catch(err => {
            dispatch({ type: 'ADD_NEW_USER_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'ADD_NEW_USER_ERROR', err });
      });
  };
};

export const adminSignUp = newUser => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firebase
      .auth()
      .createUserWithEmailAndPassword(
        newUser.updateSignUp.email,
        newUser.password
      )
      .then(resp => {
        return firestore
          .collection('users')
          .doc(resp.user.uid)
          .set({
            ...newUser.updateSignUp,
            id: resp.user.uid,
            createdDate: new Date().getTime(),
          });
      })
      .then(() => {
        dispatch({ type: 'SIGNUP_SUCCESS' });
      })
      .catch(err => {
        dispatch({ type: 'SIGNUP_ERROR', err });
      });
  };
};
