import { constantsConfig } from '../../../config/ConstanstsConfig';

export const createWarehouse = warehouseData => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();

    firestore
      .collection(constantsConfig.collectionWarehouses)
      .add({})
      .then(resp => {
        firestore
          .collection(constantsConfig.collectionWarehouses)
          .doc(resp.id)
          .set({
            ...warehouseData,
            id: resp.id,
            createdTimestamp: Date.now(),
          })
          .then(() => {
            dispatch({ type: 'CREATE_WAREHOUSE' });
          })
          .catch(err => {
            dispatch({ type: 'CREATE_WAREHOUSE_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'CREATE_WAREHOUSE_ERROR', err });
      });
  };
};

export const createPath = pathData => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();

    firestore
      .collection(constantsConfig.collectionPaths)
      .add({})
      .then(resp => {
        firestore
          .collection(constantsConfig.collectionPaths)
          .doc(resp.id)
          .set({
            ...pathData,
            id: resp.id,
            createdTimestamp: Date.now(),
          })
          .then(() => {
            dispatch({ type: 'CREATE_PATH' });
          })
          .catch(err => {
            dispatch({ type: 'CREATE_PATH_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'CREATE_PATH_ERROR', err });
      });
  };
};

export const createStand = standData => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firestore
      .collection(constantsConfig.collectionStands)
      .add({})
      .then(resp => {
        firestore
          .collection(constantsConfig.collectionStands)
          .doc(resp.id)
          .set({
            ...standData,
            id: resp.id,
            createdTimestamp: Date.now(),
          })
          .then(() => {
            dispatch({ type: 'CREATE_STAND' });
            for (let i = 0; i < standData.numOfShelves; i++) {
              firestore
                .collection(constantsConfig.collectionShelves)
                .add({})
                .then(shelveResp => {
                  firestore
                    .collection(constantsConfig.collectionShelves)
                    .doc(shelveResp.id)
                    .set({
                      shelveNum: parseInt(i + 1),
                      shelveName: 'shelve ' + (i + 1),
                      standName: standData.standName,
                      standNum: standData.standNum,
                      standId: resp.id,
                      pathId: standData.pathId,
                      pathName: standData.pathName,
                      pathNum: standData.pathNum,
                      warehouseId: standData.warehouseId,
                      warehouseName: standData.warehouseName,
                      warehouseNum: standData.warehouseNum,
                      numOfPins: parseInt(standData.numOfPinsPerShelve),
                      typeOfPins: standData.typeOfPins,
                      pinSpace: standData.pinSpace,
                      id: shelveResp.id,
                      createdTimestamp: Date.now(),
                      isFull: false,
                    })
                    .then(() => {
                      dispatch({ type: 'CREATE_STAND' });
                      for (let j = 0; j < standData.numOfPinsPerShelve; j++) {
                        firestore
                          .collection(constantsConfig.collectionPins)
                          .add({})
                          .then(pinResp => {
                            firestore
                              .collection(constantsConfig.collectionPins)
                              .doc(pinResp.id)
                              .set({
                                standName: standData.standName,
                                standNum: standData.standNum,
                                standId: resp.id,
                                pathId: standData.pathId,
                                pathName: standData.pathName,
                                pathNum: standData.pathNum,
                                warehouseId: standData.warehouseId,
                                warehouseName: standData.warehouseName,
                                warehouseNum: standData.warehouseNum,
                                pinNum: parseInt(j + 1),
                                pinName: 'pin ' + (j + 1),
                                shelveId: shelveResp.id,
                                shelveNum: parseInt(i + 1),
                                shelveName: 'shelve ' + (i + 1),
                                id: pinResp.id,
                                pinSpace: standData.pinSpace,
                                createdTimestamp: Date.now(),
                                sku: null,
                                capacity: null,
                                maxCapacity: null,
                                isFull: false,
                                seller: null,
                                sellerId: null,
                              })
                              .then(() => {
                                dispatch({ type: 'CREATE_PIN' });
                              })
                              .catch(err => {
                                dispatch({ type: 'CREATE_PIN_ERROR', err });
                              });
                          })
                          .catch(err => {
                            dispatch({ type: 'CREATE_PIN_ERROR', err });
                          });
                      }
                    })
                    .catch(err => {
                      dispatch({ type: 'CREATE_STAND_ERROR', err });
                    });
                })
                .catch(err => {
                  dispatch({ type: 'CREATE_STAND_ERROR', err });
                });
            }
          })
          .catch(err => {
            dispatch({ type: 'CREATE_STAND_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'CREATE_STAND_ERROR', err });
      });
  };
};

export const storageItemInWarehouse = item => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    // console.log('item', item);
    // firestore
    //   .collection(constantsConfig.collectionWarehouseItems)
    //   .add({})
    //   .then(resp => {
    //     firestore
    //       .collection(constantsConfig.collectionWarehouseItems)
    //       .doc(resp.id)
    //       .set({
    //         ...item,
    //         id: resp.id,
    //         createdTimestamp: Date.now(),
    //       })
    //       .then(() => {
    //         dispatch({ type: 'CREATE_WAREHOUSE_ITEM' });
    //       })
    //       .catch(err => {
    //         dispatch({ type: 'CREATE_WAREHOUSE_ITEM_ERROR', err });
    //       });
    //   })
    //   .catch(err => {
    //     dispatch({ type: 'CREATE_WAREHOUSE_ITEM_ERROR', err });
    //   });
  };
};

export const removeDeliveryRequest = shipmentInSerialNumber => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    console.log('shipmentInSerialNumber', shipmentInSerialNumber);
    dispatch({ type: 'DELIVERY_SPINNER' });
    firestore
      .collection(constantsConfig.collectionDelivery)
      .doc(shipmentInSerialNumber)
      .delete()
      .then(() => {
        dispatch({ type: 'REMOVE_DELIVERY_ITEM' });
      })
      .catch(err => {
        dispatch({ type: 'REMOVE_DELIVERY_ITEM_ERROR', err });
      });
  };
};

export const updatePin = pin => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    console.log('pin', pin);
    firestore
      .collection(constantsConfig.collectionPins)
      .doc(pin.pinId)
      .update({
        capacity:
          pin.maxQuantityForPin && pin.warehouseItemsLength
            ? parseInt(pin.maxQuantityForPin - pin.warehouseItemsLength)
            : 0,
        maxCapacity: parseInt(pin.maxQuantityForPin),
        seller: pin.seller,
        sellerId: pin.sellerId,
        sku: pin.sku,
        isFull:
          pin.maxQuantityForPin && pin.warehouseItemsLength
            ? parseInt(pin.maxQuantityForPin - pin.warehouseItemsLength) == 0
              ? true
              : false
            : true,
      })
      .then(() => {
        dispatch({ type: 'UPDATE_PIN' });
      })
      .catch(err => {
        dispatch({ type: 'UPDATE_PIN_ERROR', err });
      });
  };
};

export const createShelve = shelveData => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firestore
      .collection(constantsConfig.collectionShelves)
      .add({})
      .then(resp => {
        firestore
          .collection(constantsConfig.collectionShelves)
          .doc(resp.id)
          .set({
            ...shelveData,
            id: resp.id,
            createdTimestamp: Date.now(),
          })
          .then(() => {
            dispatch({ type: 'CREATE_STAND' });
          })
          .catch(err => {
            dispatch({ type: 'CREATE_STAND_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'CREATE_STAND_ERROR', err });
      });
  };
};

export const createPin = pinData => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firestore
      .collection(constantsConfig.collectionPins)
      .add({})
      .then(resp => {
        firestore
          .collection(constantsConfig.collectionPins)
          .doc(resp.id)
          .set({
            ...pinData,
            id: resp.id,
            createdTimestamp: Date.now(),
          })
          .then(() => {
            dispatch({ type: 'CREATE_PIN' });
          })
          .catch(err => {
            dispatch({ type: 'CREATE_PIN_ERROR', err });
          });
      })
      .catch(err => {
        dispatch({ type: 'CREATE_PIN_ERROR', err });
      });
  };
};
