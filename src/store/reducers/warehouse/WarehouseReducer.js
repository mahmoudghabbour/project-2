const initState = {
  deliverySpinner: null,
};

const WarehouseReducer = (state = initState, action) => {
  switch (action.type) {
    case 'CREATE_WAREHOUSE':
      console.log('create warehouse success');
      return {
        ...state,
      };
    case 'CREATE_WAREHOUSE_ERROR':
      console.log('create warehouse error');
      return {
        ...state,
      };
    case 'REMOVE_DELIVERY_ITEM':
      console.log('remove delivery item success');
      return {
        ...state,
        deliverySpinner: 'successful',
      };
    case 'REMOVE_DELIVERY_ITEM_ERROR':
      console.log('remove delivery item error');
      return {
        ...state,
      };
    case 'UPDATE_PIN':
      console.log('update pin success');
      return {
        ...state,
      };
    case 'UPDATE_PIN_ERROR':
      console.log('update pin error');
      return {
        ...state,
      };
    case 'CREATE_WAREHOUSE_ITEM':
      console.log('create warehouse item success');
      return {
        ...state,
      };
    case 'CREATE_WAREHOUSE_ITEM_ERROR':
      console.log('create warehouse item error');
      return {
        ...state,
      };
    case 'CREATE_PATH':
      console.log('create path success');
      return {
        ...state,
      };
    case 'CREATE_PATH_ERROR':
      console.log('create path error');
      return {
        ...state,
      };
    case 'CREATE_STAND':
      console.log('create stand success');
      return {
        ...state,
      };
    case 'CREATE_STAND_ERROR':
      console.log('action.err', action.err);
      console.log('create stand error');
      return {
        ...state,
      };
    case 'CREATE_SHELVE':
      console.log('create shelve success');
      return {
        ...state,
      };
    case 'CREATE_SHELVE_ERROR':
      console.log('action.err', action.err);
      console.log('create shelve error');
      return {
        ...state,
      };
    case 'CREATE_PIN':
      console.log('create pin success');
      return {
        ...state,
      };
    case 'CREATE_PIN_ERROR':
      console.log('create pin error');
      return {
        ...state,
      };
    case 'DELIVERY_SPINNER':
      return {
        ...state,
        deliverySpinner: 'loading',
      };
    default:
      return state;
  }
};

export default WarehouseReducer;
