const initState = {
  orderError: null,
  spinnerOrder: null,
};

const OrdersReducer = (state = initState, action) => {
  switch (action.type) {
    case 'REQUEST_ORDER_SUCCESS':
      console.log('request order success');
      return {
        ...state,
        spinnerOrder: 'successful',
      };
    case 'REQUEST_ORDER_ERROR':
      console.log('request order error');
      return {
        ...state,
        spinnerOrder: null,
      };
    case 'CREATE_ORDER_SUCCESS':
      console.log('create order success');
      return {
        ...state,
        spinnerOrder: 'successful',
      };
    case 'CREATE_ORDER_ERROR':
      console.log('create order failed');
      return {
        ...state,
      };
    case 'REMOVE_ORDER_SUCCESS':
      console.log('remove order success');
      return {
        ...state,
      };
    case 'REMOVE_ORDER_ERROR':
      console.log('remove order success');
      return {
        ...state,
      };
    case 'SPINNER_ORDER':
      return {
        ...state,
        spinnerOrder: 'loading',
      };
    default:
      return state;
  }
};

export default OrdersReducer;
