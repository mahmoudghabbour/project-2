const initState = {
  spinner: null,
  authError: null,
  isSuccessfulSignedIn: null,
};

const AuthReducer = (state = initState, action) => {
  switch (action.type) {
    case 'LOGIN_ERROR':
      console.log('login error');
      return {
        ...state,
        authError: 'Login failed',
        spinner: null,
      };

    case 'LOGIN_SUCCESS':
      console.log('login success');
      return {
        ...state,
        authError: null,
        isSuccessfulSignedIn: true,
      };

    case 'ADD_NEW_USER_ERROR':
      console.log('add new user error');
      return {
        ...state,
      };

    case 'ADD_NEW_USER':
      console.log('add new suer success');
      return {
        ...state,
      };

    case 'SIGNOUT_SUCCESS':
      console.log('signout success');
      return state;

    case 'SIGNUP_SUCCESS':
      console.log('signup success');
      return {
        ...state,
        authError: null,
      };

    case 'SIGNUP_ERROR':
      console.log('signup error');
      return {
        ...state,
        authError: action.err.message,
      };

    case 'USER_REQUEST':
      console.log('user request success');
      return {
        ...state,
        authError: null,
        spinner: false,
      };

    case 'USER_REQUEST_ERROR':
      console.log('user request error');
      return {
        ...state,
        authError: action.err.message,
      };
    case 'START_SPINNER':
      return {
        ...state,
        spinner: true,
      };

    default:
      return state;
  }
};

export default AuthReducer;
