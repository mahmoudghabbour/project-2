const initState = {
  createItemSuccessful: null,
  shipmentInState: null,
};

const ShipmentsReducer = (state = initState, action) => {
  switch (action.type) {
    case 'CREATE_ITEM_CARD':
      console.log('create item card success');
      return {
        ...state,
        createItemSuccessful: 'successful',
      };
    case 'CREATE_ITEM_CARD_ERROR':
      console.log('create item card error');
      return {
        ...state,
      };
    case 'REQUEST_ITEM_CARD':
      console.log('request item card success');
      return {
        ...state,
      };
    case 'REQUEST_ITEM_CARD_ERROR':
      console.log('request item card error');
      return {
        ...state,
      };
    case 'CREATE_INVENTORY_NOTIFICATIONS':
      console.log('create inventory notifications success');
      return {
        ...state,
      };
    case 'CREATE_INVENTORY_NOTIFICATIONS_ERROR':
      console.log('create inventory notifications error');
      return {
        ...state,
      };
    case 'CREATE_DELIVERY_NOTIFICATIONS':
      console.log('create delivery notifications success');
      return {
        ...state,
      };
    case 'CREATE_DELIVERY_NOTIFICATIONS_ERROR':
      console.log('create delivery notifications error');
      return {
        ...state,
      };
    case 'CREATE_SHIPMENT_IN_REQUEST':
      console.log('create shipment in success');
      return {
        ...state,
        shipmentInState: 'successful',
      };
    case 'CREATE_SHIPMENT_IN_REQUEST_ERROR':
      console.log('create shipment in error');
      return {
        ...state,
        shipmentInState: null,
      };
    case 'DELETE_SHIPMENT_IN_REQUEST':
      console.log('delete shipment in success');
      return {
        ...state,
      };
    case 'DELETE_SHIPMENT_IN_REQUEST_ERROR':
      console.log('delete shipment in error');
      return {
        ...state,
      };
    case 'UPDATE_DELIVERY_STATUS':
      console.log('update delivery status success');
      return {
        ...state,
      };
    case 'UPDATE_DELIVERY_STATUS_ERROR':
      console.log('update delivery status error');
      return {
        ...state,
      };
    case 'REMOVE_SHIPMENT_IN_REQUEST':
      console.log('remove shipment in request');
      return {
        ...state,
      };
    case 'REMOVE_SHIPMENT_IN_REQUEST_ERROR':
      console.log('remove shipment in request error');
      return {
        ...state,
      };

    case 'REMOVE_ITEM_CARD':
      console.log('remove item card');
      return {
        ...state,
      };
    case 'REMOVE_ITEM_CARD_ERROR':
      console.log('remove item card error');
      return {
        ...state,
      };
    case 'CREATE_NOTIFICATION':
      return {
        ...state,
      };
    case 'CREATE_NOTIFICATION_ERROR':
      return {
        ...state,
      };
    case 'UPDATE_NOTIFICATION':
      return {
        ...state,
      };
    case 'UPDATE_NOTIFICATION_ERROR':
      return {
        ...state,
      };
    case 'ITEM_SPINNER':
      return {
        ...state,
        createItemSuccessful: 'loading',
      };
    case 'SHIPMENT_IN_SPINNER':
      return {
        ...state,
        shipmentInState: 'loading',
      };
    default:
      return state;
  }
};

export default ShipmentsReducer;
