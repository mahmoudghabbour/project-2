import { combineReducers } from 'redux';
import { firestoreReducer } from 'redux-firestore';
import AuthReducer from './reducers/auth/AuthReducer';
import { firebaseReducer } from 'react-redux-firebase';
import OrdersReducer from './reducers/orders/OrdersReducer';
import ShipmentsReducer from './reducers/shipments/ShipmentsReducer';
import WarehouseReducer from './reducers/warehouse/WarehouseReducer';

const RootReducer = combineReducers({
  auth: AuthReducer,
  ordersReducer: OrdersReducer,
  shipments: ShipmentsReducer,
  warehouse:WarehouseReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer
});

export default RootReducer;
