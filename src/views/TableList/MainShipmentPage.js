import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import AddShipment from './AddShipmentIn';

class MainShipmentPage extends Component {
 constructor(props){
   super(props);
   this.state = {
    isOpenAddShipment: false,
    isOpenFiltering: false,
    isOpenHistory: false,
   };
 }
 handleOpenAddShipment = () => {
  this.setState({
    isOpenAddShipment: true,
  });
 };
 handleCloseAddShipment = () => {
  this.setState({
   isOpenAddShipment: false,
  });
 };
 handleOpenFiltering = () => {
  this.setState({
   isOpenFiltering: true,
  });
 };
 handleCloseFiltering = () => {
  this.setState({
   isOpenFiltering: false,
  });
 };
 handleOpenHistory = () => {
  this.setState({
   isOpenHistory: true,
  });
 };
 handleCloseHistory = () => {
  this.setState({
   isOpenHistory: false,
  });
 };
 render() {
  return (
   <React.Fragment>
    <Dialog open={this.state.isOpenAddShipment} onClose={this.handleCloseAddShipment}>
     <AddShipmentIn />
    </Dialog>

   </React.Fragment>
  );
 }
}