import React, { Component } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Icon from "@material-ui/core/Icon";
import CardIcon from "components/Card/CardIcon.js";
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import SystemUpdateAltIcon from '@material-ui/icons/SystemUpdateAlt';
import AddShipment from './AddShipmentIn';
import Dialog from '@material-ui/core/Dialog';
import { withStyles } from "@material-ui/core/styles";


import {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
 } from '@material-ui/core';

import {
  successColor,
  whiteColor,
  grayColor,
  hexToRgb
} from "assets/jss/material-dashboard-react.js";
import { Button } from "@material-ui/core";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardCategory: {
    color: grayColor[0],
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    paddingTop: "10px",
    marginBottom: "0"
  },
  cardTitle: {
    color: grayColor[2],
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: grayColor[1],
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  stats: {
    color: grayColor[0],
    display: "inline-flex",
    fontSize: "12px",
    lineHeight: "22px",
    "& svg": {
      top: "4px",
      width: "16px",
      height: "16px",
      position: "relative",
      marginRight: "3px",
      marginLeft: "3px"
    },
    "& .fab,& .fas,& .far,& .fal,& .material-icons": {
      top: "4px",
      fontSize: "16px",
      position: "relative",
      marginRight: "3px",
      marginLeft: "3px"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};


class TableList extends Component{    
  constructor(props) {
    super(props);
    this.state = {
     isOpenAddShipment: false,
     isOpenFiltering: false,
     isOpenHistory: false,
    };
  }
  handleOpenAddShipment = () => {
    this.setState({
      isOpenAddShipment: true,
    });
   };
   handleCloseAddShipment = () => {
    this.setState({
     isOpenAddShipment: false,
    });
   };
   handleOpenFiltering = () => {
    this.setState({
     isOpenFiltering: true,
    });
   };
   handleCloseFiltering = () => {
    this.setState({
     isOpenFiltering: false,
    });
   };
   handleOpenHistory = () => {
    this.setState({
     isOpenHistory: true,
    });
   };
   handleCloseHistory = () => {
    this.setState({
     isOpenHistory: false,
    });
   };

  render(){

      const { classes } = this.props;

    return(
        <GridContainer>
        <GridItem xs={12} sm={6} md={4}>
            <Card style={{ padding: "10px" }}>
              <CardHeader color="warning" stats icon>
                <CardIcon color="warning">
                  <Icon>content_copy</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Requsted</p>
                <h3 className={classes.cardTitle}>
                  30
                </h3>
              </CardHeader>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={4}>
            <Card style={{ padding: "10px" }}>
              <CardHeader color="warning" stats icon>
                <CardIcon color="warning">
                  <LocalShippingIcon />
                </CardIcon>
                <p className={classes.cardCategory}>Pick-Up</p>
                <h3 className={classes.cardTitle}>
                  30
                </h3>
              </CardHeader>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={4}>
            <Card style={{ padding: "10px" }}>
              <CardHeader color="warning" stats icon>
                <CardIcon color="warning">
                  <SystemUpdateAltIcon />
                </CardIcon>
                <p className={classes.cardCategory}>Inserting</p>
                <h3 className={classes.cardTitle}>
                  30
                </h3>
              </CardHeader>
            </Card>
          </GridItem>
          <DialogActions>
            <Button variant="outlined" color="primary" onClick={this.handleOpenAddShipment} style={{margin: "5px", left: "55em"}}> 
              Add Shipment
            </Button>
          </DialogActions>
          <Dialog open={this.state.isOpenAddShipment} onClose={this.handleCloseAddShipment}>
            <AddShipment />
          </Dialog>
          <DialogActions>
            <Button variant="outlined" color="primary" style={{margin: "5px", left: "55em"}} >
              Filtering
            </Button>
          </DialogActions>
          <DialogActions>
            <Button variant="outlined" color="primary" style={{margin: "5px", left: "55em"}}>
              History
            </Button>
          </DialogActions>
        <GridItem xs={12} sm={12} md={12}>  
          <Card plain>
            <CardHeader plain color="primary">
              <h4 className={classes.cardTitleWhite}>
                Shipment In Table
              </h4>
              <p className={classes.cardCategoryWhite}>
                Propratise is Table
              </p>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="primary"
                tableHead={["Sr.No.", "Ref.No.", "Seller", "Reciver", "Warehouse", "Date"]}
                tableData={[
                  ["1", "Dakota Rice", "$36,738", "Niger", "Oud-Turnhout", "1/12/2020"],
                  ["2", "Minerva Hooper", "$23,789", "Curaçao", "Sinaai-Waas","1/12/2020"],
                  ["3", "Sage Rodriguez", "$56,142", "Netherlands", "Baileux","1/12/2020"],
                  [
                    "4",
                    "Philip Chaney",
                    "$38,735",
                    "Korea, South",
                    "Overland Park",
                    "1/12/2020"
                  ],
                  [
                    "5",
                    "Doris Greene",
                    "$63,542",
                    "Malawi",
                    "Feldkirchen in Kärnten",
                    "1/12/2020"
                  ],
                  ["6", "Mason Porter", "$78,615", "Chile", "Gloucester","1/12/2020"]
                ]}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
        );

      }
    }

export default withStyles(styles)(TableList);