import React from 'react';
// react plugin for creating charts
import ChartistGraph from 'react-chartist';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import firebase from 'firebase/app';
// @material-ui/core
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
// @material-ui/icons
import Store from '@material-ui/icons/Store';
import Warning from '@material-ui/icons/Warning';
import DateRange from '@material-ui/icons/DateRange';
import LocalOffer from '@material-ui/icons/LocalOffer';
import Update from '@material-ui/icons/Update';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import AccessTime from '@material-ui/icons/AccessTime';
import Accessibility from '@material-ui/icons/Accessibility';
import BugReport from '@material-ui/icons/BugReport';
import Code from '@material-ui/icons/Code';
import Cloud from '@material-ui/icons/Cloud';
// core components
import GridItem from 'components/Grid/GridItem.js';
import GridContainer from 'components/Grid/GridContainer.js';
import Table from 'components/Table/Table.js';
import Tasks from 'components/Tasks/Tasks.js';
import CustomTabs from 'components/CustomTabs/CustomTabs.js';
import Danger from 'components/Typography/Danger.js';
import Card from 'components/Card/Card.js';
import CardHeader from 'components/Card/CardHeader.js';
import CardIcon from 'components/Card/CardIcon.js';
import CardBody from 'components/Card/CardBody.js';
import CardFooter from 'components/Card/CardFooter.js';
import Grid from '@material-ui/core/Grid';
import { bugs, website, server } from 'variables/general.js';
import { constantsConfig } from '../../config/ConstanstsConfig';
import InputIcon from '@material-ui/icons/Input';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import LoyaltyIcon from '@material-ui/icons/Loyalty';
import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart,
} from 'variables/charts.js';

import styles from 'assets/jss/material-dashboard-react/views/dashboardStyle.js';
import { Component } from 'react';
import { CircularProgress } from '@material-ui/core';

// const useStyles = makeStyles(styles);

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: null,
      totalBins: null,
      reservedBins: null,
      orderRequests: null,
      shipmentInRequests: null,
      itemsCards: null,
    };
  }

  componentDidMount() {
    console.log('componentDidMount', this.props);
    this.handleGetProfile(this.props.auth.uid);
  }

  handleGetProfile = async profileId => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionUsers)
      .where('id', '==', profileId);
    const snapshot = await query.get();
    const profile = snapshot.docs.map(doc => doc.data());
    this.setState({
      profile: profile[0],
    });
    if (profile[0].isAdmin) {
      this.handleGetReservedBins();
      this.handleGetShipmentInRequests();
      this.handleGetTotalBins();
      this.handleGetOrderRequests();
      this.handleGetItemsCardsRequests();
    } else {
      this.handleGetReservedBinsUser(profile[0].id);
      this.handleGetShipmentInRequestsUser(profile[0].id);
      this.handleGetOrderRequestsUser(profile[0].id);
      this.handleGetItemsCardsRequestsUser(profile[0].id);
    }
  };

  handleGetTotalBins = async () => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionPins);
    const snapshot = await query.get();
    const bins = snapshot.docs.map(doc => doc.data());
    this.setState({
      totalBins: bins,
    });
  };

  handleGetReservedBins = async () => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionPins)
      .where('sku', '!=', null);
    const snapshot = await query.get();
    const reservedBins = snapshot.docs.map(doc => doc.data());
    this.setState({
      reservedBins: reservedBins,
    });
  };

  handleGetOrderRequests = async () => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionOrders);
    const snapshot = await query.get();
    const orders = snapshot.docs.map(doc => doc.data());
    this.setState({
      orderRequests: orders,
    });
  };

  handleGetShipmentInRequests = async () => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionShipmentInRequest);
    const snapshot = await query.get();
    const shipmentInRequests = snapshot.docs.map(doc => doc.data());
    this.setState({
      shipmentInRequests: shipmentInRequests,
    });
  };

  handleGetItemsCardsRequests = async () => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionItemsCardsRequests);
    const snapshot = await query.get();
    const itemsCards = snapshot.docs.map(doc => doc.data());
    this.setState({
      itemsCards: itemsCards,
    });
  };

  // User functions

  handleGetReservedBinsUser = async sellerId => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionPins)
      .where('sellerId', '==', sellerId);
    const snapshot = await query.get();
    const reservedBins = snapshot.docs.map(doc => doc.data());
    this.setState({
      reservedBins: reservedBins,
    });
  };

  handleGetOrderRequestsUser = async sellerId => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionOrdersArchived)
      .where('sellerId', '==', sellerId);
    const snapshot = await query.get();
    const orders = snapshot.docs.map(doc => doc.data());
    this.setState({
      orderRequests: orders,
    });
  };

  handleGetShipmentInRequestsUser = async sellerId => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionShipmentsInHistory)
      .where('sellerId', '==', sellerId);
    const snapshot = await query.get();
    const shipmentInRequests = snapshot.docs.map(doc => doc.data());
    this.setState({
      shipmentInRequests: shipmentInRequests,
    });
  };

  handleGetItemsCardsRequestsUser = async sellerId => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionItemsCards)
      .where('sellerId', '==', sellerId);
    const snapshot = await query.get();
    const itemsCards = snapshot.docs.map(doc => doc.data());
    this.setState({
      itemsCards: itemsCards,
    });
  };

  render() {
    const { classes } = this.props;
    console.log('this.state', this.state);
    if (this.state.profile)
      if (this.state.profile.isAdmin)
        return (
          <div style={{ marginTop: '5em' }}>
            <GridContainer>
              <GridItem xs={12} sm={6} md={6}>
                <Card>
                  <CardHeader color="warning" stats icon>
                    <CardIcon color="warning">
                      {/* <Icon>content_copy</Icon> */}
                      <InputIcon />
                    </CardIcon>
                    <p className={classes.cardCategory}>Used Bins</p>
                    {this.state.totalBins ? (
                      <h3 className={classes.cardTitle}>
                        {this.state.reservedBins
                          ? this.state.reservedBins.length
                          : 0}
                        /{this.state.totalBins.length} {/* <small>GB</small> */}
                      </h3>
                    ) : null}
                  </CardHeader>
                  <CardFooter stats></CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={6} md={6}>
                <Card>
                  <CardHeader color="success" stats icon>
                    <CardIcon color="success">
                      {/* <Store /> */}
                      <Icon>content_paste</Icon>
                    </CardIcon>
                    <p className={classes.cardCategory}>Shipments In</p>
                    {this.state.shipmentInRequests ? (
                      <h3 className={classes.cardTitle}>
                        {this.state.shipmentInRequests.length}
                      </h3>
                    ) : (
                      0
                    )}
                  </CardHeader>
                  <CardFooter stats></CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={6} md={6}>
                <Card>
                  <CardHeader color="danger" stats icon>
                    <CardIcon color="danger">
                      {/* <Icon>info_outline</Icon> */}
                      <ShoppingCartIcon />
                    </CardIcon>
                    <p className={classes.cardCategory}>Orders</p>
                    {this.state.orderRequests ? (
                      <h3 className={classes.cardTitle}>
                        {this.state.orderRequests.length}
                      </h3>
                    ) : (
                      0
                    )}
                  </CardHeader>
                  <CardFooter stats></CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={6} md={6}>
                <Card>
                  <CardHeader color="info" stats icon>
                    <CardIcon color="info">
                      {/* <Accessibility /> */}
                      <LoyaltyIcon />
                    </CardIcon>
                    <p className={classes.cardCategory}>Items cards</p>
                    {this.state.itemsCards ? (
                      <h3 className={classes.cardTitle}>
                        {this.state.itemsCards.length}
                      </h3>
                    ) : (
                      0
                    )}
                  </CardHeader>
                  <CardFooter stats></CardFooter>
                </Card>
              </GridItem>
            </GridContainer>
            <GridContainer></GridContainer>
          </div>
        );
      else
        return (
          <div style={{ marginTop: '5em' }}>
            <GridContainer>
              <GridItem xs={12} sm={6} md={6}>
                <Card>
                  <CardHeader color="warning" stats icon>
                    <CardIcon color="warning">
                      {/* <Icon>content_copy</Icon> */}
                      <InputIcon />
                    </CardIcon>
                    <p className={classes.cardCategory}>Used Bins</p>
                    {this.state.reservedBins ? (
                      <h3 className={classes.cardTitle}>
                        {this.state.reservedBins
                          ? this.state.reservedBins.length
                          : 0}{' '}
                      </h3>
                    ) : null}
                  </CardHeader>
                  <CardFooter stats></CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={6} md={6}>
                <Card>
                  <CardHeader color="success" stats icon>
                    <CardIcon color="success">
                      {/* <Store /> */}
                      <Icon>content_paste</Icon>
                    </CardIcon>
                    <p className={classes.cardCategory}>Shipments In</p>
                    {this.state.shipmentInRequests ? (
                      <h3 className={classes.cardTitle}>
                        {this.state.shipmentInRequests.length}
                      </h3>
                    ) : (
                      0
                    )}
                  </CardHeader>
                  <CardFooter stats></CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={6} md={6}>
                <Card>
                  <CardHeader color="danger" stats icon>
                    <CardIcon color="danger">
                      {/* <Icon>info_outline</Icon> */}
                      <ShoppingCartIcon />
                    </CardIcon>
                    <p className={classes.cardCategory}>Orders</p>
                    {this.state.orderRequests ? (
                      <h3 className={classes.cardTitle}>
                        {this.state.orderRequests.length}
                      </h3>
                    ) : (
                      0
                    )}
                  </CardHeader>
                  <CardFooter stats></CardFooter>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={6} md={6}>
                <Card>
                  <CardHeader color="info" stats icon>
                    <CardIcon color="info">
                      {/* <Accessibility /> */}
                      <LoyaltyIcon />
                    </CardIcon>
                    <p className={classes.cardCategory}>Items cards</p>
                    {this.state.itemsCards ? (
                      <h3 className={classes.cardTitle}>
                        {this.state.itemsCards.length}
                      </h3>
                    ) : (
                      0
                    )}
                  </CardHeader>
                  <CardFooter stats></CardFooter>
                </Card>
              </GridItem>
            </GridContainer>
            <GridContainer></GridContainer>
          </div>
        );
    else
      return (
        <div>
          <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            className={classes.gridCircularProgress}
          >
            <Grid item xs={3}>
              <CircularProgress className={classes.circularProgress} />
            </Grid>
          </Grid>
        </div>
      );
  }
}

const mapStateToProps = (state, props) => {
  console.log('state', state);
  return {
    auth: state.firebase.auth,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    // removeOrder: orderId => dispatch(removeOrder(orderId)),
  };
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: constantsConfig.collectionWarehouses,
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(Dashboard);
