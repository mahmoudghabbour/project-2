import React from 'react';
import { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import firebase from 'firebase/app';
import ReactPhoneInput from 'react-phone-input-2';
import { constantsConfig } from '../../config/ConstanstsConfig';
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import CircularProgress from '@material-ui/core/CircularProgress';
// core components
import GridItem from 'components/Grid/GridItem.js';
import GridContainer from 'components/Grid/GridContainer.js';
import CustomInput from 'components/CustomInput/CustomInput.js';
import Button from 'components/CustomButtons/Button.js';
import Card from 'components/Card/Card.js';
import CardHeader from 'components/Card/CardHeader.js';
import CardAvatar from 'components/Card/CardAvatar.js';
import CardBody from 'components/Card/CardBody.js';
import CardFooter from 'components/Card/CardFooter.js';

import avatar from 'assets/img/faces/marc.jpg';

import 'react-phone-input-2/lib/material.css';
import 'react-phone-number-input/style.css';
const styles = {
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    marginTop: '0',
    marginBottom: '0',
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
  },
};

const useStyles = makeStyles(styles);

class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: null,
      name: '',
      email: '',
      role: '',
      mobile: null,
    };
  }
  componentDidMount() {
    console.log('this.props', this.props);
    this.handleGetProfile(this.props.auth.uid);
  }
  handleGetProfile = async profileId => {
    const query = firebase
      .firestore()
      .collection(constantsConfig.collectionUsers)
      .where('id', '==', profileId);
    const snapshot = await query.get();
    const profile = snapshot.docs.map(doc => doc.data());
    this.setState({
      profile: profile[0],
      name: profile[0].name,
      email: profile[0].email,
      role: profile[0].role,
      mobile: profile[0].mobile,
    });
  };

  handleChangeMobile = e => {
    this.setState({
      mobile: e,
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };

  handleSubmit = () => {
    console.log('this.state', this.state);
  };

  render() {
    const { classes } = this.props;
    console.log('this.state', this.state);
    if (this.state.profile)
      return (
        <div>
          <GridContainer>
            {/* <GridItem xs={12} sm={12} md={8}> */}
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Edit Profile</h4>
                {/* <p className={classes.cardCategoryWhite}>
                  Complete your profile
                </p> */}
              </CardHeader>
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Name"
                      id="name"
                      inputProps={this.state.name}
                      // value={this.state.name}
                      formControlProps={{
                        fullWidth: true,
                      }}
                      onChange={e => this.handleChangeMobile(e)}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Email address"
                      id="email"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      onChange={e => this.handleChangeMobile(e)}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Role"
                      id="role"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      onChange={e => this.handleChangeMobile(e)}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <div style={{ marginTop: '1em' }}>
                      <ReactPhoneInput
                        placeholder="Enter phone number"
                        defaultCountry={'us'}
                        onChange={e => this.handleChangeMobile(e)}
                      />
                    </div>
                  </GridItem>
                </GridContainer>
                {/* <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <InputLabel style={{ color: '#AAAAAA' }}>
                      About me
                    </InputLabel>
                    <CustomInput
                      labelText="Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo."
                      id="about-me"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      inputProps={{
                        multiline: true,
                        rows: 5,
                      }}
                    />
                  </GridItem>
                </GridContainer> */}
              </CardBody>
              <CardFooter>
                <Button color="primary">Update Profile</Button>
              </CardFooter>
            </Card>
            {/* </GridItem> */}
          </GridContainer>
        </div>
      );
    else return <CircularProgress />;
  }
}

const mapStateToProps = (state, props) => {
  console.log('state', state);
  return {
    auth: state.firebase.auth,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default compose(
  firestoreConnect(props => {
    console.log('props', props);
    return [
      {
        collection: constantsConfig.collectionUsers,
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles)
)(UserProfile);
