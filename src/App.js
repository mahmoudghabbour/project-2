import React, { Component } from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom';
import { connect } from 'react-redux';
// import { createBrowserHistory } from 'history';
import Admin from 'layouts/Admin.js';
// import RTL from 'layouts/RTL.js';
import 'assets/css/material-dashboard-react.css?v=1.9.0';
import SignUp from './components/auth/SignUp';
import SignIn from './components/auth/SignIn';
import OrdersNotifications from 'components/notifications/OrdersNotifications';
import ShipmentInRequest from 'components/shipments/in/ShipmentInRequest';
import CreateItemCard from 'components/shipments/in/CreateItemCard';
import ShipmentInNotifications from 'components/notifications/ShipmentInNotifications';
import InventoryNotifications from 'components/notifications/InventoryNotifications';
import MainPage from './layouts/MainPage';
import AdminSignUp from 'components/auth/AdminSignUp';
import ItemCardsList from 'components/shipments/in/ItemCardsList';
import ShipmentInList from 'components/shipments/in/ShipmentInList';
import DeliveryList from 'components/shipments/in/DeliveryList';
import InventoryList from 'components/inventory/InventoryList';
import AddPath from 'components/warehouse/AddPath';
import AddStand from 'components/warehouse/AddStand';
import AddShelve from 'components/warehouse/AddShelve';
import AddPin from 'components/warehouse/AddPin';
import UsersList from 'components/users/UsersList';
import TestReadBarcode from 'components/shipments/in/TestReadBarcode';
import TestReadBarcode2 from 'components/shipments/in/TestReadBarcode2';
import DashboardShipmentIn from 'components/dashboard/DashboardShipmentIn';
// const hist = createBrowserHistory();
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Switch>
            <Route
              path="/warehouse/:warehouseId/path/:pathId/shelve/:shelveId/pin/:pinId"
              render={props => <AddPin {...this.props} />}
            />
            <Route
              path="/warehouse/:warehouseId/path/:pathId/shelve/:shelveId"
              render={props => <AddShelve {...this.props} />}
            />
            <Route
              path="/warehouse/:warehouseId/path/:pathId"
              render={props => <AddStand {...this.props} />}
            />
            <Route path="/admin/warehouse/:warehouseId" component={AddPath} />
            <Route
              path="/inventorylist"
              render={props => <InventoryList {...this.props} />}
            />
            <Route
              path="/userslist"
              render={props => <UsersList {...this.props} />}
            />
            <Route
              path="/deliverylist"
              render={props => <DeliveryList {...this.props} />}
            />
            <Route
              path="/admin/shipmentinlist"
              render={props => <ShipmentInList {...this.props} />}
            />
            <Route
              path="/inventorynotifications"
              render={props => <InventoryNotifications {...this.props} />}
            />
            <Route
              path="/shipmentsinnotifications"
              render={props => <ShipmentInNotifications {...this.props} />}
            />
            <Route
              path="/createitemcard"
              render={props => <CreateItemCard {...this.props} />}
            />
            <Route
              path="/shipmentinrequest"
              render={props => <ShipmentInRequest {...this.props} />}
            />

            <Route path="/user/signup" component={SignUp} />
            <Route path="/user/signin" component={SignIn} />
            <Route path="/adminsignup" component={AdminSignUp} />
            <Route path="/ordernotifications" component={OrdersNotifications} />
            <Route
              path="/itemcardslist"
              // component={ItemCardsList}
              // component={<ItemCardsList auth={this.props.auth} />}
              render={props => <ItemCardsList auth={this.props.auth} />}
            />
            <Route path="/dashboard" component={DashboardShipmentIn} />
            <Route path="/testreadbarcode" component={TestReadBarcode} />
            <Route path="/testreadbarcode2" component={TestReadBarcode2} />
            {/* <Route path="/rtl" component={RTL} /> */}
            <Route
              path="/"
              render={props =>
                this.props.auth.uid ? (
                  <Admin auth={this.props.auth} />
                ) : (
                  <MainPage />
                )
              }
              // component={this.props.auth.uid ? Admin : MainPage}
            />

            {/* <Redirect from="/" to="/admin/dashboard" /> */}
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
